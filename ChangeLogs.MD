# Release v0.1.4

## Source

- Firmware Version: **v99.01.83**

- RIDEGRID Version: **v99.00.69**

- SDK Version: **v0.0.10**


## Change logs

### **Sample Code / UI**
- Preferences
  - Functional code for Whatsapp and other settings sample (iOS & Android)
  - Sleep time slider (iOS & Android)
  - Audio Setting implemented in settings (iOS & Android)
  - Dynamic volume implemented (iOS & Android)
  - Second phone paring added (iOS)
  - Device rename added (iOS)
  - Call functionality added in Buddy Screen
- Speed dial
  - Speed Dial data persistance anomalies (iOS )
- RIDELYNK & Buddy List
  - Call action added to each Buddy item
  - Buddy list duplicate entries fix
  - RIDELYNK duplicate entries fix

### **Firmware & RIDEGRID**

- Volume

  - Context aware volume control.
      > Music, voice calls and system prompts volume level are distinct


- RIDELYNK

  - MAC address is used as a placeholder for devices discovered without name


- RIDEGRID

  - Audio call quality improvement

- Connectivity

  - Default device name is set to serial name

  - Model tag change from SX10 and SX20 to S10X and S20X respectively

  - BLE connection sequence improvement to eliminate connection delay 
