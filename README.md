# BluConnect - BluArmor Companion SDK

BluConnect is an library designed for Bluetooth Low Energy  communication with BluArmor Devices. It allows for discovery, connecting and communicating with devices that are powered by BluArmor technology.

[`BluConnectService`](doc/bluconnect/com.aptener.bluconnect/-blu-connect-service/index.md) exposes high level API for connecting and communicating with BluArmor devices.

![SDK Flow](sample/img/0.png)

## Features

 **BluConnectService** class provides the following features:

 1. Seamless auto connection
 2. BluArmor device discovery
 3. Pillion discovery
 4. System's Bluetooth state
 5. System's Location state
 6. List of previously connected devices and pillions
 7. Retrieve / Update user preferences

## Importing

SDK is available under root path of this repository.

Copy `bluconnect-debug.aar`, `common-debug.aar` and `dfu-debug.aar` file to src/main/libs of the Android Studio project.

Then open ‘build.gradle’ file(the one under ‘app’) and add dependency

```grovy
    implementation fileTree(dir: 'libs', include: ['bluconnect-debug.aar', 'common-debug.aar'])
```

In case of error message, stating ‘Failed to resolve :bluconnect. To fix this error open the top level ‘build.gradle’ file and add

```grovy
flatDir {
dirs ‘src/main/libs’
}
```

#### Setting up

The library supports **minSdkVersion 23**

### SDK prerequisites

Some features require addiitonal permission to work.

#### Location

BLE scanning requires phone's location to be turned On.
SDK also needs to track the user’s location in the background to enable features such as Ridegrid Radar View and Dyanmic Volume controls based on Rider’s moving average speed.

#### Calendar

SDK reads calendar events for the day (as part of the Ride Start Routine) as soon as user starts the ride.

#### Contact

SDK announces WhatsApp messages along with the sender name by looking up the sender phone number in the Contacts database. Contact access is also required to auto answer/reject phone calls.

#### Physical Activity

On android 10 and above SDK require [Physical Activity](https://developer.android.com/about/versions/10/privacy/changes#physical-activity-recognition)permission, for ride start routine.

## Usage

[`BluConnectService`](doc/bluconnect/com.aptener.bluconnect/-blu-connect-service/index.md) is an abstract [Service](https://developer.android.com/guide/components/services) class.
Extend [`BluConnectService`](doc/bluconnect/com.aptener.bluconnect/-blu-connect-service/index.md) with app's [Foreground Service](https://developer.android.com/guide/components/foreground-services) to access the range of SDK features.

The [Foreground Service](https://developer.android.com/guide/components/foreground-services) allows the [`BluConnectService`](doc/bluconnect/com.aptener.bluconnect/-blu-connect-service/index.md) to run in background to auto connect and keep the BLE connection alive even when the phone is in standby mode.

![Connectivity Flow](sample/img/7.png)

The first step is to extend the [`BluConnectService`](doc/bluconnect/com.aptener.bluconnect/-blu-connect-service/index.md) class with [Foreground Service](https://developer.android.com/guide/components/foreground-services) and implement methods.

```java
class MyAppService: BluConnectService() {

    // This method will be called when device will connect / disconnect
    // To get the device we can use ConnectionState.device
    // Once we get `ConnectionState.Ready` that mean device is connected and initialized
    override fun onConnectionStateChange(state: ConnectionState) { }

    // When scanning starts this method will be triggers
    // Can be utilized if auto connection is not used
    override fun onBluArmorDeviceDiscovered(result: Set<AdvertisingBluArmor>) { }

    // When pillion scanning starts this method will be triggers
    // Once device is connected you can trigger `BluArmorService.scanPillion()` method
    // to scan nearby pillion devices
    override fun onPillionDeviceDiscovered(result: Set<PillionBluArmor>) { }

}
```

If BluArmor device is found in the Bluetooth range (~10 meters), connection process will be initiated and the [`ConnectionState`](doc/bluconnect/com.aptener.bluconnect.client/-connection-state/index.md) changes will be notified via  [`onConnectionStateChange(...)`](doc/bluconnect/com.aptener.bluconnect/-blu-connect-service-call-back/on-connection-state-change.md) method call as the connection gets established.

```java
override fun onConnectionStateChange(state: ConnectionState) {
    when(state){
        // Connection request initiated
        is ConnectionState.Connecting -> ...

        // BLE connected successfully
        is ConnectionState.Connected -> ...
    
        // Device initialization is completed
        // This state represent that device is ready to be used
        // To get the device use `ConnectionState.Ready.device` or `getActiveDevice()`
        is ConnectionState.Ready -> ...

        // Disconnect request initiated
        is ConnectionState.Disconnecting -> ...

        // Device is disconnected
        ConnectionState.Disconnected -> ...

        // Connection attempt failed
        // To get the reason use `ConnectionState.Failed.reason`
        is ConnectionState.Failed -> ...
    }
}
```

Once [`onConnectionStateChange(...)`](doc/bluconnect/com.aptener.bluconnect/-blu-connect-service-call-back/on-connection-state-change.md) emits  [`ConnectionState.Ready`](doc/bluconnect/com.aptener.bluconnect.client/-connection-state/-ready/index.md) state, it implies that the device is connected and ready to use. To get the connected `BluArmor` device use `ConnectionState.Ready.device` or [`getActiveDevice()`](doc/bluconnect/com.aptener.bluconnect/-blu-connect-service/get-active-device.md).

## Supported BluArmor Devices

* [**SX10**](doc/device/SX10.md)
* [**SX20**](doc/device/SX20.md)
  
### BluArmor Device Usage

BluArmor device exposes supported features as a class property.
For example `SX20` supports `Volume`, `RIDEGRID`, `RIDELYNK`  and more...

To observe specific feature's state/value, use `PropertyObserver`

```java
// Once the connection state reflect device is ready
val device:SX20 = getActiveDevice()

val volumeObserver = PropertyObserver<VolumeLevel>{ level->
    // Update the UI with `VolumeLevel`
}

device.volume.observe(volumeObserver)
```

To interact with device's feature.

```java
// Once the connection state reflect device is ready
val device:SX20 = getActiveDevice()
// Change the volume level
device.volume.changeVolume(12)
```

## Firmware Update
BluArmor supports firmware upgrade over Bluetooth.

![Connectivity Flow](sample/img/8.png)

To check if there is any Firmware update available

```java
// Once the connection state reflect device is ready
val device:SX20 = getActiveDevice()

val firmwareUpdateObserver = PropertyObserver<FirmwareUpdateState>{ state->
    // Update the UI
}

device.firmwareUpdate.observe(firmwareVersionObserver)
```

If firmware update is available, call `FirmwareVersion.startFirmwareUpdate()` method to start the firmware update process

```java
// Once the connection state reflect device is ready
val device:SX20 = getActiveDevice()
// Function will return the State to confirm if update started of failed with reason
device.firmwareUpdate.startFirmwareUpdate()
```



## Best practices

#### Ask for location permission upfront

Location permission is mandatory for BLE discovery. Location permission is also required for other features like RIDEGRID Radar view and Dynamic volume mode.

Note: From android 10 (API level 29) app need to ensure user selects **Allow all the time**. To request location permissions follow the official guide [https://developer.android.com/training/location/permissions](https://developer.android.com/training/location/permissions)

![Location permission sample](sample/img/1.png)

#### Quick action to turn on Bluetooth and Location

![Pre firmware update check](sample/img/2.png)
![Pre firmware update check](sample/img/3.png)

## SDK Documentation

Follow the link: [doc/bluconnect/index.md](doc/bluconnect/index.md)

## Play Store

Follow the link: [https://play.google.com/store/apps/details?id=com.aptener.bluarmor](https://play.google.com/store/apps/details?id=com.aptener.bluarmor)

## Test App

Follow the link: [test-app.apk](test-app.apk)

## App Prerequisites

Ther are few features that are dependent of the main app where SDK is being implemented.

1. Whatsapp message announcement
2. Whatsapp voice calls

In order for these features to work main app need to have a [NotificationListenerService](https://developer.android.com/reference/android/service/notification/NotificationListenerService) and override two methods [`onNotificationPosted`](doc/bluconnect/com.aptener.bluconnect/-blu-connect-app/on-notification-posted.md) and [`onNotificationRemoved`](doc/bluconnect/com.aptener.bluconnect/-blu-connect-app/on-notification-removed.md)

```java
class MyNotificationListenerService : NotificationListenerService() {
    override fun onNotificationPosted(sbn: StatusBarNotification?) {
        super.onNotificationPosted(sbn)

        // Requied to parse WhatsApp messages and voice calls 
        BluConnectApp.onNotificationPosted(sbn)
    }

    override fun onNotificationRemoved(sbn: StatusBarNotification?) {
        super.onNotificationRemoved(sbn)

        // Requied to parse WhatsApp messages and voice calls
        BluConnectApp.onNotificationRemoved(sbn)
    }
}
```

Calling [`BluConnectApp.onNotificationPosted(sbn)`](doc/bluconnect/com.aptener.bluconnect/-blu-connect-app/on-notification-posted.md) and [`BluConnectApp.onNotificationRemoved(sbn)`](doc/bluconnect/com.aptener.bluconnect/-blu-connect-app/on-notification-removed.md) methods let the SDK parse WhatsApp notifications to announce messages and handle voice calls.
