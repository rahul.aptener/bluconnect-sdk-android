# Voice announcement implementation guide

BluArmor device exposes Voice Announcement feature as a class property.

## Queue text messages
To queue text message, call **VoiceAnnouncement.queueMessage(TextAnnouncementMsg)**, TextAnnouncementMsg accepts two argument `message` as string and `priority` as intiger from 0 to 5. Following snippet show how to queue the text message

```
// Once the connection state reflect device is ready
val device:SX20 = getActiveDevice()

// Custom message
val message = TextAnnouncementMsg("Hello World!")

// Message is queued
device.voiceAnnouncement.queueMessage(message)
```

## Queue audio media
To queue audio media, call **VoiceAnnouncement.queueMessage(MediaAnnouncementMsg)**, MediaAnnouncementMsg accepts two argument `mediaUri` as [URI](https://developer.android.com/reference/android/net/Uri) and `priority` as intiger from 0 to 5. Following snippet show how to queue the text message
```
// Once the connection state reflect device is ready
val device:SX20 = getActiveDevice()

// Custom message
val message = MediaAnnouncementMsg(Uri.fromFile(File("/sdcard/hello.mp3")))

// Message is queued
device.voiceAnnouncement the order in which they are queued will be considered(message)
```


### **Queue Rules**
- Message with lowest priority will be first in the queue
- If priority of two messages are same then the order in which they are queued will be considered
- TextAnnouncementMsg accept SSML as well
- MediaAnnouncementMsg only supports m4a and mp3 formats