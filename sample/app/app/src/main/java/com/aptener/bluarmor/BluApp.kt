package com.aptener.bluarmor

import android.app.Application
import com.google.firebase.ktx.Firebase
import com.google.firebase.ktx.initialize
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class BluApp : Application() {
    override fun onCreate() {
        super.onCreate()
        Firebase.initialize(applicationContext)
        Timber.plant(TimberDebugLogTree())
    }
}

class TimberDebugLogTree : Timber.DebugTree() {
    override fun createStackElementTag(element: StackTraceElement): String? {
        return "${element.lineNumber}.${super.createStackElementTag(element)}"
    }
}