package com.aptener.bluarmor

import android.Manifest
import android.annotation.SuppressLint
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.graphics.Color
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.navigation
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.aptener.bluarmor.service.BluService
import com.aptener.bluarmor.service.bluconnect.model.BluArmorInterface
import com.aptener.bluarmor.service.bluconnect.model.RideGridGroup
import com.aptener.bluarmor.ui.AppDestination
import com.aptener.bluarmor.ui.app.buddylist.*
import com.aptener.bluarmor.ui.app.buddylist.invitebuddies.InviteBuddiesView
import com.aptener.bluarmor.ui.app.buddylist.invitebuddies.InviteBuddiesViewModel
import com.aptener.bluarmor.ui.app.buddylist.invitebuddies.InviteBuddiesViewModelAssistedFactory
import com.aptener.bluarmor.ui.app.dashboard.DashboardView
import com.aptener.bluarmor.ui.app.dashboard.DashboardViewDestination
import com.aptener.bluarmor.ui.app.dashboard.DashboardViewModel
import com.aptener.bluarmor.ui.app.dashboard.DashboardViewModelAssistedFactory
import com.aptener.bluarmor.ui.app.discovery.DiscoveryView
import com.aptener.bluarmor.ui.app.discovery.DiscoveryViewViewModel
import com.aptener.bluarmor.ui.app.discovery.DiscoveryViewViewModelAssistedFactory
import com.aptener.bluarmor.ui.app.landing.LandingViewModel
import com.aptener.bluarmor.ui.app.landing.SplashScreenView
import com.aptener.bluarmor.ui.app.ota.OtaView
import com.aptener.bluarmor.ui.app.ota.OtaViewModel
import com.aptener.bluarmor.ui.app.ota.OtaViewModelAssistedFactory
import com.aptener.bluarmor.ui.app.permission.PermissionViewView
import com.aptener.bluarmor.ui.app.permission.PermissionViewViewModelAssistedFactory
import com.aptener.bluarmor.ui.app.permission.PermissionViewViewViewModel
import com.aptener.bluarmor.ui.app.preference.PreferenceView
import com.aptener.bluarmor.ui.app.preference.PreferenceViewModel
import com.aptener.bluarmor.ui.app.preference.PreferenceViewModelAssistedFactory
import com.aptener.bluarmor.ui.app.ridegrid.*
import com.aptener.bluarmor.ui.app.ridegrid.inviteRiders.RideGridInviteRidersView
import com.aptener.bluarmor.ui.app.ridegrid.inviteRiders.RideGridInviteRidersViewModel
import com.aptener.bluarmor.ui.app.ridegrid.inviteRiders.RideGridInviteRidersViewModelAssistedFactory
import com.aptener.bluarmor.ui.app.speeddial.SpeedDialConfigView
import com.aptener.bluarmor.ui.app.speeddial.SpeedDialConfigViewModel
import com.aptener.bluarmor.ui.app.speeddial.SpeedDialConfigViewModelAssistedFactory
import com.aptener.bluarmor.ui.util.asMutableStateFlow
import com.aptener.bluarmor.ui.util.navigate
import com.aptener.bluarmor.ui.util.popUpTo
import com.aptener.bluconnect.BluArmorModel
import com.aptener.bluconnect.client.ConnectionState
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.components.ActivityComponent
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import timber.log.Timber
import javax.inject.Inject


class MainActivityViewModel : ViewModel() {
    val serviceBounded: StateFlow<Boolean> = MutableStateFlow(false)
}

@AndroidEntryPoint
class MainActivity : ComponentActivity(), ServiceConnection {
    lateinit var bluService: BluService
    val getBluService: BluService
        get() = bluService

    private val vm: MainActivityViewModel by lazy { ViewModelProvider(this).get(MainActivityViewModel::class.java) }

    @Inject
    lateinit var dashboardViewModelAssistedFactory: DashboardViewModelAssistedFactory

    @Inject
    lateinit var discoveryViewViewModelAssistedFactory: DiscoveryViewViewModelAssistedFactory

    @Inject
    lateinit var buddyDiscoveryViewModelAssistedFactory: BuddyDiscoveryViewModelAssistedFactory

    @Inject
    lateinit var buddyListViewModelAssistedFactory: BuddyListViewModelAssistedFactory

    @Inject
    lateinit var inviteRidersViewModelAssistedFactory: InviteBuddiesViewModelAssistedFactory

    @Inject
    lateinit var speedDialConfigViewModelAssistedFactory: SpeedDialConfigViewModelAssistedFactory

    @Inject
    lateinit var rideGridViewModelAssistedFactory: RideGridViewModelAssistedFactory

    @Inject
    lateinit var rideGridGroupViewModelAssistedFactory: RideGridGroupViewModelAssistedFactory

    @Inject
    lateinit var permissionViewViewModelAssistedFactory: PermissionViewViewModelAssistedFactory

    @Inject
    lateinit var preferenceViewModelAssistedFactory: PreferenceViewModelAssistedFactory

    @Inject
    lateinit var rideGridInviteRidersViewModelAssistedFactory: RideGridInviteRidersViewModelAssistedFactory

    @Inject
    lateinit var otaViewModelAssistedFactory: OtaViewModelAssistedFactory

    lateinit var rideGridGroupViewModel: RideGridGroupViewModel

    @OptIn(ExperimentalFoundationApi::class)
    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            val navController = rememberNavController()

            val serviceBoundedState: Boolean by vm.serviceBounded.collectAsState(
                context = lifecycleScope.coroutineContext,
                initial = false
            )


            val systemUiController = rememberSystemUiController()
            val useDarkIcons = MaterialTheme.colors.isLight
//            val serviceBoundedState2: MutableState<Boolean> = serviceBounded.observeAsState(initial = false)

            SideEffect {
                // Update all of the system bar colors to be transparent, and use
                // dark icons if we're in light theme
                systemUiController.setSystemBarsColor(
                    color = Color.Transparent,
                    darkIcons = !useDarkIcons
                )
                // setStatusBarsColor() and setNavigationBarsColor() also exist
            }

            NavHost(navController = navController, startDestination = AppDestination.LandingDest.route) {

                composable(AppDestination.LandingDest.route) {
                    SplashScreenView(
                        navController, hiltViewModel<LandingViewModel>(it)
                    )
                }

                composable(AppDestination.DiscoveryDest.route) {
//                    val vm: DiscoveryViewViewModel = viewModel(factory = DiscoveryViewViewModel.Factory(assistedFactory, bluArmorInterface))
//                    Z
                    DiscoveryView(
                        navController,
                        it.hiltViewModel(
                            viewModelProvider = DiscoveryViewViewModel.Factory(
                                discoveryViewViewModelAssistedFactory,
                                bluService
                            )
                        )
                    )
                }

                composable(AppDestination.PreferenceDest.route) {
                    val vm: PreferenceViewModel = it.hiltViewModel(
                        viewModelProvider = PreferenceViewModel.Factory(
                            preferenceViewModelAssistedFactory,
                            bluService
                        )
                    )

                    PreferenceView(
                        navController = navController,
                        vm = vm
                    )
                }

                composable(AppDestination.PermissionDest.route) {
                    val vm: PermissionViewViewViewModel =
                        viewModel(factory = PermissionViewViewViewModel.Factory(permissionViewViewModelAssistedFactory))

                    PermissionViewView(
                        navController = navController,
                        vm = vm
                    )
                }

                composable(AppDestination.OtaDest.route) {
                    val vm: OtaViewModel =
                        viewModel(factory = OtaViewModel.Factory(otaViewModelAssistedFactory, getBluService))

                    OtaView(
                        navController = navController,
                        vm = vm
                    )
                }

                navigation(
                    startDestination = DashboardViewDestination.DashboardDest.route,
                    route = AppDestination.DashboardDest.route
                ) {

                    composable(DashboardViewDestination.DashboardDest.route) {
                        DashboardView(
                            navController = navController,
                            vm = it.hiltViewModel(
                                viewModelProvider = DashboardViewModel.Factory(
                                    dashboardViewModelAssistedFactory,
                                    bluService
                                )
                            )
                        )
                    }

                    composable(DashboardViewDestination.BuddyDiscoveryDest.route) {
                        BuddyDiscoveryView(
                            navController = navController,
                            vm = it.hiltViewModel(
                                viewModelProvider = BuddyDiscoveryViewViewModel.Factory(
                                    buddyDiscoveryViewModelAssistedFactory,
                                    bluService
                                )
                            )
                        )
                    }

                    composable(DashboardViewDestination.BuddyListDest.route) {
                        val vm: BuddyListViewViewModel =
                            viewModel(
                                factory = BuddyListViewViewModel.Factory(
                                    buddyListViewModelAssistedFactory,
                                    bluService
                                )
                            )

                        BuddyListView(navController = navController, vm = vm)
                    }

                    composable(DashboardViewDestination.BuddyInviteDest.route) {
                        val vm: InviteBuddiesViewModel =
                            viewModel(
                                factory = InviteBuddiesViewModel.Factory(
                                    inviteRidersViewModelAssistedFactory,
                                    bluService
                                )
                            )

                        InviteBuddiesView(navController = navController, vm = vm)
                    }

                    composable(DashboardViewDestination.SpeedDialConfigDest.route) {
                        val vm: SpeedDialConfigViewModel =
                            viewModel(
                                factory = SpeedDialConfigViewModel.Factory(
                                    speedDialConfigViewModelAssistedFactory,
                                    bluService
                                )
                            )

                        SpeedDialConfigView(navController = navController, vm = vm)
                    }
                    composable(
                        DashboardViewDestination.RideGridDashboardDest.route + "/{id}",
                        arguments = listOf(
                            navArgument("id") { type = NavType.StringType },
                        )
                    ) {
                        Timber.e("TEST 01: ${it.arguments?.getInt("id")}")
                        val vm: RideGridViewModel =
                            viewModel(
                                factory = RideGridViewModel.Factory(
                                    rideGridViewModelAssistedFactory,
                                    bluService,
                                    it.arguments?.getString("id") ?: ""
                                )
                            )
                        RideGridDashboardView(navController = navController, vm = vm)
                    }

                    composable(DashboardViewDestination.RideGridGroupDest.route) {
                        val vm: RideGridGroupViewModel =
                            viewModel(
                                factory = RideGridGroupViewModel.Factory(
                                    rideGridGroupViewModelAssistedFactory,
                                    bluService
                                )
                            )

                        rideGridGroupViewModel = vm

                        RideGridGroupView(navController = navController, vm = vm)
                    }

                    composable(DashboardViewDestination.RideGridCreateGroupDest.route) {
                        val vm: RideGridGroupViewModel =
                            viewModel(
                                factory = RideGridGroupViewModel.Factory(
                                    rideGridGroupViewModelAssistedFactory,
                                    bluService
                                )
                            )
                        rideGridGroupViewModel = vm


                        RideGridCreateGroupView(navController = navController, vm = vm)
                    }

                    composable(
                        route = DashboardViewDestination.RideGridInviteRidersDest.route + "/{id}",
                        arguments = listOf(
                            navArgument("id") { type = NavType.StringType },
                        )
                    ) {
                        val vm: RideGridInviteRidersViewModel =
                            viewModel(
                                factory = RideGridInviteRidersViewModel.Factory(
                                    rideGridInviteRidersViewModelAssistedFactory,
                                    bluService,
                                    it.arguments?.getString("id") ?: ""
                                )
                            )
                        RideGridInviteRidersView(navController = navController, vm = vm)
                    }

                }

            }

            if (serviceBoundedState) {

                val connectionState: ConnectionState? by bluService.bluConnectManager.connectionState.observeAsState(
                    bluService.bluConnectManager.connectionState.value
                )

                val invitationData = getInvitationData()
                var id: String? = null
                var name: String? = null

                with(invitationData) {
                    this ?: return@with
                    Timber.e("DATA URL: ${hexToAscii(this)}")
                    val dataSplit = hexToAscii(this)?.split("|") ?: return@with

                    id = dataSplit[0]
                    name = dataSplit[1]
                }

                if (id == null || name == null) {
//                    return@setContent navController.navigate(DashboardViewDestination.BuddyListDest)
                    when (connectionState) {
                        is ConnectionState.Connected -> navController.navigate(AppDestination.DiscoveryDest) {
                            popUpTo(AppDestination.LandingDest) { inclusive = true }
                        }

                        is ConnectionState.Connecting -> navController.navigate(AppDestination.DiscoveryDest) {
                            popUpTo(AppDestination.LandingDest) { inclusive = true }
                        }

                        is ConnectionState.DeviceReady -> {
                            with(connectionState) {
                                when (this?.device?.model) {
                                    BluArmorModel.S20X_OTA -> navController.navigate(AppDestination.OtaDest) {
                                        popUpTo(AppDestination.LandingDest) { inclusive = true }
                                    }
                                    else -> navController.navigate(AppDestination.DashboardDest) {
                                        popUpTo(AppDestination.LandingDest) { inclusive = true }
                                    }
                                }
                            }

                        }

                        ConnectionState.Discovery -> {
                            navController.navigate(AppDestination.DiscoveryDest) {
                                popUpTo(AppDestination.LandingDest) { inclusive = true }
                            }
                        }

                        is ConnectionState.Disconnecting -> navController.navigate(AppDestination.DiscoveryDest) {
                            popUpTo(AppDestination.LandingDest) { inclusive = true }
                        }

                        is ConnectionState.Failed -> navController.navigate(AppDestination.DiscoveryDest) {
                            popUpTo(AppDestination.LandingDest) { inclusive = true }
                        }

                        is ConnectionState.ScannerNotReady -> {
                            when ((connectionState as ConnectionState.ScannerNotReady).reason) {
                                ConnectionState.ScannerNotReady.NotReadyReason.NEARBY_PERMISSION_NEEDED,
                                ConnectionState.ScannerNotReady.NotReadyReason.LOCATION_PERMISSION_NEEDED -> navController.navigate(
                                    AppDestination.PermissionDest
                                ) {
                                    popUpTo(AppDestination.LandingDest) { inclusive = true }
                                }
                                else -> navController.navigate(AppDestination.DiscoveryDest) {
                                    popUpTo(AppDestination.LandingDest) { inclusive = true }
                                }
                            }
                        }
                        null -> navController.navigate(AppDestination.DiscoveryDest) {
                            popUpTo(AppDestination.LandingDest) { inclusive = true }
                        }
                    }
                } else {
                    navController.navigate(DashboardViewDestination.RideGridCreateGroupDest) {
                        popUpTo(AppDestination.LandingDest) { inclusive = true }
                    }
                    val vm: RideGridGroupViewModel =
                        viewModel(
                            factory = RideGridGroupViewModel.Factory(
                                rideGridGroupViewModelAssistedFactory,
                                bluService
                            )
                        )

                    lifecycleScope.launchWhenCreated {

                        delay(1000)

                        if (::rideGridGroupViewModel.isInitialized) {
                            //TODO V2
                            /*    rideGridGroupViewModel.newRideGridGroupFound2(
                                    RideGridGroup(
                                        0,
                                        id!!,
                                        name!!,
                                        false,
                                        System.currentTimeMillis()
                                    )
                                )*/
                        }


                        //TODO V2
                        /* vm.newRideGridGroupFound2(
                             RideGridGroup(
                                 0,
                                 id!!,
                                 name!!,
                                 false,
                                 System.currentTimeMillis()
                             )
                         )*/

                    }

                }
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.BLUETOOTH_CONNECT, Manifest.permission.BLUETOOTH_SCAN
                ), 0
            )
        }
    }

    override fun onStart() {

        super.onStart()

        with(Intent(this, BluService::class.java)) {
            startService(this)
        }
        // Bind to LocalService
        Intent(this, BluService::class.java).also { intent ->
            bindService(intent, this, Context.BIND_AUTO_CREATE)
        }

    }

    private fun getInvitationData(): String? {
        val uri: Uri? = intent.data


        return "(?<=ridegrid\\.page\\.link/).+\$".toRegex().findAll(
            uri?.toString() ?: ""
        ).map {
            it.value
        }.firstOrNull()
    }

    override fun onStop() {
        super.onStop()
        unbindService(this)
        vm.serviceBounded.asMutableStateFlow()?.tryEmit(false)
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onServiceConnected(className: ComponentName, service: IBinder) {
        val binder = service as BluService.BluServiceBinder
        bluService = binder.service
        vm.serviceBounded.asMutableStateFlow()?.tryEmit(true)
    }

    override fun onServiceDisconnected(className: ComponentName) {
        vm.serviceBounded.asMutableStateFlow()?.tryEmit(false)
    }

}


@Module
@InstallIn(ActivityComponent::class)
abstract class BluArmorInterfaceModule {
    @Binds
    abstract fun bindAnalyticsService(bluService: BluService): BluArmorInterface
}


@Composable
internal inline fun <reified T : ViewModel> NavBackStackEntry.hiltViewModel(viewModelProvider: ViewModelProvider.Factory): T {
    return ViewModelProvider(
        this.viewModelStore,
        viewModelProvider
//        HiltViewModelFactory(LocalContext.current, this)
    ).get(T::class.java)
}

fun hexToAscii(hexStr: String): String? {
    val output = StringBuilder("")
    var i = 0
    while (i < hexStr.length) {
        val str = hexStr.substring(i, i + 2)
        output.append(str.toInt(16).toChar())
        i += 2
    }
    return output.toString()
}