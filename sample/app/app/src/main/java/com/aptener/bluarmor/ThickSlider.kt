package com.aptener.bluarmor

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.aptener.bluarmor.ui.theme.BluArmorTheme

@Preview
@Composable
fun DefaultPreview2() {
    BluArmorTheme {
        ThickSlider(value = 0f, onValueChange = {

        })
    }
}

@Composable
fun ThickSlider(
    value: Float,
    onValueChange: (Float) -> Unit,
    modifier: Modifier = Modifier,
) {

}