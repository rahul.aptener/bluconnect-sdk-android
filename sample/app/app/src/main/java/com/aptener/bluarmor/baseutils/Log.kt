package com.aptener.bluarmor.baseutils

import timber.log.Timber

fun String.logE(vararg args: Any?) = Timber.e(this, args)

fun String.logD(vararg args: Any?) = Timber.d(this, args)

val String.logE
    get() = Timber.e(this)

val String.logD
    get() = Timber.d(this)