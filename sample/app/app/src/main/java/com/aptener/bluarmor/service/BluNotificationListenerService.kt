package com.aptener.bluarmor.service

import android.service.notification.NotificationListenerService
import android.service.notification.StatusBarNotification
import com.aptener.bluconnect.BluConnectApp

class BluNotificationListenerService : NotificationListenerService() {
    override fun onNotificationPosted(sbn: StatusBarNotification?, rankingMap: RankingMap?) {
        super.onNotificationPosted(sbn, rankingMap)
        BluConnectApp.Companion.onNotificationPosted(sbn, applicationContext)
    }

    override fun onNotificationRemoved(sbn: StatusBarNotification?, rankingMap: RankingMap?, reason: Int) {
        super.onNotificationRemoved(sbn, rankingMap, reason)
        BluConnectApp.Companion.onNotificationRemoved(sbn, applicationContext)
    }
}