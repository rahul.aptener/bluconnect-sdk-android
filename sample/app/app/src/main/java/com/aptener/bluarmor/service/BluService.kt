package com.aptener.bluarmor.service

import android.content.Intent
import android.os.Binder
import android.os.Build
import android.os.IBinder
import com.aptener.bluarmor.service.bluconnect.BluConnectManager
import com.aptener.bluarmor.service.bluconnect.DeviceStateNotification
import com.aptener.bluarmor.service.bluconnect.DeviceStateNotification.Companion.NOTIFICATION_ACTION
import com.aptener.bluarmor.service.bluconnect.DeviceStateNotification.Companion.NOTIFICATION_ACTION_EXIT
import com.aptener.bluarmor.service.bluconnect.DeviceStateNotification.Companion.NOTIFICATION_ACTION_KEY
import com.aptener.bluarmor.service.bluconnect.model.BluArmorInterface
import com.aptener.bluconnect.BluConnectService
import com.aptener.bluconnect.client.ConnectionState
import com.aptener.bluconnect.device.AdvertisingBluArmorDevice
import com.aptener.bluconnect.device.sxcontrol.MusicGridAppConfig
import kotlin.system.exitProcess

class BluService : BluConnectService(
    MusicGridAppConfig.Spotify(
        clientId = "<< clientId >>",
        redirectUri = "<< redirectUri >>"
    )
), BluArmorInterface {
    private val binder = BluServiceBinder()
    private val notificationDeviceState = DeviceStateNotification()

    override val bluConnectManager
            by lazy {
                BluConnectManager(
                    bluetoothClassic::turnOnBluetooth,
                    this::disconnect,
                    applicationContext,
                    notificationDeviceState,
                    logDataSource::exportDB,
                    sdkPrefDataSource
                )
            }

    override fun onBind(intent: Intent): IBinder = binder

    override fun onCreate() {
        super.onCreate()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForeground(
                DeviceStateNotification.NOTIFICATION_ID,
                notificationDeviceState.getNotificationBuilder(applicationContext).build()
            )
            notificationDeviceState.disconnected(applicationContext)
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent?.action == NOTIFICATION_ACTION && intent.extras?.getString(NOTIFICATION_ACTION_KEY) == NOTIFICATION_ACTION_EXIT) {
            stopForeground(true)
            exitProcess(9)
        }
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onBluArmorDeviceDiscovered(result: Set<AdvertisingBluArmorDevice>) =
        bluConnectManager.onBluArmorDeviceDiscovered(result)

    override fun onConnectionStateChange(state: ConnectionState) {
        bluConnectManager.onConnectionStateChange(state)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            when (state) {
                is ConnectionState.Connected,
                is ConnectionState.Connecting,
                is ConnectionState.DeviceReady,
                is ConnectionState.Disconnecting -> notificationDeviceState.connected(
                    applicationContext,
                    state.device.model.label
                )
                ConnectionState.Discovery,
                is ConnectionState.Failed,
                is ConnectionState.ScannerNotReady -> notificationDeviceState.disconnected(
                    applicationContext
                )
            }
        }
    }

    inner class BluServiceBinder : Binder() {
        val service
            get() = this@BluService
    }

}

