package com.aptener.bluarmor.service.bluconnect

import android.annotation.SuppressLint
import android.content.Context
import android.icu.util.Calendar
import android.net.Uri
import android.os.Build
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.aptener.bluarmor.R
import com.aptener.bluarmor.baseutils.asMutableLiveData
import com.aptener.bluarmor.service.bluconnect.model.*
import com.aptener.bluarmor.service.bluconnect.model.DeviceInfo
import com.aptener.bluarmor.service.bluconnect.model.RideGridGroup
import com.aptener.bluarmor.ui.app.preference.PreferenceViewModelInterface
import com.aptener.bluarmor.ui.util.asMutableStateFlow
import com.aptener.bluconnect.BluArmorModel
import com.aptener.bluconnect.BluConnectServiceCallBack
import com.aptener.bluconnect.device.sxcontrol.VoiceNoteRecorder.VoiceNoteRecorderActions
import com.aptener.bluconnect.client.ConnectionState
import com.aptener.bluconnect.device.*
import com.aptener.bluconnect.device.sxcontrol.*
import com.aptener.bluconnect.device.sxcontrol.DeviceInfo.DeviceDetails
import com.aptener.bluconnect.device.sxcontrol.VoiceNoteRecorder.VoiceNoteRecorderState
import com.aptener.bluconnect.device.sxotacontrol.FirmwareUpdate
import com.aptener.bluconnect.repo.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import timber.log.Timber
import java.lang.IllegalArgumentException


class BluConnectManager(
    private val turnOnBluetooth: (context: Context, withUserConsent: Boolean) -> Unit,
    private val disconnectDevice: () -> Unit,
    private val context: Context,
    private val deviceStateNotification: DeviceStateNotification,
    private val exportLogs: () -> Unit,
    private val sdkPrefDataSource: SdkPrefDataSource
) : BluConnectServiceCallBack, Sx20Interface {

    var device: S20X? = null
    override var activeRideLynkDevice: LiveData<BuddyDevice?> = MutableLiveData()

    val bluArmorModel: LiveData<BluArmorModel> = MutableLiveData(BluArmorModel.NO_DEVICE)
    val connectionState: LiveData<ConnectionState> = MutableLiveData()

    override val otaSwitchState: LiveData<OtaSwitch.OtaSwitchState> = MutableLiveData()
    override val discoveryState: LiveData<DiscoveryState> = MutableLiveData()
    override val deviceInfo: LiveData<DeviceInfo> = MutableLiveData()
    override val deviceDetails: LiveData<DeviceDetails> = MutableLiveData()
    override val batteryLevel: LiveData<Int> = MutableLiveData()
    override val chargingState: LiveData<Battery.ChargingState> = MutableLiveData()
    override val audioOverlayState: LiveData<RideGrid.AudioOverlayState> = MutableLiveData()
    override val musicOverlayLevel: LiveData<Float> = MutableLiveData()
    override val volumeState: LiveData<Volume.VolumeLevel> = MutableLiveData()
    override val voicePromptLevel: LiveData<Volume.VoicePromptVerbosityLevel> = MutableLiveData()
    override val musicState: LiveData<MusicState> = MutableLiveData()
    override val voiceNoteRecorderState: LiveData<VoiceNoteRecorderState> = MutableLiveData()
    override val phoneCallState: LiveData<PhoneCallState> = MutableLiveData()
    override val rideLynkIntercomState: LiveData<RideLynkIntercomState> = MutableLiveData()
    override val rideGridState: LiveData<RideGridIntercomState> = MutableLiveData()
    override val rideGridStateFlow: StateFlow<RideGridIntercomState> =
        MutableStateFlow(RideGridIntercomState.OFF)

    override val secondPhoneState: LiveData<SecondPhone.SecondPhoneState> = MutableLiveData()

    override val rideGridGroups: LiveData<List<RideGridGroup>>
        get() = (device?.rideGridGroupDataSource?.getAllGroups() ?: MutableLiveData()).map {
            it.map { rideGridGroup ->
                RideGridGroup(
                    rideGridGroup.id,
                    rideGridGroup.groupId,
                    rideGridGroup.name,
                    rideGridGroup.active,
                    rideGridGroup.joinedAt,
                    rideGridGroup.inviteCode
                )
            }

        }


    override val buddies: LiveData<List<Buddy>> = MutableLiveData()

    override val buddyRidersList: LiveData<List<BuddyDevice>>
        get() = (device?.buddiesDataSource?.getAllBuddies() ?: MutableLiveData()).map {
            it.map { buddyRiderEntity ->
                BuddyDevice(
                    R.drawable.ic_helmet,
                    buddyRiderEntity.name,
                    buddyRiderEntity.macAddress,
                    buddy = buddyRiderEntity
                )
            }
        }

    override val buddyDiscoveryList: LiveData<List<BuddyDevice>> = MutableLiveData()
    override val rideGridRiders: LiveData<List<RideGridUserDevice>> = MutableLiveData()
    val rideGridRidersFlow: StateFlow<List<RideGridUserDevice>> = MutableStateFlow(emptyList())

    override val speedDialState: LiveData<SpeedDial.SpeedDialState> = MutableLiveData()

    override val otaProgress: LiveData<Int> = MutableLiveData()
    override val rideGridOtaProgress: LiveData<Int> = MutableLiveData()

    override val speedDialContacts: LiveData<Array<Buddy?>> = MutableLiveData()

    fun startManualOta(path: Uri) = /*sx20Ota?.firmwareUpdate?.startUpdate(path) ?:*/ Unit

    private fun firmwareUpdateStateObserver(firmwareUpdateState: FirmwareUpdate.FirmwareUpdateState) {
//        Timber.e("OTA PROGRESS: ${firmwareUpdateState.json()}")
        when (firmwareUpdateState) {
            FirmwareUpdate.FirmwareUpdateState.BinaryNotFound -> Unit
            FirmwareUpdate.FirmwareUpdateState.Completed -> Unit
            FirmwareUpdate.FirmwareUpdateState.Failed -> Unit
            FirmwareUpdate.FirmwareUpdateState.Finalizing -> Unit
            is FirmwareUpdate.FirmwareUpdateState.InvalidBinary -> Unit
            FirmwareUpdate.FirmwareUpdateState.ProcessingBinary -> Unit
            is FirmwareUpdate.FirmwareUpdateState.Retrying -> Unit
            FirmwareUpdate.FirmwareUpdateState.Starting -> Unit
            FirmwareUpdate.FirmwareUpdateState.Unknown -> Unit
            is FirmwareUpdate.FirmwareUpdateState.Upgrading -> {
                otaProgress.asMutableLiveData()?.value = firmwareUpdateState.appProgress
                rideGridOtaProgress.asMutableLiveData()?.value =
                    firmwareUpdateState.ridegridProgress
            }

            is FirmwareUpdate.FirmwareUpdateState.Downloading -> Unit
        }
    }

    private fun deviceInfoObserver(deviceDetails: DeviceDetails?) {
        this.deviceDetails.asMutableLiveData()?.value = deviceDetails
    }

    private fun otaSwitchState(otaSwitchState: OtaSwitch.OtaSwitchState) {
        this.otaSwitchState.asMutableLiveData()?.value = otaSwitchState
    }

    private fun musicOverlayLevelObserver(audioOverlayState: RideGrid.AudioOverlayState) {
        this.audioOverlayState.asMutableLiveData()?.value = audioOverlayState
        this.musicOverlayLevel.asMutableLiveData()?.value = audioOverlayState.level.toFloat()
    }

    private fun batteryObserver(level: Battery.BatteryLevel) {
        this.batteryLevel.asMutableLiveData()?.value = level.level
        this.chargingState.asMutableLiveData()?.value = level.chargingState
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            deviceStateNotification.updateBattery(
                context,
                device?.model?.label ?: "BluArmor",
                level.level
            )
        }
    }

    private fun volumeLevelObserver(volumeLevel: Volume.VolumeLevel) {
//        Timber.e("VOLUME: ${volumeLevel.json()}")
        this.volumeState.asMutableLiveData()?.value = volumeLevel
    }

    private fun voicePromptLevelObserver(voicePromptLevel: Volume.VoicePromptLevel) {
        this.voicePromptLevel.asMutableLiveData()?.value =
            voicePromptLevel.voicePromptVerbosityLevel
    }

    private fun mediaPlayerObserver(mediaPlayerState: MediaPlayer.MediaPlayerState) {
        this.musicState.asMutableLiveData()?.value = when (mediaPlayerState) {
            MediaPlayer.MediaPlayerState.Idle -> MusicState.Idle
            is MediaPlayer.MediaPlayerState.Playing -> MusicState.Playing(
                mediaPlayerState.track ?: "",
                mediaPlayerState.artist ?: ""
            )
        }
    }

    private fun speedDialObserver(speedDialState: SpeedDial.SpeedDialState) {
        this.speedDialState.asMutableLiveData()?.value = speedDialState
        when (speedDialState) {
            is SpeedDial.SpeedDialState.Active -> BluArmorSpeedDialState.Active(null)
            SpeedDial.SpeedDialState.Idle -> BluArmorSpeedDialState.Idle
        }
    }

    private fun speedDialContactObserver(speedDialContacts: Array<Buddy?>) {
        this.speedDialContacts.asMutableLiveData()?.value = speedDialContacts
    }

    private fun voiceNoteRecorderObserver(voiceNoteRecorderState: VoiceNoteRecorderState) {
        this.voiceNoteRecorderState.asMutableLiveData()?.value = voiceNoteRecorderState
    }

    private fun dialerObserver(dialerImplState: Dialer.DialerState) {
        this.phoneCallState.asMutableLiveData()?.value = when (dialerImplState) {
            Dialer.DialerState.Active -> PhoneCallState.Active("", "")
            Dialer.DialerState.Idle -> PhoneCallState.Idle
            is Dialer.DialerState.Incoming -> PhoneCallState.Incoming(
                dialerImplState.phoneNumber.toString(),
                dialerImplState.name.toString()
            )

            Dialer.DialerState.Missed -> PhoneCallState.MissedCall(
                "",
                "",
                Calendar.getInstance().apply {
                    timeInMillis = System.currentTimeMillis()
                })

            Dialer.DialerState.OutGoing -> PhoneCallState.Outgoing("", "")
            Dialer.DialerState.Unknown -> PhoneCallState.Idle
        }
    }

    private fun rideLynkObserver(rideLynkState: RideLynk.RideLynkState) {
        rideLynkIntercomState.asMutableLiveData()?.value = when (rideLynkState) {
            is RideLynk.RideLynkState.ActiveCall -> {
                activeRideLynkDevice.asMutableLiveData()?.value =
                    rideLynkState.buddy.asBuddyDevice
                RideLynkIntercomState.ActiveCall(rideLynkState.buddy.asBuddyDevice)
            }

            is RideLynk.RideLynkState.Connected -> {
                activeRideLynkDevice.asMutableLiveData()?.value =
                    rideLynkState.buddy.asBuddyDevice
                RideLynkIntercomState.Connected(rideLynkState.buddy.asBuddyDevice)
            }

            is RideLynk.RideLynkState.Discovery -> {
                activeRideLynkDevice.asMutableLiveData()?.value = null

//                val buddyList: ArrayList<BuddyDevice> =
//                    ArrayList(buddyDiscoveryList.asMutableLiveData()?.value ?: emptyList())
                Timber.e("RIDELYNK DEVICES: ${rideLynkState.buddies.map { it.macAddress }}")
                buddyDiscoveryList.asMutableLiveData()?.value = rideLynkState.buddies.map {
                    BuddyDevice(
                        R.drawable.sx20_small, it.name, it.macAddress, it
                    )
                }

                RideLynkIntercomState.Discovery
            }

            RideLynk.RideLynkState.Idle -> {
                activeRideLynkDevice.asMutableLiveData()?.value = null
                RideLynkIntercomState.Idle
            }

            is RideLynk.RideLynkState.Pairing -> {
                activeRideLynkDevice.asMutableLiveData()?.value = null
                RideLynkIntercomState.AutoPairing
            }

            RideLynk.RideLynkState.Unavailable -> {
                activeRideLynkDevice.asMutableLiveData()?.value = null
                RideLynkIntercomState.Unavailable
            }
        }
    }

    private fun rideGridObserver(rideGridState: RideGrid.RideGridState) {
        if (rideGridState == RideGrid.RideGridState.OFF)
            rideGridRiders.asMutableLiveData()?.value = emptyList()

        this.rideGridState.asMutableLiveData()?.value = when (rideGridState) {
            RideGrid.RideGridState.OFF -> RideGridIntercomState.OFF
            RideGrid.RideGridState.DISCOVERY -> RideGridIntercomState.DISCOVERY
            RideGrid.RideGridState.CONNECTED -> RideGridIntercomState.CONNECTED
            RideGrid.RideGridState.ACTIVE_CALL -> RideGridIntercomState.ACTIVE_CALL
            RideGrid.RideGridState.MUTED -> RideGridIntercomState.MUTED
            RideGrid.RideGridState.INCOMING_CALL -> RideGridIntercomState.INCOMING_CALL
            RideGrid.RideGridState.PRIVATE_CALL -> RideGridIntercomState.PRIVATE_CALL
        }

        this.rideGridStateFlow.asMutableStateFlow()?.tryEmit(
            when (rideGridState) {
                RideGrid.RideGridState.OFF -> RideGridIntercomState.OFF
                RideGrid.RideGridState.DISCOVERY -> RideGridIntercomState.DISCOVERY
                RideGrid.RideGridState.CONNECTED -> RideGridIntercomState.CONNECTED
                RideGrid.RideGridState.ACTIVE_CALL -> RideGridIntercomState.ACTIVE_CALL
                RideGrid.RideGridState.MUTED -> RideGridIntercomState.MUTED
                RideGrid.RideGridState.INCOMING_CALL -> RideGridIntercomState.INCOMING_CALL
                RideGrid.RideGridState.PRIVATE_CALL -> RideGridIntercomState.PRIVATE_CALL
            }
        )
    }

    private fun secondPhoneStateObserver(secondPhoneState: SecondPhone.SecondPhoneState) {
        this.secondPhoneState.asMutableLiveData()?.value = secondPhoneState
    }

    private fun testBytesObserver(bytes: ByteArray) {
//        Timber.e("BYTES DATA: ${bytes.byteToHexString()}")
    }

    private fun rideGridDevicesObserver(rideGridDevices: List<RideGridDevice>) {
        Timber.e("RIDE GRID APP STATE: $rideGridDevices")

        this.rideGridRidersFlow.asMutableStateFlow()?.tryEmit(rideGridDevices
            .map {
                RideGridUserDevice(
                    R.drawable.ic_helmet,
                    it.name,
                    it.macAddress,
                    it.distance,
                    it.nodeId,
                    it.directConnection,
                    it.bearing,
                    it.location
                )
            })

        rideGridRiders.asMutableLiveData()?.value = rideGridDevices
            .map {
                RideGridUserDevice(
                    R.drawable.ic_helmet,
                    it.name,
                    it.macAddress,
                    it.distance,
                    it.nodeId,
                    it.directConnection,
                    it.bearing,
                    it.location
                )
            }
    }

    private var sx20Ota: S20XOta? = null

    private fun onDeviceReady(sx20Ota: S20XOta) {
        this.sx20Ota = sx20Ota
        sx20Ota.firmwareUpdate.observe(this::firmwareUpdateStateObserver)
    }

    private fun onDeviceReady(sx20: S20X) {
        device = sx20
        deviceInfo.asMutableLiveData()?.value = DeviceInfo(
            R.drawable.sx20_small,
            R.string.model_sx20,
            sx20.deviceName ?: ""
        )

        sx20.otaSwitch.observe(this::otaSwitchState)
        sx20.deviceInfo.observe(this::deviceInfoObserver)
        sx20.volume.observe(this::volumeLevelObserver)
        sx20.volume.voicePromptLevel.observe(this::voicePromptLevelObserver)
        sx20.rideGrid.audioOverlayState.observe(this::musicOverlayLevelObserver)
        sx20.battery.observe(this::batteryObserver)
        sx20.mediaPlayer.observe(this::mediaPlayerObserver)
        sx20.dialer.observe(this::dialerObserver)
        sx20.voiceNoteRecorder.observe(this::voiceNoteRecorderObserver)

        sx20.speedDial.observe(this::speedDialObserver)

//        sx20.speedDial.refreshList()
        sx20.speedDial.speedDialContacts.observe(this::speedDialContactObserver)

        sx20.rideLynk.observe(this::rideLynkObserver)

        sx20.rideGrid.nearbyRiders.observe(this::rideGridDevicesObserver)

        sx20.rideGrid.observe(this::rideGridObserver)

        sx20.secondPhone.observe(this::secondPhoneStateObserver)

        sx20.testInterface.observe(this::testBytesObserver)

//        sx20.otaSwitch.updateOtaBinary(getBinary(R.raw.config))

    }

    override fun onBluArmorDeviceDiscovered(result: Set<AdvertisingBluArmorDevice>) {

    }

    override fun onConnectionStateChange(state: ConnectionState) {
//        Timber.e("APP STATE: ${state.javaClass.simpleName} , ${state.json()}")
        connectionState.asMutableLiveData()?.value = state
        when (state) {
            is ConnectionState.Connected -> discoveryState.asMutableLiveData()?.value =
                DiscoveryState.Connected(state.device.deviceName ?: "", R.drawable.sx20_small)

            is ConnectionState.Connecting -> discoveryState.asMutableLiveData()?.value =
                DiscoveryState.Connecting(state.device.deviceName ?: "", R.drawable.sx20_small)

            is ConnectionState.DeviceReady -> {
                bluArmorModel.asMutableLiveData()?.value = state.device.model
                discoveryState.asMutableLiveData()?.value =
                    DiscoveryState.Connected(state.device.deviceName ?: "", R.drawable.sx20_small)

                if (state.device.model == BluArmorModel.NO_DEVICE)
                    Unit
                else if (state.device.model.ota)
                    onDeviceReady(state.device as S20XOta)
                else
                    onDeviceReady(state.device as S20X)
                /*when (state.device.model) {
                    BluArmorModel.S10X -> onDeviceReady(state.device as S20X)
                    BluArmorModel.S10X_OTA -> onDeviceReady(state.device as S20XOta)
                    BluArmorModel.S20X -> onDeviceReady(state.device as S20X)
                    BluArmorModel.S20X_OTA -> onDeviceReady(state.device as S20XOta)
                    else -> Unit

                }*/
            }

            ConnectionState.Discovery -> discoveryState.asMutableLiveData()?.value =
                DiscoveryState.Scanning

            is ConnectionState.Disconnecting -> discoveryState.asMutableLiveData()?.value =
                DiscoveryState.Scanning

            is ConnectionState.Failed -> Unit
//                discoveryState.asMutableLiveData()?.value = DiscoveryState.Failed(state.device.deviceName ?: "", R.drawable.sx20_small)
            is ConnectionState.ScannerNotReady -> {
                when (state.reason) {
                    ConnectionState.ScannerNotReady.NotReadyReason.NEARBY_PERMISSION_NEEDED -> Unit
                    ConnectionState.ScannerNotReady.NotReadyReason.LOCATION_PERMISSION_NEEDED -> Unit
                    ConnectionState.ScannerNotReady.NotReadyReason.LOCATION_IS_OFF -> discoveryState.asMutableLiveData()?.value =
                        DiscoveryState.LocationOff

                    ConnectionState.ScannerNotReady.NotReadyReason.BLUETOOTH_IS_OFF -> discoveryState.asMutableLiveData()?.value =
                        DiscoveryState.BluetoothOff

                    ConnectionState.ScannerNotReady.NotReadyReason.BLUETOOTH_IN_BAD_STATE -> Unit
                }
            }
        }

    }

    override fun changeMusicLevelFocus(level: Float) {
        this.musicOverlayLevel.asMutableLiveData()?.value = level
        val audioOverlayState = when (this.audioOverlayState.value) {
            is RideGrid.AudioOverlayState.Off -> RideGrid.AudioOverlayState.Off(level.toInt())
            is RideGrid.AudioOverlayState.On -> RideGrid.AudioOverlayState.On(level.toInt())
            null -> RideGrid.AudioOverlayState.On(level.toInt())
        }
        device?.rideGrid?.changeMusicOverlayLevel(level.toInt())
    }

    override fun changeAudioOverlayFlag(enable: Boolean) {

        val audioOverlayState = when (enable) {
            true -> RideGrid.AudioOverlayState.On(this.audioOverlayState.value?.level ?: 3)
            false -> RideGrid.AudioOverlayState.Off(this.audioOverlayState.value?.level ?: 3)
        }

        device?.rideGrid?.changeMusicOverlayLevel(enable)
    }

    fun changeAudioOverlayFlag(voicePromptGainLevel: Volume.VoicePromptGainLevel) {
//        device?.volume?.changeVoicePromptLevel(voicePromptLevel)
        device?.volume?.changeVoicePromptGainLevel(voicePromptGainLevel)
    }


    fun changeVoicePromptVerbosity(voicePromptVerbosityLevel: Volume.VoicePromptVerbosityLevel) {
//        device?.volume?.changeVoicePromptLevel(voicePromptLevel)
        device?.volume?.changeVoicePromptVerbosityLevel(voicePromptVerbosityLevel)
    }

    override fun volumeAction(volumeControlAction: VolumeControlAction) {
        when (volumeControlAction) {
            is VolumeControlAction.ChangeEqualizerBoost -> {

                this.volumeState.asMutableLiveData()?.value = this.volumeState.value?.copy(
                    equalizerBassBoost = volumeControlAction.equalizerBassBoost
                )

                device?.volume?.changeEqualizerBassBoost(volumeControlAction.equalizerBassBoost)
            }

            is VolumeControlAction.ChangeVolume -> device?.volume?.changeVolume(volumeControlAction.volumeLevel.toInt())

            is VolumeControlAction.ChangeVolumeBoost -> {
                this.volumeState.asMutableLiveData()?.value = this.volumeState.value?.copy(
                    volumeBoost = volumeControlAction.volumeBoost
                )

                device?.volume?.changeVolumeBoost(volumeControlAction.volumeBoost)
            }

            is VolumeControlAction.ToggleDynamicVolume -> {
                this.volumeState.asMutableLiveData()?.value = this.volumeState.value?.copy(
                    dynamicMode = volumeControlAction.enable
                )

                if (volumeControlAction.enable) device?.volume?.enableDynamicMode()
                else device?.volume?.disableDynamicMode()
            }
        }
    }

    override fun musicAction(musicAction: MusicAction) {
        when (musicAction) {
            MusicAction.PLAY -> device?.mediaPlayer?.playMusic()
            MusicAction.PAUSE -> device?.mediaPlayer?.pauseMusic()
            MusicAction.NEXT -> device?.mediaPlayer?.nextMusic()
            MusicAction.PREVIOUS -> device?.mediaPlayer?.previousMusic()
        }
    }

    @SuppressLint("MissingPermission")
    override fun voiceNoteRecorderAction(action: VoiceNoteRecorderActions) {
        when (action) {
            VoiceNoteRecorderActions.START -> device?.voiceNoteRecorder?.startRecord()
            VoiceNoteRecorderActions.STOP -> device?.voiceNoteRecorder?.stopRecord()
            VoiceNoteRecorderActions.TERMINATE -> Unit
        }
    }

    override fun phoneCallAction(phoneCallAction: PhoneCallAction) {
        when (phoneCallAction) {
            PhoneCallAction.ANSWER -> device?.dialer?.acceptPhoneCall()
            PhoneCallAction.DROP -> device?.dialer?.endPhoneCall()
            PhoneCallAction.REJECT -> device?.dialer?.rejectPhoneCall()
        }
    }

    override fun rideLynkAction(rideLynkAction: RideLynkAction) {
        when (rideLynkAction) {
            is RideLynkAction.Connect -> with(
                rideLynkAction.buddyDevice.buddy ?: return
            ) { device?.rideLynk?.connect(this) }

            RideLynkAction.Disconnect -> device?.rideLynk?.disconnect()
            RideLynkAction.StartDiscovery -> {
                buddyDiscoveryList.asMutableLiveData()?.value = emptyList()
                device?.rideLynk?.startDiscovery()
            }

            RideLynkAction.StopDiscovery -> device?.rideLynk?.stopDiscovery()
            RideLynkAction.StartCall -> device?.rideLynk?.connectCall()
            RideLynkAction.StopCall -> device?.rideLynk?.disconnectCall()
        }
    }

    override fun rideGridAction(rideGridIntercomAction: RideGridIntercomAction) {
        when (rideGridIntercomAction) {
            RideGridIntercomAction.Mute -> device?.rideGrid?.muteCall()
            RideGridIntercomAction.StartCall -> device?.rideGrid?.startRideGridCall()
            RideGridIntercomAction.StopCall -> device?.rideGrid?.stopRideGridCall()
            RideGridIntercomAction.UnMute -> device?.rideGrid?.unMuteCall()
            RideGridIntercomAction.TurnOff -> device?.rideGrid?.turnOff()
            RideGridIntercomAction.TurnOn -> device?.rideGrid?.turnOn()
        }
    }

    override fun invokeVoiceAssistant() = device?.voiceAssistant?.invokeAssistant() ?: Unit

    override fun switchToOtaMode() = device?.otaSwitch?.switchToOtaMode() ?: Unit

    override fun turnOnBluetooth() = this.turnOnBluetooth.invoke(context, true)
    override fun turnOnGps() {}

    override fun exportLog() {
        exportLogs.invoke()
    }

    override fun dialSpeedDial(buddy: Buddy) {
        device?.speedDial?.dial(buddy)
    }

    override fun updateSpeedDialList(buddies: Array<Buddy>) {

        device?.speedDial?.updateList(buddies)

        GlobalScope.launch {
            device?.speedDialDataSource?.updateList(buddies)
//            buddyListDataSource.updateSpeedDialList(speedDialList)
        }
    }

    override suspend fun createRideGridGroup(
        rideGridGroupName: String,
        isPrimary: Boolean
    ): RideGridGroup? {
        val peripheral = Peripheral.build(device ?: return null)
        return RideGridGroup.build(
            device?.rideGridGroupDataSource?.create(
                com.aptener.bluconnect.repo.RideGridGroup.build(
                    rideGridGroupName,
                    isPrimary,
                    peripheral
                )
            ) ?: return null
        )
    }

    override suspend fun joinRideGridGroup(
        rideGridGroupInvitation: RideGridGroupInvitation,
        isPrimary: Boolean
    ): RideGridGroup? {
//        Timber.e("CREATE GROUP: 3 ${this?.json()}")


        val newGroup = device?.rideGridGroupDataSource?.join(rideGridGroupInvitation, isPrimary)
//        Timber.e("CREATE GROUP: 3 ${newGroup?.json()}, ${newGroup != null && isPrimary}")
//        withContext(Dispatchers.IO) {
        if (newGroup != null && isPrimary) {
            device?.rideGrid?.setActiveGridGroupId(newGroup.groupId)
//                device?.rideGridGroupDataSource?.setActiveGroup(newGroup)
        }//            Timber.e("CREATE GROUP: 2 ${rideGridGroup?.json()}")
        return RideGridGroup.build(newGroup ?: return null)
    }

    override fun deleteRideGridGroup(rideGridGroup: RideGridGroup) {

        GlobalScope.launch {
            val group =
                device?.rideGridGroupDataSource?.getRideGridGroupByGroupId(rideGridGroup.groupId)
                    ?: return@launch
            device?.rideGridGroupDataSource?.delete(group)
            if (rideGridGroup.isPrimary)
                device?.rideGrid?.setActiveGridGroupId("FFFF")
        }
    }

    override fun markRideGridGroupPrimary(rideGridGroup: RideGridGroup) {
//        Timber.e("MARK PRIMARY: ${rideGridGroup.json()}")
        GlobalScope.launch {
            if (rideGridGroup.groupId.uppercase() == "FFFF") {
                device?.rideGridGroupDataSource?.removeActiveGroup()
                device?.rideGrid?.setActiveGridGroupId(rideGridGroup.groupId)
            } else {
                val group =
                    device?.rideGridGroupDataSource?.getRideGridGroupByGroupId(rideGridGroup.groupId)
                        ?: return@launch
                device?.rideGridGroupDataSource?.setActiveGroup(group)
                device?.rideGrid?.setActiveGridGroupId(rideGridGroup.groupId)
            }
        }
    }

    override suspend fun getRideGridGroupById(rideGridGroupId: String): RideGridGroup? {
        return RideGridGroup.build(
            device?.rideGridGroupDataSource?.getRideGridGroupByGroupId(rideGridGroupId)
                ?: return null
        )
    }

    override suspend fun encodeRideGridGroupData(rideGridGroup: RideGridGroup): String? {
        val group =
            device?.rideGridGroupDataSource?.getRideGridGroupByGroupId(rideGridGroup.groupId)
                ?: return null
        return device?.rideGridGroupDataSource?.encodeToQrCodeData(group)
    }

    override fun decodeRideGridGroupData(rideGridGroupData: String): RideGridGroup? {
        TODO("Not yet implemented")
        device?.musicGrid?.enablePartyMode(MusicGrid.MusicGridApps.SPOTIFY)
    }

    override suspend fun encodeBuddyData(buddy: Buddy): String? =
        device?.buddiesDataSource?.encodeToQrCodeData(buddy)

    override fun decodeBuddyData(buddyData: String): Buddy? {
        TODO("Not yet implemented")
    }

    override suspend fun getActiveBuddy(): Buddy? = device?.buddiesDataSource?.getActiveBuddy()

    var phoneCallPreferenceOption: PhoneCallPreferenceOption
        get() = this.sdkPrefDataSource.phoneCallPreference
        set(value) {
            this.sdkPrefDataSource.phoneCallPreference = value
        }

    var volumeBoostPreferenceOption: VolumeBoostPreferenceOption
        get() = this.sdkPrefDataSource.volumeBoostPreference
        set(value) {
            this.sdkPrefDataSource.volumeBoostPreference = value
        }

    var equalizerPreferenceOption: EqualizerPreferenceOption
        get() = this.sdkPrefDataSource.equalizerPreferenceOption
        set(value) {
            this.sdkPrefDataSource.equalizerPreferenceOption = value
        }

    var audioWeaveMusicDucking: Boolean
        get() = this.sdkPrefDataSource.audioWeaveMusicDucking
        set(value) {
            this.sdkPrefDataSource.audioWeaveMusicDucking = value
        }

    var audioWeaveMusicDuckingLevel: Int
        get() = this.sdkPrefDataSource.audioWeaveMusicDuckingLevel
        set(value) {
            this.sdkPrefDataSource.audioWeaveMusicDuckingLevel = value
        }


    var whatsappAutoRead: Boolean
        get() = this.sdkPrefDataSource.whatsappAutoRead
        set(value) {
            this.sdkPrefDataSource.whatsappAutoRead = value
        }


    var whatsappAutoReply: Boolean
        get() = this.sdkPrefDataSource.whatsappAutoReply
        set(value) {
            this.sdkPrefDataSource.whatsappAutoReply = value
        }


    var whatsappDefaultMsg: String
        get() = this.sdkPrefDataSource.whatsappDefaultMsg
        set(value) {
            this.sdkPrefDataSource.whatsappDefaultMsg = value
        }

    private val Buddy.asBuddyDevice: BuddyDevice
        get() {
            return BuddyDevice(
                R.drawable.ic_helmet,
                this.name,
                this.macAddress
            )
        }
}
