package com.aptener.bluarmor.service.bluconnect

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.IntRange
import androidx.annotation.RequiresApi
import androidx.annotation.StringRes
import androidx.core.app.NotificationManagerCompat
import com.aptener.bluarmor.MainActivity
import com.aptener.bluarmor.R
import com.aptener.bluarmor.service.BluService

class DeviceStateNotification {

    companion object {
        const val NOTIFICATION_ID = 1001
        const val NOTIFICATION_ACTION = "1001"
         const val NOTIFICATION_ACTION_KEY = "NOTIFICATION_ACTION_KEY"
         const val NOTIFICATION_ACTION_EXIT = "EXIT"
        private const val NOTIFICATION_ACTION_OK = "OK"
        private const val NOTIFICATION_ACTION_CANCEL = "CANCEL"
    }

    private fun notificationActionIntent(context: Context) =
        Intent(NOTIFICATION_ACTION, null, context, BluService::class.java)

    @RequiresApi(Build.VERSION_CODES.O)
    private fun exitNotificationPendingIntent(context: Context) =
        PendingIntent.getForegroundService(
            context,
            0,
            notificationActionIntent(context).apply {
                putExtra(
                    NOTIFICATION_ACTION_KEY,
                    NOTIFICATION_ACTION_EXIT
                )
            },
            PendingIntent.FLAG_IMMUTABLE
        )

    @RequiresApi(Build.VERSION_CODES.O)
    private fun exitNotificationOkPendingIntent(context: Context) =
        PendingIntent.getForegroundService(
            context,
            0,
            notificationActionIntent(context).apply {
                putExtra(
                    NOTIFICATION_ACTION_KEY,
                    NOTIFICATION_ACTION_OK
                )
            },
            PendingIntent.FLAG_IMMUTABLE
        )

    @RequiresApi(Build.VERSION_CODES.O)
    private fun exitNotificationCancelPendingIntent(context: Context) =
        PendingIntent.getForegroundService(
            context,
            0,
            notificationActionIntent(context).apply {
                putExtra(
                    NOTIFICATION_ACTION_KEY,
                    NOTIFICATION_ACTION_CANCEL
                )
            },
            PendingIntent.FLAG_IMMUTABLE
        )

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(context: Context) {
        with(context.getSystemService(Service.NOTIFICATION_SERVICE) as NotificationManager) {
            createNotificationChannel(
                NotificationChannel(
                    context.getString(R.string.channel_device_id),
                    context.getString(R.string.channel_device),
                    NotificationManager.IMPORTANCE_LOW
                ).apply { description = context.getString(R.string.channel_device_description) }
            )
        }
    }

    private fun getDefaultPendingIntent(context: Context) =
        PendingIntent.getActivity(
            context,
            0,
            Intent(context, MainActivity::class.java),
            PendingIntent.FLAG_IMMUTABLE
        )

    @RequiresApi(Build.VERSION_CODES.O)
    fun getNotificationBuilder(context: Context): Notification.Builder {
        createNotificationChannel(context)

        return Notification.Builder(context, context.getString(R.string.channel_device_id))
            .setCategory(Notification.CATEGORY_SERVICE)
            .setSmallIcon(R.drawable.ic_logo_inverted)
            .setContentIntent(getDefaultPendingIntent(context))
            .setTicker(context.getString(R.string.app_name))
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun updateState(context: Context, notificationDeviceState: NotificationDeviceState) {
        val notificationBuilder = getNotificationBuilder(context)

        with(NotificationManagerCompat.from(context)) {
            notify(
                NOTIFICATION_ID,
                notificationDeviceState.updateNotification(context, notificationBuilder)
            )
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun connected(context: Context, deviceModel: String) =
        updateState(
            context,
            NotificationDeviceState.Connected(deviceModel, exitNotificationPendingIntent(context))
        )


    @RequiresApi(Build.VERSION_CODES.O)
    fun disconnected(context: Context) =
        updateState(
            context,
            NotificationDeviceState.Disconnect(exitNotificationPendingIntent(context))
        )

    @RequiresApi(Build.VERSION_CODES.O)
    fun updateBattery(context: Context, deviceModel: String, batteryLevel: Int) =
        updateState(
            context,
            NotificationDeviceState.BatteryUpdate(
                deviceModel,
                batteryLevel,
                exitNotificationPendingIntent(context)
            )
        )

    @RequiresApi(Build.VERSION_CODES.O)
    fun exitNotification(context: Context, deviceModel: String?) =
        updateState(
            context,
            NotificationDeviceState.ExitService(
                deviceModel,
                exitNotificationOkPendingIntent(context),
                exitNotificationCancelPendingIntent(context)
            )
        )

    sealed class NotificationDeviceState(
        @StringRes val title: Int,
        @StringRes val description: Int
    ) {
        data class Disconnect(val exitPendingIntent: PendingIntent) :
            NotificationDeviceState(R.string.app_name, R.string.channel_device_disconnected) {
            override fun updateNotification(
                context: Context,
                notificationBuilder: Notification.Builder
            ): Notification {
                val notificationStyle = Notification.BigTextStyle()

                val title = context.getString(title)
                val description = context.getString(description)

                notificationBuilder.setContentTitle(title)
                notificationBuilder.setContentText(description)

                notificationStyle.setBigContentTitle(title)
                notificationStyle.bigText(description)

                notificationBuilder.style = notificationStyle

                notificationBuilder.addAction(
                    Notification.Action.Builder(
                        null,
                        context.getString(R.string.exit),
                        exitPendingIntent
                    ).build()
                )
                return notificationBuilder.build()
            }
        }

        data class Connected(val deviceModel: String, val exitPendingIntent: PendingIntent) :
            NotificationDeviceState(R.string.channel_device_connected, R.string.have_safe_ride) {
            override fun updateNotification(
                context: Context,
                notificationBuilder: Notification.Builder
            ): Notification {
                val notificationStyle = Notification.BigTextStyle()

                val title = context.getString(title, deviceModel)
                val description = context.getString(description)

                notificationBuilder.setContentTitle(title)
                notificationBuilder.setContentText(description)

                notificationStyle.setBigContentTitle(title)
                notificationStyle.bigText(description)

                notificationBuilder.style = notificationStyle

                notificationBuilder.addAction(
                    Notification.Action.Builder(
                        null,
                        context.getString(R.string.exit),
                        exitPendingIntent
                    ).build()
                )
                return notificationBuilder.build()
            }
        }

        data class BatteryUpdate(
            val deviceModel: String,
            @IntRange(from = 0, to = 100) val batteryLevel: Int,
            val exitPendingIntent: PendingIntent
        ) :
            NotificationDeviceState(R.string.channel_device_connected, R.string.have_safe_ride) {
            override fun updateNotification(
                context: Context,
                notificationBuilder: Notification.Builder
            ): Notification {
                val notificationStyle = Notification.BigTextStyle()

                val title = context.getString(title, deviceModel)
                val description = context.getString(description)

                notificationBuilder.setContentTitle(title)
                notificationBuilder.setContentText(description)

                notificationStyle.setBigContentTitle(title)
                notificationStyle.bigText(description)

                notificationStyle.setSummaryText(
                    context.getString(
                        R.string.channel_device_battery,
                        batteryLevel
                    )
                )

                notificationBuilder.style = notificationStyle

                notificationBuilder.addAction(
                    Notification.Action.Builder(
                        null,
                        context.getString(R.string.exit),
                        exitPendingIntent
                    ).build()
                )
                return notificationBuilder.build()
            }
        }


        data class ExitService(
            val deviceModel: String?,
            val positiveIntent: PendingIntent,
            val negativeIntent: PendingIntent
        ) :
            NotificationDeviceState(
                R.string.channel_device_notification_exit_title,
                if (deviceModel != null)
                    R.string.channel_device_exit_desc_connected
                else
                    R.string.channel_device_exit_desc_disconnected
            ) {
            override fun updateNotification(
                context: Context,
                notificationBuilder: Notification.Builder
            ): Notification {
                val notificationStyle = Notification.BigTextStyle()

                val title = context.getString(title)
                val description = if (deviceModel == null)
                    context.getString(description)
                else context.getString(description, deviceModel)

                notificationBuilder.setContentTitle(title)
                notificationBuilder.setContentText(description)

                notificationStyle.setBigContentTitle(title)
                notificationStyle.bigText(description)

                notificationBuilder.style = notificationStyle

                notificationBuilder.addAction(
                    Notification.Action.Builder(
                        null,
                        context.getString(R.string.ok),
                        positiveIntent
                    ).build()
                )
                notificationBuilder.addAction(
                    Notification.Action.Builder(
                        null,
                        context.getString(R.string.cancel),
                        negativeIntent
                    ).build()
                )

                return notificationBuilder.build()
            }
        }


        abstract fun updateNotification(
            context: Context,
            notificationBuilder: Notification.Builder
        ): Notification
    }
}