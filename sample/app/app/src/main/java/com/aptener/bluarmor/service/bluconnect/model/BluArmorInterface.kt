package com.aptener.bluarmor.service.bluconnect.model

import com.aptener.bluarmor.service.bluconnect.BluConnectManager

interface BluArmorInterface {
    val bluConnectManager: BluConnectManager
}
