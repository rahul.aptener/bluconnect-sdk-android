package com.aptener.bluarmor.service.bluconnect.model

import com.aptener.bluconnect.repo.Buddy

sealed class BluArmorSpeedDialState {
    object Idle : BluArmorSpeedDialState()
    data class Active(val buddy: Buddy?) : BluArmorSpeedDialState()
}