package com.aptener.bluarmor.service.bluconnect.model

import androidx.annotation.DrawableRes
import com.aptener.bluconnect.repo.Buddy

data class BuddyDevice(
    @DrawableRes val imageRes: Int,
    val name: String,
    val macAddress: String,
    val buddy: Buddy? = null
)
