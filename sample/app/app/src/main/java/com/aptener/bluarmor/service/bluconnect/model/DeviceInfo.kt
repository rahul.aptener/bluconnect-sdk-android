package com.aptener.bluarmor.service.bluconnect.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class DeviceInfo(
    @DrawableRes val deviceDrawableRes: Int,
    @StringRes val modelName: Int,
    val deviceName: String
)
