package com.aptener.bluarmor.service.bluconnect.model

import androidx.annotation.DrawableRes

sealed class DiscoveryState {
    object Scanning : DiscoveryState()
    object BluetoothOff : DiscoveryState()
    object LocationOff : DiscoveryState()
    data class Connecting(val deviceName: String, @DrawableRes val deviceImage: Int) :
        DiscoveryState()

    data class Connected(val deviceName: String, @DrawableRes val deviceImage: Int) :
        DiscoveryState()
//    data class Failed(val deviceName: String, @DrawableRes val deviceImage: Int) : DiscoveryState()
}
