package com.aptener.bluarmor.service.bluconnect.model

enum class MusicAction {
    PLAY, PAUSE, NEXT, PREVIOUS
}
