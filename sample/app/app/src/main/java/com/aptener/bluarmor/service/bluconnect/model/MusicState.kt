package com.aptener.bluarmor.service.bluconnect.model

sealed class MusicState {
    object Idle : MusicState()
    data class Playing(val title: String, val artist: String) : MusicState()
}
