package com.aptener.bluarmor.service.bluconnect.model

enum class PhoneCallAction { ANSWER, DROP, REJECT }
