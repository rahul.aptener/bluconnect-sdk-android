package com.aptener.bluarmor.service.bluconnect.model

import android.icu.util.Calendar

sealed class PhoneCallState {
    object Idle : PhoneCallState()
    data class Incoming(val number: String, val contactName: String) : PhoneCallState()
    data class Outgoing(val number: String, val contactName: String) : PhoneCallState()
    data class Active(val number: String, val contactName: String) : PhoneCallState()
    data class MissedCall(val number: String, val contactName: String, val dateTime: Calendar) :
        PhoneCallState()
}
