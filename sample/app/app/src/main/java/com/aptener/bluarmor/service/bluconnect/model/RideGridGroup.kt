package com.aptener.bluarmor.service.bluconnect.model

data class RideGridGroup(
    val id: String,
    val groupId: String,
    val name: String,
    val isPrimary: Boolean,
    val firstCreated: Long,
    val inviteCode: String?
) {
    companion object {
        fun build(rideGridGroup: com.aptener.bluconnect.repo.RideGridGroup): RideGridGroup = RideGridGroup(
            rideGridGroup.id,
            rideGridGroup.groupId,
            rideGridGroup.name,
            rideGridGroup.active,
            rideGridGroup.joinedAt,
            rideGridGroup.inviteCode
        )

        fun buildFromQrCode(link: String): RideGridGroup? {
            val matchResult = "^https://ridegrid\\.page\\.link/(\\w+)\$".toRegex()
                .matchEntire(link)?.groupValues?.get(1) ?: return null
            return build(com.aptener.bluconnect.repo.RideGridGroup.build(matchResult) ?: return null)
        }

        /*    fun build(inviteLink: String): RideGridGroup? {
                //https://ridegrid.page.link
                val matchResult = "^https://ridegrid\\.page\\.link/(\\w+)\$".toRegex()
                    .matchEntire(inviteLink)?.groupValues?.get(1)?.hexToAscii()?.split("|") ?: return null

    //            Timber.e("TEST $matchResult")
                if (matchResult.size != 2)
                    return null

                return RideGridGroup(
                    0,
                    URLDecoder.decode(matchResult[0], "utf-8"),
                    URLDecoder.decode(matchResult[1], "utf-8"),
                    isPrimary = false,
                    System.currentTimeMillis()
                )
            }
    */
        /* fun build_back(inviteLink: String): RideGridGroup? {
             val matchResult = "^https://thebluarmor\\.com/(\\d+)/(.+)\$".toRegex()
                 .matchEntire(inviteLink)?.groupValues ?: return null

             if (matchResult.size != 3)
                 return null

             return RideGridGroup(
                 0,
                 URLDecoder.decode(matchResult[1], "utf-8"),
                 URLDecoder.decode(matchResult[2], "utf-8"),
                 isPrimary = false,
                 System.currentTimeMillis()
             )
         }*/
    }
}
