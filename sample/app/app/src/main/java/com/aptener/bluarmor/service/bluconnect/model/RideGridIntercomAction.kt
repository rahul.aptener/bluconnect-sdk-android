package com.aptener.bluarmor.service.bluconnect.model
sealed class RideGridIntercomAction {
    object StartCall : RideGridIntercomAction()
    object StopCall : RideGridIntercomAction()
    object Mute : RideGridIntercomAction()
    object UnMute : RideGridIntercomAction()
    object TurnOn : RideGridIntercomAction()
    object TurnOff : RideGridIntercomAction()
}

