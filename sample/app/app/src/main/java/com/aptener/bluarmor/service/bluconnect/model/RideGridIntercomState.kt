package com.aptener.bluarmor.service.bluconnect.model

enum class RideGridIntercomState {
    OFF,
    DISCOVERY,
    CONNECTED,
    ACTIVE_CALL,
    MUTED,
    INCOMING_CALL,
    PRIVATE_CALL,
}
