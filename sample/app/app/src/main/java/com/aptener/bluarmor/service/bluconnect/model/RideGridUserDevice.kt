package com.aptener.bluarmor.service.bluconnect.model

import android.location.Location
import androidx.annotation.DrawableRes

data class RideGridUserDevice(
    @DrawableRes val imageRes: Int,
    val name: String,
    val macAddress: String,
    // meters
    val distance: Int,
    val nodeId: Int,
    val directChild: Boolean,
    val bearing: Int,
    val location: Location
) {
//    override fun equals(other: Any?): Boolean {
//        if (other is RideGridUserDevice)
//            return other.nodeId == nodeId || other.name == other.name
//        return super.equals(other)
//    }
}
