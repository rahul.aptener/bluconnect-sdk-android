package com.aptener.bluarmor.service.bluconnect.model

sealed class RideLynkAction {
    object StartDiscovery : RideLynkAction()
    object StopDiscovery : RideLynkAction()
    object Disconnect : RideLynkAction()
    object StartCall : RideLynkAction()
    object StopCall : RideLynkAction()
    data class Connect(val buddyDevice: BuddyDevice) : RideLynkAction()
}
