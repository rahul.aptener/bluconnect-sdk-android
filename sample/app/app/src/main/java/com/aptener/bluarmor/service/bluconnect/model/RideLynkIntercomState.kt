package com.aptener.bluarmor.service.bluconnect.model

sealed class RideLynkIntercomState {
    object Idle : RideLynkIntercomState()
    object Unavailable : RideLynkIntercomState()
    object AutoPairing : RideLynkIntercomState()
    object Discovery : RideLynkIntercomState()
    data class Connected(val buddyDevice: BuddyDevice) : RideLynkIntercomState()
    data class ActiveCall(val buddyDevice: BuddyDevice) : RideLynkIntercomState()
}
