package com.aptener.bluarmor.service.bluconnect.model

import androidx.lifecycle.LiveData
//import com.aptener.bluarmor.ui.app.preference.PreferenceViewModelInterface
//import com.aptener.bluconnect.app.device.sx.usecase.voicenoteCtrl.VoiceNoteRecorderActions
import com.aptener.bluconnect.device.sxcontrol.*
import com.aptener.bluconnect.device.sxcontrol.VoiceNoteRecorder.*
import com.aptener.bluconnect.repo.Buddy
import com.aptener.bluconnect.repo.RideGridGroupInvitation
import kotlinx.coroutines.flow.StateFlow
import java.lang.Thread.State

interface Sx20Interface {
    val otaSwitchState: LiveData<OtaSwitch.OtaSwitchState>
    val discoveryState: LiveData<DiscoveryState>
    val deviceInfo: LiveData<DeviceInfo>
    val deviceDetails: LiveData<com.aptener.bluconnect.device.sxcontrol.DeviceInfo.DeviceDetails>
    val batteryLevel: LiveData<Int>
    val chargingState: LiveData<Battery.ChargingState>
    val audioOverlayState: LiveData<RideGrid.AudioOverlayState>
    val musicOverlayLevel: LiveData<Float>
    val volumeState: LiveData<Volume.VolumeLevel>
    val voicePromptLevel: LiveData<Volume.VoicePromptVerbosityLevel>
    val musicState: LiveData<MusicState>
    val voiceNoteRecorderState: LiveData<VoiceNoteRecorderState>
    val phoneCallState: LiveData<PhoneCallState>

    var activeRideLynkDevice: LiveData<BuddyDevice?>
    val buddyRidersList: LiveData<List<BuddyDevice>>
    val buddyDiscoveryList: LiveData<List<BuddyDevice>>
    val rideGridRiders: LiveData<List<RideGridUserDevice>>
    val rideLynkIntercomState: LiveData<RideLynkIntercomState>
    val rideGridState: LiveData<RideGridIntercomState>
    val rideGridStateFlow: StateFlow<RideGridIntercomState>

    val secondPhoneState: LiveData<SecondPhone.SecondPhoneState>

    val rideGridGroups: LiveData<List<RideGridGroup>>
    val buddies: LiveData<List<Buddy>>

    val speedDialState: LiveData<SpeedDial.SpeedDialState>
    val speedDialContacts: LiveData<Array<Buddy?>>

    val otaProgress: LiveData<Int>
    val rideGridOtaProgress: LiveData<Int>

    fun volumeAction(volumeControlAction: VolumeControlAction)
    fun changeMusicLevelFocus(level: Float)
    fun changeAudioOverlayFlag(enable: Boolean)
    fun musicAction(musicAction: MusicAction)
    fun voiceNoteRecorderAction(action: VoiceNoteRecorderActions)
    fun phoneCallAction(phoneCallAction: PhoneCallAction)
    fun rideLynkAction(rideLynkAction: RideLynkAction)
    fun rideGridAction(rideGridIntercomAction: RideGridIntercomAction)
    fun turnOnBluetooth()
    fun turnOnGps()
    fun exportLog()
    fun invokeVoiceAssistant()
    fun switchToOtaMode()
    fun dialSpeedDial(buddy: Buddy)
    fun updateSpeedDialList(buddies: Array<Buddy>)

    fun deleteRideGridGroup(rideGridGroup: RideGridGroup)
    fun markRideGridGroupPrimary(rideGridGroup: RideGridGroup)
    suspend fun joinRideGridGroup(rideGridGroupInvitation: RideGridGroupInvitation,isPrimary: Boolean): RideGridGroup?
    suspend fun createRideGridGroup(rideGridGroupName: String, isPrimary: Boolean): RideGridGroup?
    suspend fun getRideGridGroupById(rideGridGroupId: String): RideGridGroup?
    suspend fun encodeRideGridGroupData(rideGridGroup: RideGridGroup): String?
    fun decodeRideGridGroupData(rideGridGroupData: String): RideGridGroup?

    suspend fun encodeBuddyData(buddy: Buddy): String?
    fun decodeBuddyData(buddyData: String): Buddy?

    suspend fun getActiveBuddy(): Buddy?
}
