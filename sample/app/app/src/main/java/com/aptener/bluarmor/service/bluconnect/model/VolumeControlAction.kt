package com.aptener.bluarmor.service.bluconnect.model

import androidx.annotation.IntRange
import com.aptener.bluconnect.device.sxcontrol.Volume

sealed class VolumeControlAction {
    data class ToggleDynamicVolume(val enable: Boolean) : VolumeControlAction()
    data class ChangeVolume(@IntRange(from = 0, to = 15) val volumeLevel: Int) :
        VolumeControlAction()

    data class ChangeVolumeBoost(val volumeBoost: Volume.VolumeBoost) : VolumeControlAction()
    data class ChangeEqualizerBoost(val equalizerBassBoost: Volume.EqualizerBassBoost) :
        VolumeControlAction()
}
