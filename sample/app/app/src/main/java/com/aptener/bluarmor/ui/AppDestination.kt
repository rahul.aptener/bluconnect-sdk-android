package com.aptener.bluarmor.ui

import com.aptener.bluarmor.ui.util.NavControllerDestination

sealed class AppDestination(private val _route: String) : NavControllerDestination {
    object LandingDest : AppDestination("landing")

    object DiscoveryDest : AppDestination("discovery")
    object DashboardDest : AppDestination("dashboard")

    object PreferenceDest : AppDestination("preference")

    object PermissionDest : AppDestination("permission")

    object OtaDest : AppDestination("ota")

    override val route: String get() = "ad_$_route"
}