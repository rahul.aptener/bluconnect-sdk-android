package com.aptener.bluarmor.ui.app.buddylist

import androidx.compose.animation.*
import androidx.compose.animation.core.tween
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.imageResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.aptener.bluarmor.R
import com.aptener.bluarmor.service.bluconnect.model.*
import com.aptener.bluarmor.ui.app.ridegrid.EditText
import com.aptener.bluarmor.ui.theme.*
import com.aptener.bluarmor.ui.util.EdgeCard
import com.aptener.bluarmor.ui.util.loadPicture
import com.aptener.bluconnect.device.sxcontrol.SpeedDial
import com.aptener.bluconnect.repo.Buddy
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject

@AssistedFactory
interface BuddyDiscoveryViewModelAssistedFactory {
    fun create(bluArmorInterface: BluArmorInterface): BuddyDiscoveryViewViewModel
}


val buddyDiscoveryViewInterface = object : BuddyDiscoveryViewInterface {
    override val activeDevice: LiveData<BuddyDevice?>
        get() = MutableLiveData()
    override val devices: LiveData<List<BuddyDevice>> = MutableLiveData()
    override val deviceInfo: LiveData<DeviceInfo>
        get() = MutableLiveData()
    override val batteryLevel: LiveData<Int>
        get() = MutableLiveData()
    override val rideLynkIntercomState: LiveData<RideLynkIntercomState>
        get() = MutableLiveData()
    override val speedDialState: LiveData<SpeedDial.SpeedDialState>
        get() = MutableLiveData()
    override val speedDialContacts: LiveData<Array<Buddy?>>
        get() = MutableLiveData()

    override fun rideLynkAction(rideLynkAction: RideLynkAction) {
        Unit
    }

    override fun dialSpeedDial(buddy: Buddy) {

    }
}

interface BuddyDiscoveryViewInterface {
    val activeDevice: LiveData<BuddyDevice?>
    val devices: LiveData<List<BuddyDevice>>
    val deviceInfo: LiveData<DeviceInfo>
    val batteryLevel: LiveData<Int>
    val rideLynkIntercomState: LiveData<RideLynkIntercomState>

    val speedDialState: LiveData<SpeedDial.SpeedDialState>
    val speedDialContacts: LiveData<Array<Buddy?>>

    fun rideLynkAction(rideLynkAction: RideLynkAction)
    fun dialSpeedDial(buddy: Buddy)
}

class BuddyDiscoveryViewViewModel @AssistedInject constructor(@Assisted private val bluArmorInterface: BluArmorInterface) : ViewModel(),
    BuddyDiscoveryViewInterface {
    override val activeDevice: LiveData<BuddyDevice?> = bluArmorInterface.bluConnectManager.activeRideLynkDevice
    override val devices: LiveData<List<BuddyDevice>> = bluArmorInterface.bluConnectManager.buddyDiscoveryList
    override val deviceInfo: LiveData<DeviceInfo> = bluArmorInterface.bluConnectManager.deviceInfo
    override val batteryLevel: LiveData<Int> = bluArmorInterface.bluConnectManager.batteryLevel
    override val rideLynkIntercomState: LiveData<RideLynkIntercomState> = bluArmorInterface.bluConnectManager.rideLynkIntercomState

    override val speedDialContacts: LiveData<Array<Buddy?>> = bluArmorInterface.bluConnectManager.speedDialContacts
    override val speedDialState: LiveData<SpeedDial.SpeedDialState> = bluArmorInterface.bluConnectManager.speedDialState

    override fun rideLynkAction(rideLynkAction: RideLynkAction) = bluArmorInterface.bluConnectManager.rideLynkAction(rideLynkAction)
    override fun dialSpeedDial(buddy: Buddy) = bluArmorInterface.bluConnectManager.dialSpeedDial(buddy)

    class Factory(
        private val assistedFactory: BuddyDiscoveryViewModelAssistedFactory,
        private val bluArmorInterface: BluArmorInterface,
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return assistedFactory.create(bluArmorInterface) as T
        }
    }
}

@Composable
fun BuddyDiscoveryView(
    navController: NavController,
    vm: BuddyDiscoveryViewInterface,
) {
    val activeDevice: BuddyDevice? by vm.activeDevice.observeAsState(BuddyDevice(R.drawable.ic_helmet, "Name", "MAC"))
    val devices: List<BuddyDevice> by vm.devices.observeAsState(listOf())
    val rideLynkIntercomState: RideLynkIntercomState by vm.rideLynkIntercomState.observeAsState(RideLynkIntercomState.Idle)

    BluArmorTheme {
        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {

            TopAppBar(
                backgroundColor = MaterialTheme.colors.background,
                elevation = 0.dp,
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween,
                ) {
                    Image(
                        imageVector = ImageVector.vectorResource(R.drawable.ic_back_nav),
                        contentDescription = null,
                        modifier = Modifier
                            .padding(0.dp)
                            .clickable {
                                navController.popBackStack()
                            }
                    )
                }
            }

            DeviceInfoCardCollapsed(
                vm.deviceInfo,
                vm.batteryLevel
            )

            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 12.dp)
                        .weight(1f),
                    text = "Available Riders".uppercase(),
                    style = MaterialTheme.typography.subtitle2,
                    color = Color.White,
                    fontWeight = FontWeight.SemiBold
                )

                Button(
                    onClick = {
                        if (rideLynkIntercomState is RideLynkIntercomState.Discovery)
                            vm.rideLynkAction(RideLynkAction.StopDiscovery)
                        else
                            vm.rideLynkAction(RideLynkAction.StartDiscovery)
                    },
                    shape = RoundedCornerShape(50),
                    elevation = ButtonDefaults.elevation(0.dp),
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = Color.White,
                        contentColor = ColorShark
                    ),
                    modifier = Modifier
                        .wrapContentWidth()
                        .wrapContentHeight(),

                    ) {
                    Row(verticalAlignment = Alignment.CenterVertically) {

                        if (rideLynkIntercomState is RideLynkIntercomState.Discovery)
                            CircularProgressIndicator(
                                strokeWidth = 2.dp,
                                modifier = Modifier
                                    .padding(end = 7.dp)
                                    .height(14.dp)
                                    .width(14.dp),
                                color = ColorShark
                            )

                        Text(
                            text =
                            if (rideLynkIntercomState is RideLynkIntercomState.Discovery)
                                "Stop discovery".uppercase()
                            else "Start discovery".uppercase(),
                            style = MaterialTheme.typography.caption,
                            fontWeight = FontWeight.SemiBold,
                        )
                    }
                }

//                when (rideLynkIntercomState) {
//                    RideLynkIntercomState.Discovery -> {
//
//                        CircularProgressIndicator(
//                            strokeWidth = 3.dp,
//                            modifier = Modifier
//                                .padding(horizontal = 16.dp)
//                                .height(20.dp)
//                                .width(20.dp),
//                            color = Color.White
//                        )
//                    }
////                    is RideLynkIntercomState.ActiveCall -> TODO()
////                    RideLynkIntercomState.AutoPairing -> TODO()
////                    is RideLynkIntercomState.Connected -> TODO()
////                    RideLynkIntercomState.Idle -> TODO()
////                    RideLynkIntercomState.Unavailable -> TODO()
//                    else -> Unit
//                }

            }


            LazyColumn(modifier = Modifier.weight(1f)) {
                items(items = devices,
                    itemContent = { device ->
                        Card(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(horizontal = 16.dp)
                                .padding(bottom = 12.dp),
                            backgroundColor = Color(0x2EFFFFFF),
                            elevation = 0.dp,
                            shape = RoundedCornerShape(8.dp)
                        ) {
                            Row(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(6.dp),
                                verticalAlignment = Alignment.CenterVertically
                            ) {

                                val imageRes = loadPicture(R.drawable.ic_helmet).value

                                if (imageRes != null)
                                    Icon(
                                        bitmap = imageRes.asImageBitmap(),
                                        contentDescription = null,
                                        tint = Color.White,
                                        modifier = Modifier
                                            .clip(shape = CircleShape)
                                            .padding(8.dp)
                                            .width(38.dp)
                                            .height(38.dp)
                                            .padding(4.dp),
                                    )

                                Text(
                                    modifier = Modifier
                                        .padding(horizontal = 8.dp)
                                        .weight(1f),
                                    text = device.name,
                                    style = MaterialTheme.typography.subtitle1,
                                    color = Color.White,
                                    fontWeight = FontWeight.SemiBold
                                )

                                val imageRes2 = loadPicture(R.drawable.ic_phone).value

                                if (imageRes2 != null)
                                    Icon(
                                        imageVector = ImageVector.vectorResource(id = R.drawable.ic_phone),
                                        contentDescription = null,
                                        tint = Color.White,
                                        modifier = Modifier
                                            .clip(shape = CircleShape)
                                            .clickable {
                                                vm.rideLynkAction(
                                                    RideLynkAction.Connect(
                                                        device
                                                    )
                                                )
                                            }
                                            .padding(8.dp)
                                            .width(38.dp)
                                            .height(38.dp)
                                            .background(color = ColorPositive, CircleShape)
                                            .padding(6.dp),
                                    )
                            }

                        }
                    }
                )
            }

            if (rideLynkIntercomState is RideLynkIntercomState.Connected || rideLynkIntercomState is RideLynkIntercomState.ActiveCall) {
                Card(
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentHeight()
                        .padding(horizontal = 16.dp)
                        .padding(bottom = 20.dp)
                        .padding(top = 18.dp)
                        .background(
                            color = ColorDarkShadow,
                            shape = RoundedCornerShape(8.dp)
                        ),
                    backgroundColor = Color.Transparent,
                    elevation = 0.dp
                ) {
                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentHeight()
                            .padding(16.dp),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        val imageRes = loadPicture(R.drawable.ic_helmet).value

                        if (imageRes != null)
                            Icon(
                                bitmap = imageRes.asImageBitmap(),
                                contentDescription = null,
                                tint = Color.White,
                                modifier = Modifier
                                    .clip(shape = CircleShape)
                                    .width(38.dp)
                                    .height(38.dp)
                                    .padding(bottom = 8.dp),
                            )

                        Text(
                            modifier = Modifier
                                .alpha(0.7f),
                            text = "Connected".uppercase(),
                            style = MaterialTheme.typography.caption,
                            color = Color.White,
                            fontWeight = FontWeight.SemiBold
                        )

                        Text(
                            modifier = Modifier,
                            text = activeDevice?.name ?: "",
                            style = MaterialTheme.typography.subtitle2,
                            color = Color.White,
                            fontWeight = FontWeight.SemiBold
                        )


                        Row {
                            Button(
                                onClick = { vm.rideLynkAction(RideLynkAction.Disconnect) },
                                shape = RoundedCornerShape(50),
                                elevation = ButtonDefaults.elevation(0.dp),
                                colors = ButtonDefaults.buttonColors(
                                    backgroundColor = ColorNegative,
                                    contentColor = Color.White
                                ),
                                modifier = Modifier
                                    .padding(top = 20.dp)
                                    .wrapContentWidth()
                                    .wrapContentHeight(),

                                ) {
                                Text(
                                    text = "Disconnect".uppercase(),
                                    style = MaterialTheme.typography.subtitle2,
                                    fontWeight = FontWeight.Medium,
                                )
                            }

                            Spacer(modifier = Modifier.width(12.dp))

                            Button(
                                onClick = {

                                    if (rideLynkIntercomState is RideLynkIntercomState.ActiveCall)
                                        vm.rideLynkAction(RideLynkAction.StopCall)
                                    else
                                        vm.rideLynkAction(RideLynkAction.StartCall)

                                },
                                shape = RoundedCornerShape(50),
                                elevation = ButtonDefaults.elevation(0.dp),
                                colors = ButtonDefaults.buttonColors(
                                    backgroundColor = if (rideLynkIntercomState is RideLynkIntercomState.ActiveCall)
                                        ColorNegative
                                    else
                                        ColorPositive,
                                    contentColor = Color.White
                                ),
                                modifier = Modifier
                                    .padding(top = 20.dp)
                                    .wrapContentHeight()
                                    .weight(1f),

                                ) {
                                Text(
                                    text = if (rideLynkIntercomState is RideLynkIntercomState.ActiveCall)
                                        "End call".uppercase()
                                    else
                                        "Start call".uppercase(),
                                    style = MaterialTheme.typography.subtitle2,
                                    fontWeight = FontWeight.Medium,
                                )
                            }
                        }

                    }
                }
            } else {
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp, vertical = 12.dp),
                    text = "Speed Dial".uppercase(),
                    style = MaterialTheme.typography.subtitle2,
                    color = Color.White,
                    fontWeight = FontWeight.SemiBold
                )
                SpeedDialView(
                    modifier = Modifier

                        .wrapContentHeight()
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp),
                    _speedDialUsers = vm.speedDialContacts,
                    dialSpeedDial = vm::dialSpeedDial
                )
            }

        }

    }
}

@Composable
fun DeviceInfoCardCollapsed(
    _deviceInfo: LiveData<DeviceInfo>,
    _batteryLevel: LiveData<Int>,
) {
    val batteryLevel: Int by _batteryLevel.observeAsState(0)
    val deviceInfo: DeviceInfo by _deviceInfo.observeAsState(
        DeviceInfo(
            R.drawable.sx20_small,
            R.string.app_name,
            ""
        )
    )

    BluArmorTheme {
        Box(modifier = Modifier.padding(top = 10.dp)) {
            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp)
                    .padding(top = 18.dp)
                    .background(
                        brush = Brush.linearGradient(
                            colors = listOf(Color(0xFF2E5A80), Color(0xFF254C6D), Color(0xFF0C2941)),
                            start = Offset(0f, 0f),
                            end = Offset(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY)
                        ),
                        shape = RoundedCornerShape(8.dp)
                    ),
                backgroundColor = Color.Transparent,
                elevation = 0.dp
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 10.dp)
                ) {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                    ) {
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(start = 78.dp)
                                .padding(end = 16.dp),
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.SpaceBetween,
                        ) {

                            Column {
                                Text(
                                    text = if (deviceInfo.deviceName.isBlank())
                                        "Disconnected"
                                    else deviceInfo.deviceName.replace("^Sx\\d{2}_".toRegex(), ""),
                                    color = Color.White,
                                    fontWeight = FontWeight.Medium,
                                    style = MaterialTheme.typography.body2
                                )
                                Text(
                                    text = stringResource(id = deviceInfo.modelName),
                                    color = Color.White,
                                    fontWeight = FontWeight.SemiBold,
                                    style = MaterialTheme.typography.caption,
                                    modifier = Modifier.alpha(0.7f)
                                )
                            }

                            if (batteryLevel > 0)
                                Text(
                                    text = "${batteryLevel}%",
                                    color = Color.White,
                                    fontWeight = FontWeight.SemiBold,
                                    style = MaterialTheme.typography.caption
                                )

                        }
                    }
                }
            }
            Image(
                bitmap = ImageBitmap.imageResource(id = deviceInfo.deviceDrawableRes),
                contentDescription = null,
                modifier = Modifier
                    .padding(horizontal = 16.dp)
                    .width(78.dp),
                contentScale = ContentScale.Fit
            )
        }
    }
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun DeviceInfoCardCollapsedWithRename(
    _discoveryState: LiveData<DiscoveryState>,
    _deviceInfo: LiveData<DeviceInfo>,
    _batteryLevel: LiveData<Int>,
    enableRename: Boolean = false,
    deviceName: String,
    onNameFiledChange: (name: String) -> Unit,
    rename: () -> Unit,
    cancelRename: () -> Unit,
    toggleRename: (enable: Boolean) -> Unit,
) {
    val discoveryState: DiscoveryState? by _discoveryState.observeAsState(null)
    val batteryLevel: Int by _batteryLevel.observeAsState(0)
    val deviceInfo: DeviceInfo by _deviceInfo.observeAsState(
        DeviceInfo(
            R.drawable.sx20_small,
            R.string.app_name,
            ""
        )
    )

    BluArmorTheme {
        Box(modifier = Modifier.padding(top = 10.dp)) {
            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp)
                    .padding(top = 18.dp)
                    .background(
                        brush = Brush.linearGradient(
                            colors = listOf(Color(0xFF2E5A80), Color(0xFF254C6D), Color(0xFF0C2941)),
                            start = Offset(0f, 0f),
                            end = Offset(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY)
                        ),
                        shape = RoundedCornerShape(8.dp)
                    ),
                backgroundColor = Color.Transparent,
                elevation = 0.dp
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 10.dp)
                ) {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                    ) {

                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(start = 78.dp)
                                .padding(end = 16.dp),
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.SpaceBetween,
                        ) {

                            Column {
                                Row(verticalAlignment = Alignment.CenterVertically) {
                                    Text(
                                        text = if (discoveryState !is DiscoveryState.Connected)
                                            "Disconnected"
                                        else deviceInfo.deviceName.replace("^Sx\\d{2}_".toRegex(), ""),
                                        color = Color.White,
                                        fontWeight = FontWeight.Medium,
                                        style = MaterialTheme.typography.body2
                                    )
                                    Spacer(modifier = Modifier.width(6.dp))
                                    if (discoveryState is DiscoveryState.Connected)
                                        Image(
                                            imageVector = ImageVector.vectorResource(id = R.drawable.ic_edit),
                                            contentDescription = null,
                                            modifier = Modifier
                                                .clip(shape = CircleShape)
                                                .clickable {
                                                    toggleRename(!enableRename)
                                                }
                                                .padding(2.dp)
                                                .height(20.dp)
                                                .aspectRatio(1f),

                                            )
                                }
                                Text(
                                    text = stringResource(id = deviceInfo.modelName),
                                    color = Color.White,
                                    fontWeight = FontWeight.SemiBold,
                                    style = MaterialTheme.typography.caption,
                                    modifier = Modifier.alpha(0.7f)
                                )
                            }

                            if (discoveryState is DiscoveryState.Connected)
                                Text(
                                    text = "${batteryLevel}%",
                                    color = Color.White,
                                    fontWeight = FontWeight.SemiBold,
                                    style = MaterialTheme.typography.caption
                                )

                        }

                        if (discoveryState is DiscoveryState.Connected) {
                            val animationTime = 200
                            androidx.compose.animation.AnimatedVisibility(
                                visible = enableRename,
                                enter = expandVertically(
                                    expandFrom = Alignment.Top,
                                    animationSpec = tween(durationMillis = animationTime)
                                ) + fadeIn(
                                    initialAlpha = 0.3f,
                                    animationSpec = tween(durationMillis = animationTime + 50)
                                ),

                                exit = shrinkVertically(
                                    animationSpec = tween(durationMillis = animationTime)
                                ) + fadeOut(
                                    animationSpec = tween(durationMillis = animationTime - 100)
                                ),
                                modifier = Modifier,
                                content = {
                                    Column(horizontalAlignment = Alignment.Start) {
                                        Spacer(modifier = Modifier.height(12.dp))
                                        EditText(
                                            modifier = Modifier
                                                .fillMaxWidth()
                                                .padding(horizontal = 24.dp),
                                            label = "New Name", onValueChange = onNameFiledChange, text = deviceName,
                                            textFieldBackGroundColor = Color(0x1FFFFFFF),
                                            maxCharacterCount = 10,
                                            showCharacterCount = true
                                        )

                                        Spacer(modifier = Modifier.height(12.dp))


                                        Text(
                                            modifier = Modifier
                                                .fillMaxWidth()
                                                .padding(horizontal = 24.dp),
                                            text = "Device will restart to reflect the change!",
                                            style = MaterialTheme.typography.caption,
                                            color = ColorTextPrimaryAccent,
                                            fontWeight = FontWeight.Medium,
                                            textAlign = TextAlign.Start
                                        )

                                        Spacer(modifier = Modifier.height(8.dp))
                                        Row(
                                            modifier = Modifier
                                                .fillMaxWidth()
                                                .padding(horizontal = 24.dp)
                                        ) {

                                            ButtonPrimaryHollow(
                                                label = "Cancel".uppercase(),
                                                onClick = cancelRename,
                                                modifier = Modifier
                                            )

                                            Spacer(modifier = Modifier.width(16.dp))

                                            ButtonPrimary(
                                                label = "Rename".uppercase(),
                                                onClick = rename,
                                                modifier = Modifier
                                                    .weight(1f)
                                            )
                                        }
                                        Spacer(modifier = Modifier.height(12.dp))
                                    }
                                },
                            )
                        }
                    }
                }
            }
            Image(
                bitmap = ImageBitmap.imageResource(id = deviceInfo.deviceDrawableRes),
                contentDescription = null,
                modifier = Modifier
                    .padding(horizontal = 16.dp)
                    .width(78.dp),
                contentScale = ContentScale.Fit
            )
        }
    }
}


@Composable
fun SpeedDialView(
    modifier: Modifier = Modifier,
    _speedDialUsers: LiveData<Array<Buddy?>>,
    dialSpeedDial: (speedDialUser: Buddy) -> Unit
) {

    val speedDialRowScrollState = rememberScrollState()
    val speedDialUsers: Array<Buddy?> by _speedDialUsers.observeAsState(arrayOf())

    Row(
        modifier
            .padding(horizontal = 16.dp)
            .horizontalScroll(speedDialRowScrollState)
    ) {
        speedDialUsers.forEachIndexed { index, speedDialUser ->
            if (speedDialUser == null)
                SpeedDialFreeSlot(
                    modifier = Modifier,
                    slotPosition = index + 1
                )
            else
                SpeedDialSlot(
                    modifier = Modifier.clickable {
                        dialSpeedDial.invoke(speedDialUser)
                    },
                    slotPosition = index + 1, speedDialUser = speedDialUser
                )

            Spacer(modifier = Modifier.width(10.dp))
        }
    }

//    LazyRow(
//        modifier = modifier
//    ) {
//        itemsIndexed(
//            items = speedDialUsers,
//            itemContent = { index, speedDialUser ->
//
//                Timber.e("T3: $index ,${speedDialUser?.json()}")
//                if (speedDialUser == null)
//                    SpeedDialFreeSlot(
//                        modifier = Modifier,
//                        slotPosition = index + 1
//                    )
//                else
//                    SpeedDialSlot(
//                        modifier = Modifier,
//                        slotPosition = index + 1, speedDialUser = speedDialUser
//                    )
//
//                Spacer(modifier = Modifier.width(10.dp))
//            }
//        )
//    }
}

@Composable
fun SpeedDialSlot(modifier: Modifier = Modifier, slotPosition: Int, speedDialUser: Buddy) {
    EdgeCard(
        modifier = modifier
            .defaultMinSize(minWidth = 102.dp, minHeight = 94.dp)
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
            modifier = Modifier.padding(top = 0.dp, bottom = 4.dp, end = 8.dp, start = 8.dp)
        ) {
            Text(
                text = slotPosition.toString(),
                style = MaterialTheme.typography.h6,
                fontWeight = FontWeight.Black,
                color = Color.White,
                modifier = Modifier.alpha(0.5f)
            )

            Icon(
                imageVector = ImageVector.vectorResource(id = R.drawable.ic_helmet),
                contentDescription = null,
                modifier = Modifier
                    .padding(bottom = 4.dp)
                    .height(22.dp)
                    .wrapContentWidth(),
                tint = Color.White
            )

            Text(
                text = speedDialUser.name,
                style = MaterialTheme.typography.caption,
                fontWeight = FontWeight.Medium,
                color = Color.White
            )
        }
    }
}

@Composable
fun SpeedDialFreeSlot(modifier: Modifier = Modifier, slotPosition: Int) {
    EdgeCard(
        modifier = modifier
            .defaultMinSize(minWidth = 70.dp, minHeight = 94.dp)
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
            modifier = Modifier.padding(vertical = 4.dp, horizontal = 12.dp)
        ) {


            Text(
                text = "Slot",
                style = MaterialTheme.typography.caption,
                fontWeight = FontWeight.Medium,
                color = Color.White
            )
            Text(
                text = slotPosition.toString(),
                style = MaterialTheme.typography.h6,
                fontWeight = FontWeight.Black,
                color = Color.White,
                modifier = Modifier.alpha(0.5f)
            )

            Text(
                text = "Free",
                style = MaterialTheme.typography.overline,
                fontWeight = FontWeight.Medium,
                color = Color.White,
                modifier = Modifier.alpha(0.6f)
            )
        }
    }
}


@Preview(
    showBackground = true
)
@Composable
fun Preview() {
    BuddyDiscoveryView(rememberNavController(), buddyDiscoveryViewInterface)
}
