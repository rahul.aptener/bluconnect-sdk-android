package com.aptener.bluarmor.ui.app.buddylist

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.aptener.bluarmor.R
import com.aptener.bluarmor.service.bluconnect.model.*
import com.aptener.bluarmor.service.bluconnect.BluConnectManager

import com.aptener.bluarmor.ui.app.dashboard.DashboardViewDestination
import com.aptener.bluarmor.ui.theme.BluArmorTheme
import com.aptener.bluarmor.ui.theme.ColorPositive
import com.aptener.bluarmor.ui.util.loadPicture
import com.aptener.bluarmor.ui.util.navigate
import com.aptener.bluconnect.repo.Buddy
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject


@AssistedFactory
interface BuddyListViewModelAssistedFactory {
    fun create(bluArmorInterface: BluArmorInterface): BuddyListViewViewModel
}


interface BuddyListViewInterface {
    val buddyDevices: LiveData<List<BuddyDevice>>
    val deviceInfo: LiveData<DeviceInfo>
    val batteryLevel: LiveData<Int>
    val speedDialContacts: LiveData<Array<Buddy?>>
    fun dialSpeedDial(speedDialUser: Buddy)
    fun navigateToSpeedDialConfigView(navController: NavController)
    fun navigateToInviteBuddyView(navController: NavController)
}

class BuddyListViewViewModel @AssistedInject constructor(@Assisted private val bluArmorInterface: BluArmorInterface) : ViewModel(),
    BuddyListViewInterface {
    override val buddyDevices: LiveData<List<BuddyDevice>> = bluArmorInterface.bluConnectManager.buddyRidersList
    override val deviceInfo: LiveData<DeviceInfo> = bluArmorInterface.bluConnectManager.deviceInfo
    override val batteryLevel: LiveData<Int> = bluArmorInterface.bluConnectManager.batteryLevel

    override val speedDialContacts: LiveData<Array<Buddy?>> = bluArmorInterface.bluConnectManager.speedDialContacts
    override fun dialSpeedDial(speedDialUser: Buddy) = bluArmorInterface.bluConnectManager.dialSpeedDial(speedDialUser)
    override fun navigateToSpeedDialConfigView(navController: NavController) {
        navController.navigate(DashboardViewDestination.SpeedDialConfigDest)
    }

    override fun navigateToInviteBuddyView(navController: NavController) {
        navController.navigate(DashboardViewDestination.BuddyInviteDest)
    }

    class Factory(
        private val assistedFactory: BuddyListViewModelAssistedFactory,
        private val bluArmorInterface: BluArmorInterface,
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return assistedFactory.create(bluArmorInterface) as T
        }
    }
}


@Composable
fun BuddyListView(
    navController: NavController,
    vm: BuddyListViewInterface,
) {
    val devices: List<BuddyDevice> by vm.buddyDevices.observeAsState(listOf())

    BluArmorTheme {
        Scaffold(
            topBar = {
                TopAppBar(
                    backgroundColor = MaterialTheme.colors.background,
                    elevation = 0.dp,
                ) {
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.SpaceBetween,
                    ) {
                        Image(
                            imageVector = ImageVector.vectorResource(R.drawable.ic_back_nav),
                            contentDescription = null,
                            modifier = Modifier
                                .padding(0.dp)
                                .clickable {
                                    navController.popBackStack()
                                }
                        )
                    }
                }
            }
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
            ) {
                DeviceInfoCardCollapsed(
                    vm.deviceInfo,
                    vm.batteryLevel
                )

                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp)
                        .padding(top = 16.dp)
                        .padding(bottom = 8.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {

                    Text(
                        modifier = Modifier
                            .padding(vertical = 4.dp, horizontal = 16.dp)
                            .weight(1f),
                        text = "Available Riders".uppercase(),
                        style = MaterialTheme.typography.caption,
                        color = Color.White,
                        fontWeight = FontWeight.SemiBold
                    )

                    Text(
                        text = "+ Add Buddies".uppercase(),
                        style = MaterialTheme.typography.caption,
                        fontWeight = FontWeight.SemiBold,
                        color = Color.White,
                        modifier = Modifier
                            .background(
                                color = Color(0x21FFFFFF),
                                shape = RoundedCornerShape(50)
                            )
                            .padding(vertical = 8.dp, horizontal = 14.dp)
                            .clickable {
                                vm.navigateToInviteBuddyView(navController)
                            }
                    )

                }


                LazyColumn(modifier = Modifier.weight(1f)) {
                    items(items = devices,
                        itemContent = { device ->
                            Card(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(horizontal = 16.dp)
                                    .padding(bottom = 12.dp),
                                backgroundColor = Color(0x2EFFFFFF),
                                elevation = 0.dp,
                                shape = RoundedCornerShape(8.dp)
                            ) {
                                Row(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(6.dp),
                                    verticalAlignment = Alignment.CenterVertically
                                ) {

                                    val imageRes = loadPicture(R.drawable.ic_helmet).value

                                    if (imageRes != null)
                                        Icon(
                                            bitmap = imageRes.asImageBitmap(),
                                            contentDescription = null,
                                            tint = Color.White,
                                            modifier = Modifier
                                                .clip(shape = CircleShape)
                                                .padding(8.dp)
                                                .width(38.dp)
                                                .height(38.dp)
                                                .padding(4.dp),
                                        )

                                    Text(
                                        modifier = Modifier
                                            .padding(horizontal = 8.dp)
                                            .weight(1f),
                                        text = device.name,
                                        style = MaterialTheme.typography.subtitle1,
                                        color = Color.White,
                                        fontWeight = FontWeight.SemiBold
                                    )

                                    val imageRes2 = loadPicture(R.drawable.ic_phone).value

                                    if (imageRes2 != null)
                                        Icon(
                                            imageVector = ImageVector.vectorResource(id = R.drawable.ic_phone),
                                            contentDescription = null,
                                            tint = Color.White,
                                            modifier = Modifier
                                                .clip(shape = CircleShape)
                                                .clickable {
                                                    vm.dialSpeedDial(device.buddy ?: return@clickable)
                                                }
                                                .padding(8.dp)
                                                .width(38.dp)
                                                .height(38.dp)
                                                .background(color = ColorPositive, CircleShape)
                                                .padding(6.dp),
                                        )
                                }
                            }
                        }
                    )
                }

                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp)
                        .padding(top = 16.dp)
                        .padding(bottom = 8.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {


                    Text(
                        modifier = Modifier
                            .padding(vertical = 4.dp, horizontal = 16.dp)
                            .weight(1f),
                        text = "Speed Dial".uppercase(),
                        style = MaterialTheme.typography.caption,
                        color = Color.White,
                        fontWeight = FontWeight.SemiBold
                    )

                    Text(
                        text = "Edit Speed Dial".uppercase(),
                        style = MaterialTheme.typography.caption,
                        fontWeight = FontWeight.SemiBold,
                        color = Color.White,
                        modifier = Modifier
                            .clickable { vm.navigateToSpeedDialConfigView(navController) }
                            .background(
                                color = Color(0x21FFFFFF),
                                shape = RoundedCornerShape(50)
                            )
                            .padding(vertical = 8.dp, horizontal = 14.dp)

                    )

                }

                SpeedDialView(
                    modifier = Modifier

                        .wrapContentHeight()
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp),
                    _speedDialUsers = vm.speedDialContacts,
                    dialSpeedDial = vm::dialSpeedDial
                )

            }
        }
    }
}


@Preview(
    showBackground = true
)
@Composable
fun Preview1() {
    BuddyListView(rememberNavController(), object : BuddyListViewInterface {
        override val buddyDevices: LiveData<List<BuddyDevice>>
            get() = MutableLiveData()
        override val deviceInfo: LiveData<DeviceInfo>
            get() = MutableLiveData()
        override val batteryLevel: LiveData<Int>
            get() = MutableLiveData()
        override val speedDialContacts: LiveData<Array<Buddy?>>
            get() = MutableLiveData()

        override fun dialSpeedDial(speedDialUser: Buddy) {

        }

        override fun navigateToSpeedDialConfigView(navController: NavController) {

        }

        override fun navigateToInviteBuddyView(navController: NavController) {

        }

    })
}
