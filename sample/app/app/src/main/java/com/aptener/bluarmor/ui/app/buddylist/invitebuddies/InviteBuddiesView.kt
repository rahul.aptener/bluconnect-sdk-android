package com.aptener.bluarmor.ui.app.buddylist.invitebuddies


import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.aptener.bluarmor.R
import com.aptener.bluarmor.service.bluconnect.model.RideGridUserDevice
import com.aptener.bluarmor.ui.app.ridegrid.AdminClubMembershipScanScreen
import com.aptener.bluarmor.ui.app.ridegrid.EditText
import com.aptener.bluarmor.ui.theme.BluArmorTheme
import com.aptener.bluarmor.ui.theme.ButtonPrimary
import com.aptener.bluarmor.ui.theme.ColorShark
import com.aptener.bluarmor.ui.util.loadPicture
//import com.aptener.bluconnect.common.ext.asciiToHex
//import com.aptener.bluconnect.common.json
import com.aptener.bluconnect.repo.Buddy
import com.aptener.bluconnect.repo.RideGridGroupInvitation
import com.journeyapps.barcodescanner.CaptureManager
import com.journeyapps.barcodescanner.CompoundBarcodeView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import timber.log.Timber


interface InviteBuddiesViewInterface {
    val buddy: LiveData<Buddy>
    fun getQrCode(context: Context): Flow<Bitmap>
    fun newBuddyFound(buddy: Buddy, navController: NavController)
}

private fun migrateQrCodeToOldSchema(string: String): String {
    return string.replace("\"d\":\"SX_20\"","\"d\":\"S20X\"")
}

@Composable
fun AdminClubMembershipScanScreen2(
    navController: NavController,
    newBuddyFound: (buddy: Buddy, navController: NavController) -> Unit,
    lifecycleOwner: LifecycleOwner = LocalLifecycleOwner.current,
) {
    val context = LocalContext.current
    var scanFlag by remember {
        mutableStateOf(false)
    }

    val compoundBarcodeView = remember {
        CompoundBarcodeView(context).apply {
            val capture = CaptureManager(context as Activity, this)
            capture.initializeFromIntent(context.intent, null)
            this.setStatusText("")
            capture.decode()
            this.resume()
            this.decodeContinuous { result ->
                if (scanFlag) {
                    return@decodeContinuous
                }
                scanFlag = true
                result.text?.let { qrCodeResult ->

                    val newBuddy =
                        Buddy.decodeFromQrCodeData(migrateQrCodeToOldSchema(qrCodeResult))
//                    Timber.e("QR CODE RESULT: $qrCodeResult, ${newBuddy?.json()}")
                    if (newBuddy != null) {
                        newBuddyFound(newBuddy, navController)
                        scanFlag = true
                        capture.onPause()
                        return@let
                    }
                    //Do something and when you finish this something
                    //put scanFlag = false to scan another item
                    scanFlag = false

                }
                //If you don't put this scanFlag = false, it will never work again.
                //you can put a delay over 2 seconds and then scanFlag = false to prevent multiple scanning

            }
        }
    }

    AndroidView(
        modifier = Modifier,
        factory = { compoundBarcodeView },
    )


    DisposableEffect(lifecycleOwner) {
        // Create an observer that triggers our remembered callbacks
        // for sending analytics events
        val observer = LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_PAUSE) {
                compoundBarcodeView.pause()
            } else if (event == Lifecycle.Event.ON_STOP) {
                compoundBarcodeView.pause()
            }
        }

        // Add the observer to the lifecycle
        lifecycleOwner.lifecycle.addObserver(observer)

        // When the effect leaves the Composition, remove the observer


        onDispose {
            compoundBarcodeView.pause()
            lifecycleOwner.lifecycle.removeObserver(observer)
        }
    }
}

@Composable
fun InviteBuddiesView(
    navController: NavController = rememberNavController(),
    vm: InviteBuddiesViewInterface
) {


    val buddy by vm.buddy.observeAsState()
    val groupInviteQr: Bitmap? by vm.getQrCode(LocalContext.current)
        .collectAsState(initial = null, Dispatchers.IO)
    val context = LocalContext.current

    BluArmorTheme {
        Scaffold(
            modifier = Modifier.fillMaxSize(),
            topBar = {
                TopAppBar(
                    backgroundColor = MaterialTheme.colors.background,
                    elevation = 0.dp,
                ) {
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.SpaceBetween,
                    ) {
                        Image(
                            imageVector = ImageVector.vectorResource(R.drawable.ic_back_nav),
                            contentDescription = null,
                            modifier = Modifier
                                .padding(0.dp)
                                .clickable {
                                    navController.popBackStack()
                                }
                        )
                    }
                }
            }
        ) {
            Column(
                modifier = Modifier
                    .padding(horizontal = 32.dp)
                    .fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {

                Card(
                    modifier = Modifier
                        .fillMaxWidth(0.8f)
                        .aspectRatio(1f)
                        .padding(horizontal = 16.dp)
                        .padding(top = 18.dp)
                        .background(
                            color = Color.White,
                            shape = RoundedCornerShape(8.dp)
                        ),
                    backgroundColor = Color.White,
                    elevation = 0.dp,
                ) {
                    if (ContextCompat.checkSelfPermission(
                            LocalContext.current,
                            Manifest.permission.CAMERA
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        AdminClubMembershipScanScreen2(navController, vm::newBuddyFound)
                    } else
                        Text(
                            modifier = Modifier
                                .wrapContentWidth()
                                .padding(vertical = 4.dp, horizontal = 16.dp),
                            text = "Camera permission is needed",
                            style = MaterialTheme.typography.subtitle2,
                            color = ColorShark,
                            fontWeight = FontWeight.SemiBold
                        )
                }

                Text(
                    modifier = Modifier
                        .wrapContentWidth()
                        .padding(vertical = 4.dp, horizontal = 16.dp),
                    text = "Scan Buddy’s QR code to join them",
                    style = MaterialTheme.typography.caption,
                    color = Color.White,
                    fontWeight = FontWeight.SemiBold
                )
                Spacer(modifier = Modifier.weight(1f))

                EditText(
                    label = "Buddy Name",
                    text = buddy?.name ?: "",
                    onValueChange = {},
                    enabled = false
                )

                Card(
                    modifier = Modifier
                        .fillMaxWidth(0.8f)
                        .aspectRatio(1f)
                        .padding(horizontal = 16.dp)
                        .padding(top = 18.dp)
                        .background(
                            color = Color.White,
                            shape = RoundedCornerShape(8.dp)
                        ),
                    backgroundColor = Color.White,
                    elevation = 0.dp,
                ) {

                    if (groupInviteQr != null)
                        Image(
                            bitmap = groupInviteQr!!.asImageBitmap(),
                            contentDescription = null
                        )


//                    QrCodeView(
//                        content = vm.groupName.value ?: " ",
//                    )
                }

                ButtonPrimary(
                    label = "Share".uppercase(),
                    onClick = {
                        /* val sendIntent: Intent = Intent().apply {
                             action = Intent.ACTION_SEND
                             putExtra(Intent.EXTRA_TEXT, "https://ridegrid.page.link/${(buddy?.id ?: "").asciiToHex()}")
                             type = "text/plain"
                         }
                         val shareIntent = Intent.createChooser(sendIntent, null)
                         context.startActivity(shareIntent)*/
                    },
                    modifier = Modifier
                        .padding(vertical = 16.dp)
                        .fillMaxWidth(0.8f)
                )

                Text(
                    modifier = Modifier
                        .wrapContentWidth()
                        .padding(vertical = 4.dp, horizontal = 16.dp),
                    text = "Invite other riders via link",
                    style = MaterialTheme.typography.caption,
                    color = Color.White,
                    fontWeight = FontWeight.SemiBold
                )

                Spacer(modifier = Modifier.weight(0.1f))
            }
        }
    }
}

@Composable
fun BuddyRiderView(
    riderName: String,
    onClick: () -> Unit,
    position: Int? = null,
    positiveIcon: Boolean = true
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp)
            .padding(bottom = 12.dp),
        backgroundColor = Color(0x1AFFFFFF),
        elevation = 0.dp,
        shape = RoundedCornerShape(10.dp)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(6.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {

            val imageRes = loadPicture(R.drawable.ic_helmet).value

            if (imageRes != null)
                Icon(
                    bitmap = imageRes.asImageBitmap(),
                    contentDescription = null,
                    tint = Color.White,
                    modifier = Modifier
                        .clip(shape = CircleShape)
                        .padding(6.dp)
                        .width(24.dp)
                        .height(24.dp)
                        .padding(0.dp),
                )

            Text(
                modifier = Modifier
                    .padding(horizontal = 8.dp)
                    .weight(1f),
                text = riderName,
                style = MaterialTheme.typography.subtitle1,
                color = Color.White,
                fontWeight = FontWeight.SemiBold
            )



            if (position != null)
                Text(
                    modifier = Modifier
                        .padding(horizontal = 8.dp),
                    text = position.toString(),
                    style = MaterialTheme.typography.subtitle1,
                    color = Color.White,
                    fontWeight = FontWeight.SemiBold
                )

            val imageRes2 = loadPicture(
                if (positiveIcon)
                    R.drawable.ic_add
                else R.drawable.ic_remove
            ).value

            if (imageRes2 != null)
                Image(
                    bitmap = imageRes2.asImageBitmap(),
                    contentDescription = null,
                    modifier = Modifier
                        .clip(shape = CircleShape)
                        .clickable(onClick = onClick)
                        .padding(2.dp)
                        .width(40.dp)
                        .height(40.dp),
                )
        }

    }
}

@Preview
@Composable
fun Preview() {
    InviteBuddiesView(rememberNavController(), object : InviteBuddiesViewInterface {
        override val buddy: LiveData<Buddy>
            get() = MutableLiveData()


        override fun getQrCode(context: Context): Flow<Bitmap> {
            TODO("Not yet implemented")
        }

        override fun newBuddyFound(buddy: Buddy, navController: NavController) {
            TODO("Not yet implemented")
        }


    })
}