package com.aptener.bluarmor.ui.app.buddylist.invitebuddies

import android.content.Context
import android.graphics.Bitmap
import androidx.core.content.ContextCompat
import androidx.lifecycle.*
import androidx.navigation.NavController
import com.aptener.bluarmor.R
import com.aptener.bluarmor.baseutils.asMutableLiveData
import com.aptener.bluarmor.service.bluconnect.model.BluArmorInterface
import com.aptener.bluarmor.ui.util.getQrCodeBitmap
//import com.aptener.bluconnect.common.json
import com.aptener.bluconnect.repo.Buddy
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.launch

@AssistedFactory
interface InviteBuddiesViewModelAssistedFactory {
    fun create(bluArmorInterface: BluArmorInterface): InviteBuddiesViewModel
}

class InviteBuddiesViewModel @AssistedInject constructor(
    @Assisted private val bluArmorInterface: BluArmorInterface
) : ViewModel(),
    InviteBuddiesViewInterface {

    override val buddy: LiveData<Buddy> = MutableLiveData()

    init {
        viewModelScope.launch {
            buddy.asMutableLiveData()?.value = bluArmorInterface.bluConnectManager.getActiveBuddy()
        }
    }

    override fun newBuddyFound(buddy: Buddy, navController: NavController) {
        bluArmorInterface.bluConnectManager.device?.buddiesDataSource?.add(buddy)
        navController.popBackStack()
    }

    override fun getQrCode(context: Context): Flow<Bitmap> {
        return callbackFlow {

            val buddy: Buddy = bluArmorInterface.bluConnectManager.getActiveBuddy() ?: run {
                close()
                return@callbackFlow
            }

            val qrData = bluArmorInterface.bluConnectManager.encodeBuddyData(buddy) ?: run {
                close()
                return@callbackFlow
            }

            val backgroundTint = ContextCompat.getColor(context, R.color.white)
            val mainTint = ContextCompat.getColor(context, R.color.black)

            send(
                getQrCodeBitmap(
                    qrTint = mainTint,
                    backgroundTint = backgroundTint,
                    content = qrData
                )
            )

            awaitClose { }
        }
    }

    class Factory(
        private val assistedFactory: InviteBuddiesViewModelAssistedFactory,
        private val bluArmorInterface: BluArmorInterface,
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return assistedFactory.create(bluArmorInterface) as T
        }
    }

}
