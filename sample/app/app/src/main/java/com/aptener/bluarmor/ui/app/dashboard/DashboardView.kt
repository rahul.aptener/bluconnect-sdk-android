package com.aptener.bluarmor.ui.app.dashboard

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.imageResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.aptener.bluarmor.R
import com.aptener.bluarmor.service.bluconnect.model.*
import com.aptener.bluarmor.ui.app.buddylist.SpeedDialView
import com.aptener.bluarmor.ui.theme.*
import com.aptener.bluarmor.ui.util.EdgeCard
import com.aptener.bluarmor.ui.util.NavControllerDestination
import com.aptener.bluarmor.ui.util.SwipeActionView
import com.aptener.bluconnect.device.sxcontrol.VoiceNoteRecorder.VoiceNoteRecorderActions
import com.aptener.bluconnect.BluArmorModel
import com.aptener.bluconnect.device.sxcontrol.Battery
import com.aptener.bluconnect.device.sxcontrol.OtaSwitch
import com.aptener.bluconnect.device.sxcontrol.SpeedDial
import com.aptener.bluconnect.device.sxcontrol.VoiceNoteRecorder.VoiceNoteRecorderState
import com.aptener.bluconnect.device.sxcontrol.Volume
import com.aptener.bluconnect.repo.Buddy

sealed class DashboardViewDestination(private val _route: String) : NavControllerDestination {
    object DashboardDest : DashboardViewDestination("dashboard")
    object BuddyDiscoveryDest : DashboardViewDestination("buddyDiscovery")
    object BuddyListDest : DashboardViewDestination("buddyList")
    object BuddyInviteDest : DashboardViewDestination("buddyInvite")
    object SpeedDialConfigDest : DashboardViewDestination("speedDial")
    object RideGridDashboardDest : DashboardViewDestination("rideGridDashboard")
    object RideGridGroupDest : DashboardViewDestination("rideGridGroup")
    object RideGridCreateGroupDest : DashboardViewDestination("rideGridCreateGroup")
    object RideGridInviteRidersDest : DashboardViewDestination("rideGridInviteRiders")

    override val route: String get() = "dvd_$_route"
}

@Composable
fun DashboardView(
    navController: NavController,
    vm: DashboardViewModelInterface
) {

    val rideLynkIntercomState: RideLynkIntercomState by vm.rideLynkIntercomState.observeAsState(RideLynkIntercomState.Idle)
    val voiceNoteRecorderState: VoiceNoteRecorderState by vm.voiceNoteRecorderState.observeAsState(VoiceNoteRecorderState.Idle)
    val deviceDetails: com.aptener.bluconnect.device.sxcontrol.DeviceInfo.DeviceDetails? by vm.deviceDetails.observeAsState()
    val otaSwitch: OtaSwitch.OtaSwitchState by vm.otaSwitchState.observeAsState(OtaSwitch.OtaSwitchState.NoUpdateAvailable)
    val bluArmorModel: BluArmorModel by vm.bluArmorModel.observeAsState(BluArmorModel.NO_DEVICE)
    val dashboardScrollState = rememberScrollState()

    fun navigate(destination: DashboardViewDestination) = navController.navigate(destination.route)

    BluArmorTheme {
        Scaffold(
            topBar = {
                TopAppBar(
                    backgroundColor = MaterialTheme.colors.background,
                    elevation = 0.dp,
                ) {
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.SpaceBetween,
                    ) {

                        Image(
                            imageVector = ImageVector.vectorResource(R.drawable.ic_logo_bluarmor),
                            contentDescription = null,
                            modifier = Modifier.padding(11.dp)
                        )

                        Image(
                            imageVector = ImageVector.vectorResource(R.drawable.ic_bluarmor_text),
                            contentDescription = null,
                            modifier = Modifier.padding(11.dp)
                        )

                        Image(
                            imageVector = ImageVector.vectorResource(R.drawable.ic_options),
                            contentDescription = null,
                            modifier = Modifier
                                .padding(0.dp)
                                .clickable {
                                    vm.navigateToPreferences(navController)
                                }
                        )
                    }
                }
            },
            floatingActionButton = {
                FloatingActionButton(
                    modifier = Modifier.height(46.dp),
                    onClick = { vm.invokeVoiceAssistant() },
                    backgroundColor = Color.White
                ) {
                    Row(
                        modifier = Modifier.padding(vertical = 10.dp, horizontal = 17.dp),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Image(
                            imageVector = ImageVector
                                .vectorResource(id = R.drawable.ic_google_assistant_logo),
                            contentDescription = "Google Assistant Icon",
                            contentScale = ContentScale.Fit,
                            modifier = Modifier
                                .fillMaxHeight(1f)
                                .aspectRatio(1f, false),
                        )

                        Spacer(modifier = Modifier.width(6.dp))

                        Text(
                            text = "Assistant",
                            style = MaterialTheme.typography.caption,
                            fontWeight = FontWeight.Medium,
                            color = ColorShark
                        )
                    }
                }
            },
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .verticalScroll(dashboardScrollState)
            ) {

                DeviceInfoCard(
                    vm.deviceInfo,
                    vm.batteryLevel,
                    vm.chargingState,
                    vm.musicState,
                    vm.phoneCallState,
                    vm::musicAction,
                    vm::phoneCallAction
                )

                Spacer(modifier = Modifier.height(12.dp))

                if (otaSwitch is OtaSwitch.OtaSwitchState.UpdateAvailable )
                    EdgeCard(
                        modifier = Modifier
                            .fillMaxWidth()
                            .defaultMinSize(minHeight = 68.dp)
                            .padding(horizontal = 16.dp),
                        backgroundColor = ColorNegative,
                        bottomStrokeColor = ColorNegativeLight
                    ) {

                        SwipeActionView(
                            modifier = Modifier
                                .fillMaxWidth()
                                .fillMaxHeight(0.2f),
                            onDragCompleted = {
                                vm.switchToOtaMode()
                            },
                            foregroundContent = {
                                Row(
                                    verticalAlignment = Alignment.CenterVertically,
                                    modifier = Modifier
                                        .background(ColorNegative)
                                        .fillMaxWidth()
                                        .wrapContentHeight()
                                ) {
                                    Image(
                                        imageVector = ImageVector.vectorResource(id = R.drawable.ic_update),
                                        contentDescription = null,
                                        modifier = Modifier
                                            .height(36.dp)
                                            .wrapContentWidth()
                                            .padding(start = 16.dp)
                                            .rotate(90f),
                                        contentScale = ContentScale.Fit
                                    )

                                    Column(
                                        horizontalAlignment = Alignment.Start,
                                        verticalArrangement = Arrangement.Center,
                                        modifier = Modifier
                                            .padding(16.dp)
                                            .weight(1f)
                                    ) {
                                        Text(
                                            text = "Firmware Update Available",
                                            style = MaterialTheme.typography.subtitle2,
                                            fontWeight = FontWeight.Normal,
                                            color = Color.White,
                                        )

                                        Text(
                                            text = "Swipe right to update",
                                            style = MaterialTheme.typography.overline,
                                            fontWeight = FontWeight.SemiBold,
                                            color = Color.White,
                                        )
                                    }
                                }
                            },
                            backgroundContent = {
                                Row(
                                    verticalAlignment = Alignment.CenterVertically,
                                    modifier = Modifier
                                        .background(ColorSharkDark)
                                        .fillMaxWidth()
                                        .wrapContentHeight()
                                ) {

                                    Spacer(modifier = Modifier.width(16.dp))
                                    CircularProgressIndicator(
                                        strokeWidth = 2.dp,
                                        modifier = Modifier
                                            .height(24.dp)
                                            .width(24.dp),
                                        color = Color.White
                                    )

                                    Column(
                                        horizontalAlignment = Alignment.Start,
                                        verticalArrangement = Arrangement.Center,
                                        modifier = Modifier.padding(16.dp)
                                    ) {
                                        Text(
                                            text = "Update Firmware",
                                            style = MaterialTheme.typography.subtitle2,
                                            fontWeight = FontWeight.Normal,
                                            color = Color.White,
                                        )

                                        Text(
                                            text = "Swipe right to update",
                                            style = MaterialTheme.typography.overline,
                                            fontWeight = FontWeight.SemiBold,
                                            color = Color.White,
                                        )
                                    }
                                }
                            }
                        )
                    }

                Spacer(modifier = Modifier.height(12.dp))
                Row(modifier = Modifier.padding(horizontal = 16.dp)) {

                    if (bluArmorModel.supportRideGrid) {
                        EdgeCard(
                            modifier = Modifier
                                .height(74.dp)
                                .weight(1f)
                                .clickable {
                                    navigate(DashboardViewDestination.RideGridGroupDest)
                                }
                        ) {
                            Column(
                                horizontalAlignment = Alignment.CenterHorizontally,
                                verticalArrangement = Arrangement.Center
                            ) {
                                Image(
                                    imageVector = ImageVector.vectorResource(id = R.drawable.ic_ridegrid),
                                    contentDescription = null,
                                    modifier = Modifier
                                        .padding(bottom = 4.dp)
                                        .height(22.dp)
                                        .wrapContentWidth(),
                                    contentScale = ContentScale.Fit
                                )

                                Text(
                                    text = "RIDEGRID".uppercase(),
                                    style = MaterialTheme.typography.subtitle2,
                                    fontWeight = FontWeight.Normal,
                                    color = Color.White,
                                    letterSpacing = 2.sp
                                )

                                Text(
                                    text = "Intercom".uppercase(),
                                    style = MaterialTheme.typography.overline,
                                    fontWeight = FontWeight.SemiBold,
                                    color = Color.White,
                                    letterSpacing = 2.sp
                                )
                            }
                        }
                        Spacer(modifier = Modifier.width(12.dp))
                    }

                    EdgeCard(
                        modifier = Modifier
                            .height(74.dp)
                            .weight(1f)
                            .clickable {
                                when (vm.rideLynkIntercomState.value) {
                                    is RideLynkIntercomState.ActiveCall,
                                    RideLynkIntercomState.AutoPairing,
                                    is RideLynkIntercomState.Connected,
                                    RideLynkIntercomState.Unavailable -> Unit
                                    else -> vm.rideLynkAction(RideLynkAction.StartDiscovery)
                                }
                                navigate(DashboardViewDestination.BuddyDiscoveryDest)
                            },
                        bottomStrokeColor = when (rideLynkIntercomState) {
                            is RideLynkIntercomState.ActiveCall,
                            is RideLynkIntercomState.Connected -> ColorPositive
                            RideLynkIntercomState.AutoPairing,
                            RideLynkIntercomState.Discovery,
                            RideLynkIntercomState.Idle,
                            RideLynkIntercomState.Unavailable -> ColorDarkShadow
                        }
                    ) {
                        Column(
                            horizontalAlignment = Alignment.CenterHorizontally,
                            verticalArrangement = Arrangement.Center
                        ) {
                            Image(
                                imageVector = ImageVector.vectorResource(id = R.drawable.ic_ridelynk),
                                contentDescription = null,
                                modifier = Modifier
                                    .padding(bottom = 4.dp)
                                    .height(22.dp)
                                    .wrapContentWidth(),
                                contentScale = ContentScale.Fit
                            )

                            Text(
                                text = "RIDELYNK".uppercase(),
                                style = MaterialTheme.typography.subtitle2,
                                fontWeight = FontWeight.Normal,
                                color = Color.White,
                                letterSpacing = 2.sp
                            )

                            Text(
                                text = "Intercom".uppercase(),
                                style = MaterialTheme.typography.overline,
                                fontWeight = FontWeight.SemiBold,
                                color = Color.White,
                                letterSpacing = 2.sp
                            )
                        }
                    }

                    Spacer(modifier = Modifier.width(12.dp))

                    EdgeCard(
                        modifier = Modifier
                            .height(74.dp)
                            .weight(1f)
                            .clickable {
                                navigate(DashboardViewDestination.BuddyListDest)
                            }
                    ) {
                        Column(
                            horizontalAlignment = Alignment.CenterHorizontally,
                            verticalArrangement = Arrangement.Center
                        ) {
                            Image(
                                imageVector = ImageVector.vectorResource(id = R.drawable.ic_ride_group),
                                contentDescription = null,
                                modifier = Modifier
                                    .padding(bottom = 4.dp)
                                    .height(22.dp)
                                    .wrapContentWidth(),
                                contentScale = ContentScale.Fit
                            )

                            Text(
                                text = "Buddy".uppercase(),
                                style = MaterialTheme.typography.subtitle2,
                                fontWeight = FontWeight.Normal,
                                color = Color.White,
                                letterSpacing = 2.sp
                            )

                            Text(
                                text = "List".uppercase(),
                                style = MaterialTheme.typography.overline,
                                fontWeight = FontWeight.SemiBold,
                                color = Color.White,
                                letterSpacing = 2.sp
                            )
                        }
                    }
                }
                Spacer(modifier = Modifier.height(12.dp))

                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 4.dp, horizontal = 32.dp),
                    text = "Volume".uppercase(),
                    style = MaterialTheme.typography.caption,
                    color = Color.White,
                    fontWeight = FontWeight.SemiBold
                )

                VolumeControlCard(
                    modifier = Modifier.padding(horizontal = 16.dp),
                    vm.volumeLevel,
                    vm::volumeControlAction
                )
                Spacer(modifier = Modifier.height(12.dp))

                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 4.dp, horizontal = 32.dp),
                    text = "Speed Dial".uppercase(),
                    style = MaterialTheme.typography.caption,
                    color = Color.White,
                    fontWeight = FontWeight.SemiBold
                )
                SpeedDialView(
                    modifier = Modifier
                        .wrapContentHeight()
                        .fillMaxWidth(),
                    _speedDialUsers = vm.speedDialContacts,
                    dialSpeedDial = vm::dialSpeedDial
                )
                Spacer(modifier = Modifier.height(12.dp))
                /*Row(modifier = Modifier.padding(horizontal = 16.dp)) {

                    EdgeCard(
                        modifier = Modifier
                            .height(74.dp)
                            .weight(1f)
                            .clickable {
                                if (voiceNoteRecorderState == VoiceNoteRecorderState.Idle)
                                    vm.voiceNoteRecordAction(VoiceNoteRecorderActions.START)
                                else {
                                    vm.voiceNoteRecordAction(VoiceNoteRecorderActions.STOP)
                                }
                            },
                        bottomStrokeColor = if (voiceNoteRecorderState == VoiceNoteRecorderState.Idle) ColorDarkShadow else ColorPositive
                    ) {
                        Column(
                            horizontalAlignment = Alignment.CenterHorizontally,
                            verticalArrangement = Arrangement.Center
                        ) {
                            Icon(
                                imageVector = ImageVector.vectorResource(id = R.drawable.ic_baseline_mic),
                                contentDescription = null,
                                modifier = Modifier
                                    .padding(bottom = 4.dp)
                                    .height(22.dp)
                                    .wrapContentWidth(),
                                tint = Color.White
                            )

                            Text(
                                text = if (voiceNoteRecorderState == VoiceNoteRecorderState.Idle) "Record".uppercase()
                                else "Stop".uppercase(),
                                style = MaterialTheme.typography.subtitle2,
                                fontWeight = FontWeight.Normal,
                                color = Color.White,
                                letterSpacing = 2.sp
                            )

                            Text(
                                text = if (voiceNoteRecorderState == VoiceNoteRecorderState.Idle) "Voice Note".uppercase()
                                else "Voice Note Recording".uppercase(),
                                style = MaterialTheme.typography.overline,
                                fontWeight = FontWeight.SemiBold,
                                color = Color.White,
                                letterSpacing = 2.sp
                            )
                        }
                    }
                }*/

                Spacer(modifier = Modifier.height(68.dp))
            }
        }
    }

}

@Composable
fun VolumeControlCard(
    modifier: Modifier = Modifier,
    _volumeState: LiveData<Volume.VolumeLevel>,
    volumeControlAction: (volumeControlAction: VolumeControlAction) -> Unit
) {

    val volumeState: Volume.VolumeLevel? by _volumeState.observeAsState(null)

//    var sliderPosition by remember { mutableStateOf(0f) }

    EdgeCard(modifier = modifier) {
        Column {
            Row(
                modifier = Modifier
                    .padding(horizontal = 16.dp, vertical = 16.dp)
                    .fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Slider(
                    value = volumeState?.volumeLevel?.toFloat() ?: 8f,
                    onValueChange = {
                        volumeControlAction.invoke(VolumeControlAction.ChangeVolume(it.toInt()))
//                        sliderPosition = it
                    },
                    modifier = Modifier.weight(1f),
                    valueRange = 0f..15f
                )
                Spacer(modifier = Modifier.width(4.dp))
                Column(
                    modifier = Modifier
                        .clip(RoundedCornerShape(8.dp))
                        .height(48.dp)
                        .clickable {
                            volumeControlAction(VolumeControlAction.ToggleDynamicVolume(!(volumeState?.dynamicMode ?: false)))
                        }
                        .background(
                            if (volumeState?.dynamicMode == true)
                                Color(0x1AFFFFFF)
                            else
                                Color(0x33FFFFFF)
                        )
                        .padding(8.dp),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {

                    Text(
                        text = stringResource(id = R.string.dynamic).uppercase(),
                        textAlign = TextAlign.Center,
                        style = MaterialTheme.typography.caption,
                        fontWeight = FontWeight.SemiBold,
                    )

                    Spacer(
                        modifier = Modifier
                            .padding(top = 2.dp)
                            .height(3.dp)
                            .width(38.dp)
                            .background(
                                color = if (volumeState?.dynamicMode == true) ColorPositive else ColorNegative,
                                shape = RoundedCornerShape(50)
                            )
                    )
                }
            }

/*            Row(
                modifier = Modifier
                    .padding(horizontal = 16.dp, vertical = 16.dp)
                    .fillMaxWidth(),
                verticalAlignment = Alignment.Top
            ) {
                Column {

                    Text(
                        text = "Volume Boost",
                        style = MaterialTheme.typography.subtitle2,
                        fontWeight = FontWeight.Medium,
                        modifier = Modifier
                            .padding(bottom = 12.dp)
                            .padding(horizontal = 16.dp),
                        color = Color(0x88FFFFFF)
                    )

                    Text(
                        text = "Easy On The Ears",
                        style = MaterialTheme.typography.subtitle2,
                        fontWeight = FontWeight.Medium,
                        modifier = Modifier
                            .background(
                                color = if (volumeState.volumeBoost == Volume.VolumeBoost.EASY_ON_EARS)
                                    Color(0x33FFFFFF)
                                else Color.Transparent,
                                shape = RoundedCornerShape(50)
                            )
                            .clickable { volumeControlAction(VolumeControlAction.ChangeVolumeBoost(Volume.VolumeBoost.EASY_ON_EARS)) }
                            .padding(vertical = 4.dp, horizontal = 16.dp)

                    )

                    Spacer(modifier = Modifier.height(8.dp))

                    Text(
                        text = "Balanced Sound",
                        style = MaterialTheme.typography.subtitle2,
                        fontWeight = FontWeight.Medium,
                        modifier = Modifier
                            .background(
                                color = if (volumeState.volumeBoost == Volume.VolumeBoost.BALANCED)
                                    Color(0x33FFFFFF)
                                else Color.Transparent,
                                shape = RoundedCornerShape(50)
                            )
                            .clickable { volumeControlAction.invoke(VolumeControlAction.ChangeVolumeBoost(Volume.VolumeBoost.BALANCED)) }
                            .padding(vertical = 4.dp, horizontal = 16.dp)

                    )

                    Spacer(modifier = Modifier.height(8.dp))

                    Text(
                        text = "High Output",
                        style = MaterialTheme.typography.subtitle2,
                        fontWeight = FontWeight.Medium,
                        modifier = Modifier
                            .background(
                                color = if (volumeState.volumeBoost == Volume.VolumeBoost.HIGH_OUTPUT)
                                    Color(0x33FFFFFF)
                                else Color.Transparent,
                                shape = RoundedCornerShape(50)
                            )
                            .clickable { volumeControlAction.invoke(VolumeControlAction.ChangeVolumeBoost(Volume.VolumeBoost.HIGH_OUTPUT)) }
                            .padding(vertical = 4.dp, horizontal = 16.dp)

                    )

                }

                Spacer(modifier = Modifier.width(26.dp))
                Column {

                    Text(
                        text = "Bass Boost",
                        style = MaterialTheme.typography.subtitle2,
                        fontWeight = FontWeight.Medium,
                        modifier = Modifier
                            .padding(bottom = 12.dp)
                            .padding(horizontal = 16.dp),
                        color = Color(0x88FFFFFF)
                    )

                    Text(
                        text = "Regular",
                        style = MaterialTheme.typography.subtitle2,
                        fontWeight = FontWeight.Medium,
                        modifier = Modifier
                            .background(
                                color = if (volumeState.bassBoost == Volume.EqualizerBassBoost.REGULAR)
                                    Color(0x33FFFFFF)
                                else Color.Transparent,
                                shape = RoundedCornerShape(50)
                            )
                            .clickable { volumeControlAction.invoke(VolumeControlAction.ChangeEqualizerBoost(Volume.EqualizerBassBoost.REGULAR)) }
                            .padding(vertical = 4.dp, horizontal = 16.dp)

                    )

                    Spacer(modifier = Modifier.height(8.dp))

                    Text(
                        text = "Bass Boost",
                        style = MaterialTheme.typography.subtitle2,
                        fontWeight = FontWeight.Medium,
                        modifier = Modifier
                            .background(
                                color = if (volumeState.bassBoost == Volume.EqualizerBassBoost.BASS_BOOST)
                                    Color(0x33FFFFFF)
                                else Color.Transparent,
                                shape = RoundedCornerShape(50)
                            )
                            .clickable { volumeControlAction.invoke(VolumeControlAction.ChangeEqualizerBoost(Volume.EqualizerBassBoost.BASS_BOOST)) }
                            .padding(vertical = 4.dp, horizontal = 16.dp)
                    )
                }
            }*/
        }
    }

}

/* TODO, separate info Bar from the card */
@Composable
fun DeviceInfoCard(
    _deviceInfo: LiveData<DeviceInfo>,
    _batteryLevel: LiveData<Int>,
    _batteryState: LiveData<Battery.ChargingState>,
    _musicState: LiveData<MusicState>,
    _phoneCallState: LiveData<PhoneCallState>,
    musicAction: (musicAction: MusicAction) -> Unit,
    phoneCallAction: (phoneCallAction: PhoneCallAction) -> Unit
) {
    val batteryLevel: Int by _batteryLevel.observeAsState(0)
    val batteryState: Battery.ChargingState by _batteryState.observeAsState(initial = Battery.ChargingState.CHARGING)
    val deviceInfo: DeviceInfo by _deviceInfo.observeAsState(
        DeviceInfo(
            R.drawable.sx20_small,
            R.string.app_name,
            ""
        )
    )

    val musicState: MusicState by _musicState.observeAsState(MusicState.Idle)
    val phoneCallState: PhoneCallState by _phoneCallState.observeAsState(PhoneCallState.Idle)


    BluArmorTheme {
        Box(modifier = Modifier.padding(top = 10.dp)) {
            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp)
                    .padding(top = 18.dp)
                    .background(
                        brush = Brush.linearGradient(
                            colors = listOf(Color(0xFF2E5A80), Color(0xFF254C6D), Color(0xFF0C2941)),
                            start = Offset(0f, 0f),
                            end = Offset(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY)
                        ),
                        shape = RoundedCornerShape(8.dp)
                    ),
                backgroundColor = Color.Transparent,
                elevation = 0.dp
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 10.dp)
                ) {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                    ) {
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(start = 78.dp)
                                .padding(end = 16.dp),
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.SpaceBetween,
                        ) {

                            Column(modifier = Modifier.weight(1f)) {
                                Text(
                                    text = if (deviceInfo.deviceName.isBlank())
                                        "Disconnected"
                                    else deviceInfo.deviceName.replace("^Sx\\d{2}_".toRegex(), ""),
                                    color = Color.White,
                                    fontWeight = FontWeight.Medium,
                                    style = MaterialTheme.typography.body2
                                )
                                Text(
                                    text = stringResource(id = deviceInfo.modelName),
                                    color = Color.White,
                                    fontWeight = FontWeight.SemiBold,
                                    style = MaterialTheme.typography.caption,
                                    modifier = Modifier.alpha(0.7f)
                                )
                            }

                            if (batteryLevel > 0)
                                Text(
                                    text = "${batteryLevel}%",
                                    color = Color.White,
                                    fontWeight = FontWeight.SemiBold,
                                    style = MaterialTheme.typography.caption
                                )

                            if (batteryState == Battery.ChargingState.CHARGING)
                                Icon(
                                    imageVector = ImageVector.vectorResource(R.drawable.ic_baseline_offline_bolt_24),
                                    contentDescription = null,
                                    modifier = Modifier
                                        .padding(0.dp)
                                        .height(18.dp)
                                        .width(18.dp),
                                    tint = ColorPositive
                                )

                        }
                    }
                    Spacer(modifier = Modifier.height(12.dp))

                    if (phoneCallState is PhoneCallState.Idle || phoneCallState is PhoneCallState.MissedCall)
                        MusicControls(modifier = Modifier.padding(start = 16.dp), musicState, musicAction)
                    else
                        DialerControls(modifier = Modifier.padding(start = 16.dp), phoneCallState, phoneCallAction)
                }
            }

            Image(
                bitmap = ImageBitmap.imageResource(id = deviceInfo.deviceDrawableRes),
                contentDescription = null,
                modifier = Modifier
                    .padding(horizontal = 16.dp)
                    .width(78.dp),
                contentScale = ContentScale.Fit
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun Preview() {
    DashboardView(rememberNavController(), object : DashboardViewModelInterface {
        override val bluArmorModel: LiveData<BluArmorModel>
            get() = TODO("Not yet implemented")
        override val otaSwitchState: LiveData<OtaSwitch.OtaSwitchState>
            get() = TODO("Not yet implemented")
        override val deviceInfo: LiveData<DeviceInfo>
            get() = MutableLiveData()
        override val deviceDetails: LiveData<com.aptener.bluconnect.device.sxcontrol.DeviceInfo.DeviceDetails>
            get() = TODO("Not yet implemented")
        override val batteryLevel: LiveData<Int>
            get() = MutableLiveData()
        override val chargingState: LiveData<Battery.ChargingState>
            get() = MutableLiveData()

        override val volumeLevel: LiveData<Volume.VolumeLevel>
            get() = TODO("Not yet implemented")
        override val musicState: LiveData<MusicState> = MutableLiveData(MusicState.Playing("rer", "23"))
        override val voiceNoteRecorderState: LiveData<VoiceNoteRecorderState>
            get() = MutableLiveData()

        override val phoneCallState: LiveData<PhoneCallState>
            get() = MutableLiveData()
        override val rideLynkIntercomState: LiveData<RideLynkIntercomState>
            get() = MutableLiveData()
        override val speedDialState: LiveData<SpeedDial.SpeedDialState>
            get() = MutableLiveData()
        override val speedDialContacts: LiveData<Array<Buddy?>>
            get() = MutableLiveData()

        override fun volumeControlAction(volumeControlAction: VolumeControlAction) {

        }

        override fun musicAction(musicAction: MusicAction) {
            Unit
        }

        override fun voiceNoteRecordAction(action: VoiceNoteRecorderActions) {

        }

        override fun phoneCallAction(phoneCallAction: PhoneCallAction) {
            Unit
        }

        override fun rideLynkAction(rideLynkAction: RideLynkAction) {
            Unit
        }

        override fun invokeVoiceAssistant() {
        }

        override fun switchToOtaMode() {

        }

        override fun dialSpeedDial(speedDialUser: Buddy) {

        }

        override fun navigateToPreferences(navController: NavController) {

        }

    })
}