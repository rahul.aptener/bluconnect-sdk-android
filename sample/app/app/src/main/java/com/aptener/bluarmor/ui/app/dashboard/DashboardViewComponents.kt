package com.aptener.bluarmor.ui.app.dashboard

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.lifecycle.*
import com.aptener.bluarmor.R
import com.aptener.bluarmor.service.*
import com.aptener.bluarmor.ui.theme.BluArmorTheme
import com.aptener.bluarmor.ui.theme.ColorNegative
import com.aptener.bluarmor.ui.theme.ColorPositive
import java.text.SimpleDateFormat
import java.util.*
import com.aptener.bluarmor.service.bluconnect.model.*
import com.aptener.bluarmor.service.bluconnect.BluConnectManager


@Composable
fun MusicControls(
    modifier: Modifier = Modifier,
    musicState: MusicState,
    musicAction: (musicAction: MusicAction) -> Unit
) {

    BluArmorTheme {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.End
        ) {
            with(musicState) {
                if (this is MusicState.Playing)
                    Column(modifier = Modifier.weight(1f)) {
                        Text(
                            text = this@with.title,
                            color = Color.White,
                            fontWeight = FontWeight.Medium,
                            style = MaterialTheme.typography.body2,
                            maxLines = 1
                        )
                        Text(
                            text = this@with.artist,
                            color = Color.White,
                            fontWeight = FontWeight.SemiBold,
                            style = MaterialTheme.typography.caption,
                            modifier = Modifier.alpha(0.7f),
                            maxLines = 1
                        )
                    }
            }

            Icon(
                imageVector = ImageVector.vectorResource(id = R.drawable.ic_control_next),
                contentDescription = null,
                tint = Color.White,
                modifier = Modifier
                    .clip(shape = CircleShape)
                    .clickable {
                        musicAction.invoke(MusicAction.PREVIOUS)
                    }
                    .padding(16.dp)
                    .rotate(180f)
                        .width(32.dp),
            )

            Icon(
                imageVector = ImageVector.vectorResource(
                    id = if (musicState is MusicState.Idle) R.drawable.ic_control_play
                    else R.drawable.ic_control_pause
                ),
                contentDescription = null,
                tint = Color.White,
                modifier = Modifier
                    .clip(shape = CircleShape)
                    .clickable {
                        if (musicState is MusicState.Idle)
                            musicAction.invoke(MusicAction.PLAY)
                        else
                            musicAction.invoke(MusicAction.PAUSE)
                    }
                    .padding(vertical = 12.dp, horizontal = 20.dp)
                    .width(32.dp),
            )

            Icon(
                imageVector = ImageVector.vectorResource(id = R.drawable.ic_control_next),
                contentDescription = null,
                tint = Color.White,
                modifier = Modifier
                    .clip(shape = CircleShape)
                    .clickable {
                        musicAction.invoke(MusicAction.NEXT)
                    }
                    .padding(16.dp)
                    .width(32.dp),
            )
        }
    }
}


@Composable
fun DialerControls(
    modifier: Modifier = Modifier,
    phoneCallState: PhoneCallState,
    phoneCallAction: (phoneCallAction: PhoneCallAction) -> Unit
) {
    BluArmorTheme {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = modifier.fillMaxWidth()
        ) {

            with(phoneCallState) {
                Column(modifier = Modifier.weight(1f)) {
                    Text(
                        text = when (this@with) {
                            PhoneCallState.Idle -> String()
                            is PhoneCallState.Incoming -> this@with.contactName
                            is PhoneCallState.MissedCall -> this@with.contactName
                            is PhoneCallState.Active -> this@with.contactName
                            is PhoneCallState.Outgoing -> this@with.contactName
                        },
                        color = Color.White,
                        fontWeight = FontWeight.Medium,
                        style = MaterialTheme.typography.body2
                    )
                    Text(
                        text = when (this@with) {
                            PhoneCallState.Idle -> String()
                            is PhoneCallState.Incoming -> this@with.number
                            is PhoneCallState.Active -> this@with.number
                            is PhoneCallState.MissedCall -> this@with.number
                            is PhoneCallState.Outgoing -> this@with.number
                        },
                        color = Color.White,
                        fontWeight = FontWeight.SemiBold,
                        style = MaterialTheme.typography.caption,
                        modifier = Modifier.alpha(0.7f)
                    )

                    if (this@with is PhoneCallState.MissedCall)
                        Text(
                            text = "Missed Call - ${SimpleDateFormat("h:m a").format(this@with.dateTime.time)}".uppercase(),
                            color = Color.White,
                            fontWeight = FontWeight.SemiBold,
                            style = MaterialTheme.typography.overline,
                            modifier = Modifier.alpha(0.7f)
                        )

                }
            }

            if (phoneCallState is PhoneCallState.Incoming)
                Icon(
                    imageVector = ImageVector.vectorResource(id = R.drawable.ic_phone),
                    contentDescription = null,
                    tint = Color.White,
                    modifier = Modifier
                        .clip(shape = CircleShape)
                        .clickable {
                            phoneCallAction(PhoneCallAction.ANSWER)
                        }
                        .padding(vertical = 12.dp, horizontal = 12.dp)
                        .width(48.dp)
                        .height(48.dp)
                        .background(color = ColorPositive, CircleShape)
                        .padding(9.dp),
                )
            if (phoneCallState is PhoneCallState.Incoming || phoneCallState is PhoneCallState.Active)
                Icon(
                    imageVector = ImageVector.vectorResource(id = R.drawable.ic_phone),
                    contentDescription = null,
                    tint = Color.White,
                    modifier = Modifier
                        .clip(shape = CircleShape)
                        .clickable {
                            if (phoneCallState is PhoneCallState.Incoming)
                                phoneCallAction(PhoneCallAction.REJECT)
                            else
                                phoneCallAction(PhoneCallAction.DROP)
                        }
                        .padding(vertical = 12.dp, horizontal = 12.dp)
                        .width(48.dp)
                        .height(48.dp)
                        .background(color = ColorNegative, CircleShape)
                        .rotate(135f)
                        .padding(9.dp),
                )
            else if (phoneCallState is PhoneCallState.MissedCall)
                Icon(
                    imageVector = ImageVector.vectorResource(id = R.drawable.ic_phone_missed),
                    contentDescription = null,
                    tint = Color.White,
                    modifier = Modifier
                        .clip(shape = CircleShape)
                        .padding(vertical = 12.dp, horizontal = 12.dp)
                        .width(48.dp)
                        .height(48.dp)
                        .background(color = ColorNegative, CircleShape)
                        .padding(9.dp),
                )

        }
    }
}
