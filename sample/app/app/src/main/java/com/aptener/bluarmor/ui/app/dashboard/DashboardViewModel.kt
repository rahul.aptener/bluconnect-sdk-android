package com.aptener.bluarmor.ui.app.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import com.aptener.bluarmor.baseutils.asMutableLiveData
import com.aptener.bluarmor.baseutils.logE
import com.aptener.bluarmor.service.bluconnect.model.*
import com.aptener.bluarmor.ui.AppDestination
import com.aptener.bluarmor.ui.util.navigate
import com.aptener.bluconnect.BluArmorModel
import com.aptener.bluconnect.device.sxcontrol.Battery
import com.aptener.bluconnect.device.sxcontrol.OtaSwitch
import com.aptener.bluconnect.device.sxcontrol.SpeedDial
import com.aptener.bluconnect.device.sxcontrol.VoiceNoteRecorder
import com.aptener.bluconnect.device.sxcontrol.VoiceNoteRecorder.*
import com.aptener.bluconnect.device.sxcontrol.Volume
import com.aptener.bluconnect.repo.Buddy
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import timber.log.Timber

@AssistedFactory
interface DashboardViewModelAssistedFactory {
    fun create(bluArmorInterface: BluArmorInterface): DashboardViewModel
}

interface DashboardViewModelInterface {
    val bluArmorModel: LiveData<BluArmorModel>
    val otaSwitchState: LiveData<OtaSwitch.OtaSwitchState>
    val deviceInfo: LiveData<DeviceInfo>
    val deviceDetails: LiveData<com.aptener.bluconnect.device.sxcontrol.DeviceInfo.DeviceDetails>
    val batteryLevel: LiveData<Int>
    val chargingState: LiveData<Battery.ChargingState>
    val musicState: LiveData<MusicState>
    val voiceNoteRecorderState: LiveData<VoiceNoteRecorderState>
    val phoneCallState: LiveData<PhoneCallState>
    val volumeLevel: LiveData<Volume.VolumeLevel>
    val rideLynkIntercomState: LiveData<RideLynkIntercomState>

    val speedDialState: LiveData<SpeedDial.SpeedDialState>
    val speedDialContacts: LiveData<Array<Buddy?>>

    fun volumeControlAction(volumeControlAction: VolumeControlAction)
    fun musicAction(musicAction: MusicAction)
    fun voiceNoteRecordAction(action: VoiceNoteRecorderActions)
    fun phoneCallAction(phoneCallAction: PhoneCallAction)
    fun rideLynkAction(rideLynkAction: RideLynkAction)
    fun invokeVoiceAssistant()
    fun switchToOtaMode()
    fun dialSpeedDial(speedDialUser: Buddy)

    fun navigateToPreferences(navController: NavController)
}

class DashboardViewModel @AssistedInject constructor(@Assisted private val bluArmorInterface: BluArmorInterface) :
    ViewModel(),
    DashboardViewModelInterface {

    override val bluArmorModel: LiveData<BluArmorModel> =
        bluArmorInterface.bluConnectManager.bluArmorModel
    override val otaSwitchState: LiveData<OtaSwitch.OtaSwitchState> =
        bluArmorInterface.bluConnectManager.otaSwitchState
    override val deviceInfo: LiveData<DeviceInfo> = bluArmorInterface.bluConnectManager.deviceInfo
    override val deviceDetails: LiveData<com.aptener.bluconnect.device.sxcontrol.DeviceInfo.DeviceDetails> =
        bluArmorInterface.bluConnectManager.deviceDetails
    override val batteryLevel: LiveData<Int> = bluArmorInterface.bluConnectManager.batteryLevel
    override val chargingState: LiveData<Battery.ChargingState> =
        bluArmorInterface.bluConnectManager.chargingState
    override val musicState: LiveData<MusicState> = bluArmorInterface.bluConnectManager.musicState
    override val voiceNoteRecorderState: LiveData<VoiceNoteRecorderState> =
        bluArmorInterface.bluConnectManager.voiceNoteRecorderState
    override val phoneCallState: LiveData<PhoneCallState> =
        bluArmorInterface.bluConnectManager.phoneCallState
    override val rideLynkIntercomState: LiveData<RideLynkIntercomState> =
        bluArmorInterface.bluConnectManager.rideLynkIntercomState
    override val volumeLevel: LiveData<Volume.VolumeLevel> =
        bluArmorInterface.bluConnectManager.volumeState
    override val speedDialContacts: LiveData<Array<Buddy?>> =
        bluArmorInterface.bluConnectManager.speedDialContacts
    override val speedDialState: LiveData<SpeedDial.SpeedDialState> =
        bluArmorInterface.bluConnectManager.speedDialState

    override fun dialSpeedDial(speedDialUser: Buddy) =
        bluArmorInterface.bluConnectManager.dialSpeedDial(speedDialUser)

    init {
        "DashboardViewViewModel CREATED".logE
    }

    override fun volumeControlAction(volumeControlAction: VolumeControlAction) {
        if (volumeControlAction is VolumeControlAction.ChangeVolume) {
            Timber.e("volumeControlAction: ${volumeControlAction.volumeLevel}")
            volumeLevel.asMutableLiveData()?.value =
                volumeLevel.value?.copy(volumeLevel = volumeControlAction.volumeLevel)
        }
        bluArmorInterface.bluConnectManager.volumeAction(volumeControlAction)
    }

    override fun musicAction(musicAction: MusicAction) =
        bluArmorInterface.bluConnectManager.musicAction(musicAction)

    override fun voiceNoteRecordAction(action: VoiceNoteRecorderActions) =
        bluArmorInterface.bluConnectManager.voiceNoteRecorderAction(action)

    override fun phoneCallAction(phoneCallAction: PhoneCallAction) =
        bluArmorInterface.bluConnectManager.phoneCallAction(phoneCallAction)

    override fun rideLynkAction(rideLynkAction: RideLynkAction) =
        bluArmorInterface.bluConnectManager.rideLynkAction(rideLynkAction)

    override fun switchToOtaMode() = bluArmorInterface.bluConnectManager.switchToOtaMode()
    override fun invokeVoiceAssistant() = bluArmorInterface.bluConnectManager.invokeVoiceAssistant()

    override fun navigateToPreferences(navController: NavController) {
        navController.navigate(AppDestination.PreferenceDest)
    }

    override fun onCleared() {
        "DashboardViewViewModel DESTROYED".logE
        super.onCleared()
    }

    class Factory(
        private val assistedFactory: DashboardViewModelAssistedFactory,
        private val bluArmorInterface: BluArmorInterface,
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return assistedFactory.create(bluArmorInterface) as T
        }
    }
}
