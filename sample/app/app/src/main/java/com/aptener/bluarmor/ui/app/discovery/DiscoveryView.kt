package com.aptener.bluarmor.ui.app.discovery

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.imageResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.aptener.bluarmor.BuildConfig
import com.aptener.bluarmor.R
import com.aptener.bluarmor.service.bluconnect.model.DiscoveryState
import com.aptener.bluarmor.ui.theme.BluArmorTheme
import com.aptener.bluarmor.ui.theme.ButtonPrimary

@Composable
fun DiscoveryView(
    navController: NavController,
    vm: DiscoveryViewViewModelInterface
) {
    val discoveryState: DiscoveryState by vm.discoveryState.observeAsState(initial = DiscoveryState.Scanning)

    val context = LocalContext.current
    BluArmorTheme {
        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {

            TopAppBar(
                backgroundColor = MaterialTheme.colors.background,
                elevation = 0.dp,
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.End,
                ) {

                    if (BuildConfig.DEBUG)
                        ButtonPrimary(label = "Export Logs", onClick = {
                            vm.exportLogs()
                            Toast
                                .makeText(context, "Exported at: internalStorage/Android/media/com.aptener.bluarmor.dev/logs/", Toast.LENGTH_LONG)
                                .show()
                        })

                    Image(
                        imageVector = ImageVector.vectorResource(R.drawable.ic_options),
                        contentDescription = null,
                        modifier = Modifier
                            .padding(0.dp)
                            .clickable {
                                vm.navigateToPreferences(navController)

                            }
                    )
                }
            }

            DiscoveryViewHeader(
                modifier = Modifier
                    .padding(horizontal = 32.dp)
                    .fillMaxWidth(),
                discoveryState = discoveryState
            )

            Spacer(modifier = Modifier.weight(0.3f))

            when (discoveryState) {
                DiscoveryState.BluetoothOff -> BluetoothAlertView(
                    modifier =
                    Modifier
                        .align(alignment = Alignment.CenterHorizontally)
                        .padding(horizontal = 32.dp),
                    turnOn = vm::turnOnBluetooth
                )

                DiscoveryState.LocationOff -> LocationAlertView(
                    modifier =
                    Modifier
                        .align(alignment = Alignment.CenterHorizontally)
                        .padding(horizontal = 32.dp),
                    turnOn = vm::turnOnGps
                )

                is DiscoveryState.Connected,
                is DiscoveryState.Connecting,
                DiscoveryState.Scanning -> Image(
                    bitmap = ImageBitmap.imageResource(id = R.drawable.sx20),
                    contentDescription = "BluArmor Icon",
                    modifier = Modifier
                        .align(Alignment.CenterHorizontally)
                        .fillMaxWidth(0.75f)
                        .aspectRatio(1f),
                    contentScale = ContentScale.Fit,
                )
            }

            Spacer(modifier = Modifier.weight(0.7f))
        }
    }
}

@Composable
fun DiscoveryViewHeader(
    modifier: Modifier = Modifier,
    discoveryState: DiscoveryState
) {
    Column(
        modifier = modifier
    ) {
        Text(
            text = when (discoveryState) {
                DiscoveryState.Scanning -> stringResource(id = R.string.looking_bluarmor)
                DiscoveryState.BluetoothOff -> stringResource(id = R.string.bluetooth_off)
                DiscoveryState.LocationOff -> stringResource(id = R.string.location_off)
                is DiscoveryState.Connected -> stringResource(
                    id = R.string.connecting_to,
                    discoveryState.deviceName
                )
                is DiscoveryState.Connecting -> stringResource(
                    id = R.string.connecting_to,
                    discoveryState.deviceName
                )
            },
            style = MaterialTheme.typography.h4,
            color = Color.White,
            fontWeight = FontWeight.Bold
        )
        Text(
            text = stringResource(id = R.string.make_sure_bluarmor_on),
            style = MaterialTheme.typography.subtitle1,
            color = Color.White,
        )
    }

}

@Composable
fun BluetoothAlertView(modifier: Modifier = Modifier, turnOn: () -> Unit) {
    BluArmorTheme {
        Card(
            shape = RoundedCornerShape(10.dp),
            modifier = modifier
                .fillMaxWidth()
                .wrapContentHeight(),
            backgroundColor = Color(0x38FFFFFF),
            elevation = 0.dp
        ) {
            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                Image(
                    imageVector = ImageVector.vectorResource(R.drawable.ic_bluetooth_round),
                    contentDescription = null,
                    modifier = Modifier
                        .padding(top = 32.dp)
                        .width(56.dp)
                        .height(56.dp)
                        .aspectRatio(1f)
                )

                Text(
                    text = stringResource(id = R.string.bluetooth_off_tap_to_turn_on),
                    style = MaterialTheme.typography.subtitle2,
                    color = Color.White,
                    modifier = Modifier.padding(16.dp),
                    fontWeight = FontWeight.Normal,
                    textAlign = TextAlign.Center
                )

//                Button(
//                    onClick = { turnOn.invoke() },
//                    shape = RoundedCornerShape(50),
//                    elevation = ButtonDefaults.elevation(0.dp),
//                    colors = ButtonDefaults.buttonColors(
//                        backgroundColor = ColorPositive,
//                        contentColor = Color.White
//                    ),
//                    modifier = Modifier
//                        .fillMaxWidth(0.8f)
//                        .padding(bottom = 32.dp)
//                        .height(46.dp)
//
//                ) {
//                    Text(
//                        text = "Turn On".uppercase(),
//                        style = MaterialTheme.typography.subtitle1,
//                        fontWeight = FontWeight.Medium,
//                    )
//                }

            }
        }
    }
}

@Composable
fun LocationAlertView(modifier: Modifier = Modifier, turnOn: () -> Unit) {
    BluArmorTheme {
        Card(
            shape = RoundedCornerShape(10.dp),
            modifier = modifier
                .fillMaxWidth()
                .wrapContentHeight(),
            backgroundColor = Color(0x38FFFFFF),
            elevation = 0.dp
        ) {
            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                Image(
                    imageVector = ImageVector.vectorResource(R.drawable.ic_gps_round),
                    contentDescription = null,
                    modifier = Modifier
                        .padding(top = 32.dp)
                        .width(56.dp)
                        .height(56.dp)
                        .aspectRatio(1f)
                )

                Text(
                    text = stringResource(id = R.string.location_off_tap_to_turn_on),
                    style = MaterialTheme.typography.subtitle2,
                    color = Color.White,
                    modifier = Modifier.padding(16.dp),
                    fontWeight = FontWeight.Normal,
                    textAlign = TextAlign.Center
                )

//                Button(
//                    onClick = { turnOn.invoke() },
//                    shape = RoundedCornerShape(50),
//                    elevation = ButtonDefaults.elevation(0.dp),
//                    colors = ButtonDefaults.buttonColors(
//                        backgroundColor = ColorPositive,
//                        contentColor = Color.White
//                    ),
//                    modifier = Modifier
//                        .fillMaxWidth(0.8f)
//                        .padding(bottom = 32.dp)
//                        .height(46.dp)
//
//                ) {
//                    Text(
//                        text = "Turn On".uppercase(),
//                        style = MaterialTheme.typography.subtitle1,
//                        fontWeight = FontWeight.Medium,
//                    )
//                }

            }
        }
    }
}

@Preview(
    showBackground = true
)
@Composable
fun Preview() {
    DiscoveryView(navController = rememberNavController(), object : DiscoveryViewViewModelInterface {
        override val discoveryState: LiveData<DiscoveryState>
            get() = MutableLiveData()

        override fun turnOnBluetooth() {

        }

        override fun turnOnGps() {

        }

        override fun exportLogs() {

        }

        override fun navigateToPreferences(navController: NavController) {

        }

    })
}