package com.aptener.bluarmor.ui.app.discovery

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import com.aptener.bluarmor.baseutils.logE
import com.aptener.bluarmor.service.bluconnect.model.BluArmorInterface
import com.aptener.bluarmor.service.bluconnect.model.DiscoveryState
import com.aptener.bluarmor.ui.AppDestination
import com.aptener.bluarmor.ui.util.navigate
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject

@AssistedFactory
interface DiscoveryViewViewModelAssistedFactory {
    fun create(bluArmorInterface: BluArmorInterface): DiscoveryViewViewModel
}

interface DiscoveryViewViewModelInterface {
    val discoveryState: LiveData<DiscoveryState>
    fun turnOnBluetooth()
    fun turnOnGps()

    fun exportLogs()

    fun navigateToPreferences(navController: NavController)

}

class DiscoveryViewViewModel @AssistedInject constructor(@Assisted private val bluArmorInterface: BluArmorInterface) : ViewModel(), DiscoveryViewViewModelInterface {

    override val discoveryState: LiveData<DiscoveryState> = bluArmorInterface.bluConnectManager.discoveryState

    override fun turnOnBluetooth() = bluArmorInterface.bluConnectManager.turnOnBluetooth()
    override fun turnOnGps() = bluArmorInterface.bluConnectManager.turnOnGps()

    override fun exportLogs() {
        bluArmorInterface.bluConnectManager.exportLog()
    }

    override fun navigateToPreferences(navController: NavController) {
        navController.navigate(AppDestination.PreferenceDest)
    }

    init {
        "DiscoveryViewViewModel CREATED".logE
    }

    override fun onCleared() {
        "DiscoveryViewViewModel DESTROYED".logE
        super.onCleared()
    }

    class Factory(
        private val assistedFactory: DiscoveryViewViewModelAssistedFactory,
        private val bluArmorInterface: BluArmorInterface,
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return assistedFactory.create(bluArmorInterface) as T
        }
    }

}
