package com.aptener.bluarmor.ui.app.landing

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import com.aptener.bluarmor.ui.AppDestination
import com.aptener.bluarmor.ui.util.intervalFlow
import com.aptener.bluarmor.ui.util.navigate
import com.aptener.bluconnect.client.ConnectionState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject


interface LandingViewModelInterface {
    fun navigateToSeekPermission(navController: NavController)
    fun navigateToDiscoveryView(navController: NavController)
    fun navigateToDashboardView(navController: NavController)
}

@HiltViewModel
class LandingViewModel @Inject constructor() : ViewModel(), LandingViewModelInterface {

    val bluArmorConnectionState: StateFlow<ConnectionState?> = MutableStateFlow(null)

    private val timerJob: Job = viewModelScope.intervalFlow(
        1000,
        2 * 1000,
        {}
    )
    {
        
    }

    init {
    }

    override fun onCleared() {
        timerJob.cancel()
        super.onCleared()
    }

    override fun navigateToSeekPermission(navController: NavController) = navController.navigate(AppDestination.PermissionDest)
    override fun navigateToDiscoveryView(navController: NavController) = navController.navigate(AppDestination.DiscoveryDest)
    override fun navigateToDashboardView(navController: NavController) = navController.navigate(AppDestination.DashboardDest)

}