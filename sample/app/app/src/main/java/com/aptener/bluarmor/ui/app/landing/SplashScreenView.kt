package com.aptener.bluarmor.ui.app.landing

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.aptener.bluarmor.MainActivityViewModel
import com.aptener.bluarmor.R
import com.aptener.bluarmor.ui.theme.BluArmorTheme

@Composable
fun SplashScreenView(
    navController: NavController,
    vm: LandingViewModelInterface
) {
    BluArmorTheme {
        ConstraintLayout(
            modifier = Modifier.fillMaxSize()
        ) {
            val (logo, textLogo) = createRefs()
            Image(
                imageVector = ImageVector
                    .vectorResource(id = R.drawable.ic_logo_bluarmor),
                contentDescription = "BluArmor Icon",
                contentScale = ContentScale.Fit,
                modifier = Modifier
                    .padding(16.dp)
                    .height(100.dp)
                    .aspectRatio(1f, false)
                    .constrainAs(logo) {
                        top.linkTo(parent.top)
                        bottom.linkTo(parent.bottom)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    },
            )

            Image(
                imageVector = ImageVector
                    .vectorResource(id = R.drawable.ic_bluarmor_text),
                contentDescription = "BluArmor Icon",
                contentScale = ContentScale.Fit,
                modifier = Modifier
                    .height(28.dp)
                    .fillMaxWidth()
                    .constrainAs(textLogo) {
                        top.linkTo(logo.bottom)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    },
            )

        }
    }
}

@Preview
@Composable
fun PreviewSplashScreenView() {
    SplashScreenView(
        navController = rememberNavController(),
        vm = object : LandingViewModelInterface {
            override fun navigateToSeekPermission(navController: NavController) {
                
            }

            override fun navigateToDiscoveryView(navController: NavController) {
                
            }

            override fun navigateToDashboardView(navController: NavController) {
                
            }

        }
    )
}
