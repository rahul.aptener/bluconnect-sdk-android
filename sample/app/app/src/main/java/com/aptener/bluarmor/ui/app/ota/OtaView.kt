package com.aptener.bluarmor.ui.app.ota

import android.annotation.SuppressLint
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.imageResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.aptener.bluarmor.R
import com.aptener.bluarmor.ui.theme.BluArmorTheme
import com.aptener.bluarmor.ui.theme.ButtonPrimary
import com.aptener.bluarmor.ui.theme.ColorPositive
import com.aptener.bluarmor.ui.theme.ColorSharkDark


@SuppressLint("Range")
private fun getFileName(uri: Uri?, context: Context): String? {
    uri ?: return null

    var fileName: String? = null

    if (uri.scheme.equals("file")) {
        fileName = uri.lastPathSegment
    } else {
        var cursor: Cursor? = null
        try {
            cursor = context.contentResolver.query(
                uri, arrayOf(
                    MediaStore.Images.ImageColumns.DISPLAY_NAME
                ), null, null, null
            )
            if (cursor != null && cursor.moveToFirst()) {
                fileName = cursor.getString(cursor.getColumnIndex(MediaStore.Images.ImageColumns.DISPLAY_NAME))

            }
        } finally {
            cursor?.close()
        }
    }

    return fileName
}

@Composable
fun DropdownDemo(click: (uri: Uri) -> Unit) {

    var path: Uri? by remember { mutableStateOf(null) }

    val pickOtaPathLauncher = rememberLauncherForActivityResult(ActivityResultContracts.GetContent()) { uri ->
        if (uri != null) {
            path = uri
        }
    }

    var clicked by remember { mutableStateOf(false) }

    var selectedIndex by remember { mutableStateOf(0) }

    Box(
        modifier = Modifier
            .wrapContentSize(Alignment.TopStart)
    ) {

        Row {
            Spacer(modifier = Modifier.width(16.dp))

            Text(
                getFileName(path, LocalContext.current) ?: "Select",
                modifier = Modifier
                    .weight(1f)
                    .clickable(onClick = { pickOtaPathLauncher.launch("*/*") })
                    .background(
                        ColorSharkDark
                    )
                    .padding(8.dp)
            )

            Spacer(modifier = Modifier.width(32.dp))
            if (!clicked)
                ButtonPrimary(label = "Start", onClick = {
                    clicked = true
                    click.invoke(path ?: return@ButtonPrimary)
                })

            Spacer(modifier = Modifier.width(16.dp))

        }


/*        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier
                .padding(32.dp)
                .fillMaxWidth()

        ) {
            items.forEachIndexed { index, s ->
                DropdownMenuItem(onClick = {
                    selectedIndex = index
                    expanded = false
                    clicked = false
                }) {

                    Text(
                        text = s,
                        style = MaterialTheme.typography.body2,
                        fontWeight = FontWeight.SemiBold,
                        color = Color.White,
                        modifier = Modifier.padding(8.dp)
                    )
                }
            }
        }*/
    }
}

@Composable
fun OtaView(navController: NavController, vm: OtaViewModelInterface) {
    val progress by vm.progress.observeAsState(initial = 0)
    val rideGridProgress by vm.rideGridProgress.observeAsState(initial = 0)
    BluArmorTheme {
        Scaffold(topBar = {
            TopAppBar(
                backgroundColor = MaterialTheme.colors.background,
                elevation = 0.dp,
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.End,
                ) {
                    /* Image(
                         imageVector = ImageVector.vectorResource(R.drawable.ic_options),
                         contentDescription = null,
                         modifier = Modifier
                             .padding(0.dp)
                             .clickable {
 //                                vm.navigateToPreferences(navController)
                             }
                     )*/
                }
            }

        }) {
            Column(
                modifier = Modifier
                    .fillMaxSize(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {


//                DropdownDemo(click = vm::startOta)

                Spacer(modifier = Modifier.weight(.5f))
                Image(
                    bitmap = ImageBitmap.imageResource(id = R.drawable.sx20),
                    contentDescription = "BluArmor Icon",
                    modifier = Modifier
                        .aspectRatio(1f)
                        .fillMaxWidth(0.75f),
                    contentScale = ContentScale.Fit,
                )
                Spacer(modifier = Modifier.weight(.25f))

                Box(contentAlignment = Alignment.Center) {
                    Spacer(
                        modifier = Modifier
                            .height(90.dp)
                            .width(90.dp)
                            .background(
                                color = Color(0x2EFFFFFF),
                                shape = CircleShape
                            )
                            .border(width = 10.dp, color = Color(0x66FFFFFF), shape = CircleShape)

                    )

                    CircularProgressIndicator(
                        progress = progress / 100f,
                        modifier = Modifier
                            .height(90.dp)
                            .width(90.dp),
                        color = ColorPositive,
                        strokeWidth = 10.dp
                    )


                    CircularProgressIndicator(
                        progress = rideGridProgress / 100f,
                        modifier = Modifier
                            .height(60.dp)
                            .width(60.dp),
                        color = ColorPositive,
                        strokeWidth = 3.dp
                    )

                    Text(
                        text = "${progress}%",
                        style = MaterialTheme.typography.h6,
                        color = Color.White,
                        fontWeight = FontWeight.Black
                    )

                }
                Spacer(modifier = Modifier.weight(.25f))

            }
        }
    }
}

@Preview(
    showBackground = true
)
@Composable
fun Preview() {
    OtaView(rememberNavController(), object : OtaViewModelInterface {
        override val progress: LiveData<Int>
            get() = MutableLiveData()
        override val rideGridProgress: LiveData<Int>
            get() = MutableLiveData()

        override fun startOta(path: Uri) {
        }

    })
}