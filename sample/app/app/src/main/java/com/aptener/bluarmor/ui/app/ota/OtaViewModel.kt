package com.aptener.bluarmor.ui.app.ota

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.aptener.bluarmor.service.bluconnect.model.BluArmorInterface
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject

@AssistedFactory
interface OtaViewModelAssistedFactory {
    fun create(bluArmorInterface: BluArmorInterface): OtaViewModel
}

interface OtaViewModelInterface {
    val progress: LiveData<Int>
    val rideGridProgress: LiveData<Int>
    fun startOta(path: Uri)
}

class OtaViewModel @AssistedInject constructor(@Assisted private val bluArmorInterface: BluArmorInterface) : ViewModel(), OtaViewModelInterface {
    override val progress: LiveData<Int> = bluArmorInterface.bluConnectManager.otaProgress
    override val rideGridProgress: LiveData<Int> = bluArmorInterface.bluConnectManager.rideGridOtaProgress
    override fun startOta(path: Uri) = bluArmorInterface.bluConnectManager.startManualOta(path)

    class Factory(
        private val assistedFactory: OtaViewModelAssistedFactory,
        private val bluArmorInterface: BluArmorInterface,
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return assistedFactory.create(bluArmorInterface) as T
        }
    }
}