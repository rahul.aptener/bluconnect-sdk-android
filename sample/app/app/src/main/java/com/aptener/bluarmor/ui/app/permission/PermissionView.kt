package com.aptener.bluarmor.ui.app.permission

import android.os.Build
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.aptener.bluarmor.R
import com.aptener.bluarmor.ui.theme.BluArmorTheme
import com.aptener.bluarmor.ui.theme.ColorPositive
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject

@AssistedFactory
interface PermissionViewViewModelAssistedFactory {
    fun create(): PermissionViewViewViewModel
}


val permissionViewViewInterface = object : PermissionViewViewInterface {
    override fun navigateToSystemSettings() {
        
    }

}


interface PermissionViewViewInterface {
    fun navigateToSystemSettings()
}

class PermissionViewViewViewModel @AssistedInject constructor() : ViewModel(),
    PermissionViewViewInterface {

    override fun navigateToSystemSettings() {
//        with(Intent()) {
//            this.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
//            this.data =
//                Uri.fromParts("package", BuildConfig.APPLICATION_ID, null)
//            startActivity(context, this, null)
//        }
    }


    class Factory(
        private val assistedFactory: PermissionViewViewModelAssistedFactory,
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return assistedFactory.create() as T
        }
    }
}

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun PermissionViewView(
    navController: NavController,
    vm: PermissionViewViewInterface,
) {

    BluArmorTheme {
        Column(
            modifier = Modifier
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            Spacer(modifier = Modifier.weight(0.14f))

            Build.VERSION.SDK_INT >= Build.VERSION_CODES.S
            Icon(
                imageVector = ImageVector.vectorResource(
                    id =
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)
                        R.drawable.ic_permission_bluetooth
                    else
                        R.drawable.ic_permission_location

                ),
                contentDescription = null,
                tint = Color.White,
                modifier = Modifier
                    .fillMaxWidth(0.6f)
                    .aspectRatio(1f),
            )

            Spacer(modifier = Modifier.weight(0.3f))

            Text(
                text = stringResource(
                    id =
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)
                        R.string.bluetooth_permission
                    else
                        R.string.location_permission
                ),
                style = MaterialTheme.typography.subtitle1,
                fontWeight = FontWeight.SemiBold,
                color = Color.White,
                modifier = Modifier.fillMaxWidth(0.8f),
                textAlign = TextAlign.Center
            )


            Text(
                text = stringResource(
                    id =
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)
                        R.string.bluetooth_permission_description
                    else
                        R.string.location_permission_description
                ),
                style = MaterialTheme.typography.subtitle2,
                fontWeight = FontWeight.Normal,
                color = Color.White,
                modifier = Modifier
                    .fillMaxWidth(0.8f)
                    .padding(vertical = 8.dp),
                textAlign = TextAlign.Center
            )


//            Spacer(modifier = Modifier.weight(0.1f))
//            Button(
//                onClick = { vm.navigateToSystemSettings() },
//                shape = RoundedCornerShape(50),
//                elevation = ButtonDefaults.elevation(0.dp),
//                colors = ButtonDefaults.buttonColors(
//                    backgroundColor = ColorPositive,
//                    contentColor = Color.White
//                ),
//                modifier = Modifier
//                    .padding(vertical = 42.dp)
//                    .fillMaxWidth(0.75f)
//                    .height(46.dp)
//
//            ) {
//                Text(
//                    text = "Allow".uppercase(),
//                    style = MaterialTheme.typography.subtitle2,
//                    fontWeight = FontWeight.Medium,
//                )
//            }

//
//            val imageRes = loadPicture(R.drawable.ic_helmet).value
//
//            if (imageRes != null)
//                Icon(
//                    bitmap = imageRes.asImageBitmap(),
//                    contentDescription = null,
//                    tint = Color.White,
//                    modifier = Modifier
//                        .clip(shape = CircleShape)
//                        .width(38.dp)
//                        .height(38.dp)
//                        .padding(bottom = 8.dp),
//                )

        }

    }

}


@Preview(
    showBackground = true
)
@Composable
fun Preview() {
    PermissionViewView(rememberNavController(), permissionViewViewInterface)
}