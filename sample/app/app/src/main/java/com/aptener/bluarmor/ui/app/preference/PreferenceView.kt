package com.aptener.bluarmor.ui.app.preference

import android.content.Context
import androidx.compose.animation.*
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.aptener.bluarmor.R
import com.aptener.bluarmor.service.bluconnect.model.DeviceInfo
import com.aptener.bluarmor.service.bluconnect.model.DiscoveryState
import com.aptener.bluarmor.ui.app.buddylist.DeviceInfoCardCollapsedWithRename
import com.aptener.bluarmor.ui.app.preference.PreferenceViewModelInterface.*
import com.aptener.bluarmor.ui.theme.BluArmorTheme
import com.aptener.bluarmor.ui.theme.ButtonPrimaryLarge
import com.aptener.bluarmor.ui.theme.ColorTextPrimary
import com.aptener.bluarmor.ui.theme.ColorTextPrimaryAccent
import com.aptener.bluconnect.device.sxcontrol.RideGrid
import com.aptener.bluconnect.device.sxcontrol.SecondPhone
import com.aptener.bluconnect.device.sxcontrol.Volume
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlin.math.roundToInt

@Composable
fun PreferenceView(navController: NavController, vm: PreferenceViewModelInterface) {

    val context = LocalContext.current
    val lifecycleOwner = LocalLifecycleOwner.current
    val coroutineContext = rememberCompositionContext()

    val deviceInfo: DeviceInfo? by vm.deviceInfo.observeAsState(null)
    val appVersion by vm.appVersion.observeAsState()
    val deviceInfoMetadata by vm.deviceDetails.observeAsState()
    val secondPhoneState by vm.secondPhoneState.observeAsState()

    val whatsAppAutoRead by vm.whatsAppAutoRead.collectAsState()
    val whatsAppAutoRespond by vm.whatsAppAutoRespond.collectAsState()
    val whatsAppAutoLocationShare by vm.whatsAppAutoLocationShare.collectAsState()
    val whatsAppAutoLocationShareContact by vm.whatsAppAutoLocationShareContact.collectAsState()
    val whatsAppDefaultMessage by vm.whatsAppDefaultMessage.collectAsState()

    val phoneAutoAnswer by vm.phoneAutoAnswer.collectAsState()
    val phoneAutoReject by vm.phoneAutoReject.collectAsState()

    val audioOverlayState: RideGrid.AudioOverlayState? by vm.audioOverlayState.observeAsState()
    val volumeBoost by vm.volumeBoost.collectAsState()
    val equalizerBoost by vm.equalizerBoost.collectAsState()
    val audioOverlayLevel by vm.audioOverlayLevel.observeAsState(initial = 1f)
    val voicePromptLevel by vm.voicePromptLevel.observeAsState()


    val renameSwitch by vm.renameSwitch.collectAsState()
    val deviceName by vm.deviceName.collectAsState()

    var expandedItemId: Int by rememberSaveable { mutableStateOf(-1) }
    val preferenceScrollState = rememberScrollState()

    fun expandItem(itemId: Int) {
        expandedItemId = if (expandedItemId == itemId) -1 else itemId
    }

    BluArmorTheme {
        Scaffold(topBar = {
            TopAppBar(
                backgroundColor = MaterialTheme.colors.background,
                elevation = 0.dp,
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween,
                ) {
                    Image(
                        imageVector = ImageVector.vectorResource(R.drawable.ic_back_nav),
                        contentDescription = null,
                        modifier = Modifier
                            .padding(0.dp)
                            .clickable {
                                navController.popBackStack()
                            }
                    )
                }
            }
        }) {
            Column {

                DeviceInfoCardCollapsedWithRename(
                    vm.discoveryState,
                    vm.deviceInfo,
                    vm.batteryLevel,
                    enableRename = renameSwitch,
                    deviceName = deviceName,
                    onNameFiledChange = vm::deviceNameChange,
                    rename = vm::confirmDeviceNameChange,
                    cancelRename = vm::cancelDeviceNameChange,
                    toggleRename = vm::toggleRenameSwitch
                )

                Column(
                    modifier = Modifier
                        .verticalScroll(preferenceScrollState)
                        .fillMaxSize()
                ) {

                    // WhatsApp
                    PreferenceItem(
                        modifier = Modifier,
                        bluArmorAppPreference = BluArmorAppPreference(
                            R.drawable.ic_pref_whatsapp,
                            R.string.whatsapp_title,
                            R.string.whatsapp_desc,
                        ),
                        position = 0,
                        expandBody = expandedItemId == 0,
                        expandItem = ::expandItem
                    ) {
                        SwitchWithLabel(
                            stringResource(id = R.string.whatsapp_auto_read_title),
                            stringResource(id = R.string.whatsapp_auto_read_desc),
                            whatsAppAutoRead,
                        ) { vm.toggleWhatsAppAutoRead(context, it) }

                        Spacer(
                            modifier = Modifier
                                .height(1.dp)
                                .fillMaxWidth()
                                .background(color = Color(0xff000000))
                        )

                        SwitchWithLabel(
                            stringResource(id = R.string.whatsapp_auto_respond),
                            stringResource(id = R.string.whatsapp_auto_respond_desc),
                            whatsAppAutoRespond
                        ) { vm.toggleWhatsAppAutoRespond(context, it) }

                        /*
                        Disabled / Require cloud implementation
                        Spacer(
                             modifier = Modifier
                                 .height(1.dp)
                                 .fillMaxWidth()
                                 .background(color = Color(0xff000000))
                         )

                         SwitchWithLabel(
                             stringResource(id = R.string.whatsapp_auto_share_location),
                             if (whatsAppAutoLocationShareContact.isNullOrBlank()) stringResource(id = R.string.whatsapp_auto_share_location_contact) else whatsAppAutoLocationShareContact,
                             whatsAppAutoLocationShare,
                             vm::toggleWhatsAppAutoLocationShare
                         )*/

                        Spacer(
                            modifier = Modifier
                                .height(1.dp)
                                .fillMaxWidth()
                                .background(color = Color(0xff000000))
                        )

                        Text(
                            text = stringResource(id = R.string.whatsapp_default_msg),
                            style = MaterialTheme.typography.subtitle1,
                            color = ColorTextPrimary,
                            fontWeight = FontWeight.SemiBold,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(start = 24.dp, end = 12.dp, top = 12.dp, bottom = 8.dp)
                        )

                        TextField(
                            value = whatsAppDefaultMessage,
                            onValueChange = vm::updateWhatsAppDefaultMessage,
                            maxLines = 6,
                            shape = RoundedCornerShape(10.dp),
                            colors = TextFieldDefaults.textFieldColors(
                                backgroundColor = Color(0x11FFFFFF),
                                focusedIndicatorColor = Color.Transparent,
                                unfocusedIndicatorColor = Color.Transparent
                            ),
                            modifier = Modifier
                                .fillMaxWidth()
                                .defaultMinSize(minHeight = 100.dp)
                                .padding(start = 24.dp, end = 12.dp, bottom = 12.dp),
                            textStyle = TextStyle(
                                fontFamily = MaterialTheme.typography.body2.fontFamily,
                                fontWeight = FontWeight.Normal,
                                fontSize = MaterialTheme.typography.body2.fontSize,
                                fontStyle = MaterialTheme.typography.body2.fontStyle,
                                color = ColorTextPrimaryAccent
                            )
                        )
                    }


                    // Phone Call
                    PreferenceItem(
                        modifier = Modifier,
                        bluArmorAppPreference = BluArmorAppPreference(
                            R.drawable.ic_pref_phone,
                            R.string.phone_call,
                            R.string.phone_call_desc,
                        ),
                        position = 1,
                        expandBody = expandedItemId == 1,
                        expandItem = ::expandItem
                    ) {

                        PreferenceItemOptionLabel(title = stringResource(id = R.string.auto_accept_incoming_calls))

                        RadioButtonWithLabel(
                            label = stringResource(id = R.string.accept_call_from_known_contacts),
                            modifier = Modifier
                                .clickable { vm.phoneAutoAnswerChange(context, PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_KNOWN) }
                                .padding(horizontal = 16.dp, vertical = 6.dp),
                            selected = phoneAutoAnswer == PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_KNOWN
                        )

                        RadioButtonWithLabel(
                            label = stringResource(id = R.string.accept_all_incoming_calls),
                            modifier = Modifier
                                .clickable { vm.phoneAutoAnswerChange(context, PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_ALL) }
                                .padding(horizontal = 16.dp, vertical = 6.dp),
                            selected = phoneAutoAnswer == PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_ALL
                        )

                        RadioButtonWithLabel(
                            label = stringResource(id = R.string.accept_call_button_click),
                            modifier = Modifier
                                .clickable { vm.phoneAutoAnswerChange(context, PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_MANUALLY) }
                                .padding(horizontal = 16.dp, vertical = 6.dp),
                            selected = phoneAutoAnswer == PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_MANUALLY
                        )

                        PreferenceItemOptionLabel(title = stringResource(id = R.string.auto_reject_incoming_calls))

                        RadioButtonWithLabel(
                            label = stringResource(id = R.string.reject_call_from_unknown_contacts),
                            modifier = Modifier
                                .clickable { vm.phoneAutoRejectChange(context, PhoneCallAutoRejectRadioOption.AUTO_REJECT_UNKNOWN) }
                                .padding(horizontal = 16.dp, vertical = 6.dp),
                            selected = phoneAutoReject == PhoneCallAutoRejectRadioOption.AUTO_REJECT_UNKNOWN
                        )

                        RadioButtonWithLabel(
                            label = stringResource(id = R.string.reject_all_incoming_calls),
                            modifier = Modifier
                                .clickable { vm.phoneAutoRejectChange(context, PhoneCallAutoRejectRadioOption.AUTO_REJECT_ALL) }
                                .padding(horizontal = 16.dp, vertical = 6.dp),
                            selected = phoneAutoReject == PhoneCallAutoRejectRadioOption.AUTO_REJECT_ALL
                        )

                        RadioButtonWithLabel(
                            label = stringResource(id = R.string.reject_call_button_click),
                            modifier = Modifier
                                .clickable { vm.phoneAutoRejectChange(context, PhoneCallAutoRejectRadioOption.AUTO_REJECT_MANUALLY) }
                                .padding(horizontal = 16.dp, vertical = 6.dp),
                            selected = phoneAutoReject == PhoneCallAutoRejectRadioOption.AUTO_REJECT_MANUALLY
                        )

                    }


                    // Audio Controls
                    PreferenceItem(
                        modifier = Modifier,
                        bluArmorAppPreference = BluArmorAppPreference(
                            R.drawable.ic_pref_audio,
                            R.string.audio_ctrl,
                            R.string.audio_ctrl_desc,
                        ),
                        position = 2,
                        expandBody = expandedItemId == 2,
                        expandItem = ::expandItem
                    ) {

                        PreferenceItemOptionLabel(title = stringResource(id = R.string.audio_ctrl_vol_boost))

                        RadioButtonWithLabel(
                            label = stringResource(id = R.string.audio_ctrl_vol_ease_on_ears),
                            modifier = Modifier
                                .clickable { vm.volumeBoostChange(VolumeBoostRadioOption.EASE_ON_EARS) }
                                .padding(horizontal = 16.dp, vertical = 6.dp),
                            selected = volumeBoost == VolumeBoostRadioOption.EASE_ON_EARS
                        )

                        RadioButtonWithLabel(
                            label = stringResource(id = R.string.audio_ctrl_vol_balanced),
                            modifier = Modifier
                                .clickable { vm.volumeBoostChange(VolumeBoostRadioOption.BALANCED) }
                                .padding(horizontal = 16.dp, vertical = 6.dp),
                            selected = volumeBoost == VolumeBoostRadioOption.BALANCED
                        )

                        RadioButtonWithLabel(
                            label = stringResource(id = R.string.audio_ctrl_vol_high_output),
                            modifier = Modifier
                                .clickable { vm.volumeBoostChange(VolumeBoostRadioOption.HIGH_OUTPUT) }
                                .padding(horizontal = 16.dp, vertical = 6.dp),
                            selected = volumeBoost == VolumeBoostRadioOption.HIGH_OUTPUT
                        )


                        PreferenceItemOptionLabel(title = stringResource(id = R.string.audio_ctrl_eq))

                        RadioButtonWithLabel(
                            label = stringResource(id = R.string.audio_ctrl_eq_bass_regular),
                            modifier = Modifier
                                .clickable { vm.equalizerBoostChange(EqualizerBoostRadioOption.REGULAR) }
                                .padding(horizontal = 16.dp, vertical = 6.dp),
                            selected = equalizerBoost == EqualizerBoostRadioOption.REGULAR
                        )

                        RadioButtonWithLabel(
                            label = stringResource(id = R.string.audio_ctrl_eq_bass_boost),
                            modifier = Modifier
                                .clickable { vm.equalizerBoostChange(EqualizerBoostRadioOption.BASS_BOOST) }
                                .padding(horizontal = 16.dp, vertical = 6.dp),
                            selected = equalizerBoost == EqualizerBoostRadioOption.BASS_BOOST
                        )

                        SwitchWithLabel(
                            stringResource(id = R.string.audio_ctrl_music_overlay),
                            stringResource(id = R.string.audio_ctrl_music_overlay_desc),
                            when (audioOverlayState) {
                                is RideGrid.AudioOverlayState.Off -> false
                                is RideGrid.AudioOverlayState.On -> true
                                null -> false
                            },
                            vm::audioOverlayChange
                        )
                        PreferenceItemOptionLabel(title = stringResource(id = R.string.audio_ctrl_music_overlay))


                        Slider(
                            value = audioOverlayLevel,
                            onValueChange = vm::audioOverlayLevelChange,
                            modifier = Modifier
                                .padding(vertical = 6.dp, horizontal = 24.dp),
                            valueRange = 0f..7f,
                            steps = 6
                        )

                        PreferenceItemOptionLabel(title = stringResource(id = R.string.voice_prompt_level))

                        RadioButtonWithLabel(
                            label = stringResource(id = R.string.voice_prompt_level_1),
                            modifier = Modifier
                                .clickable { vm.changeVoicePromptLevel(Volume.VoicePromptVerbosityLevel.ALL) }
                                .padding(horizontal = 16.dp, vertical = 6.dp),
                            selected = voicePromptLevel == Volume.VoicePromptVerbosityLevel.ALL
                        )

                        RadioButtonWithLabel(
                            label = stringResource(id = R.string.voice_prompt_level_2),
                            modifier = Modifier
                                .clickable { vm.changeVoicePromptLevel(Volume.VoicePromptVerbosityLevel.IMPORTANT) }
                                .padding(horizontal = 16.dp, vertical = 6.dp),
                            selected = voicePromptLevel == Volume.VoicePromptVerbosityLevel.IMPORTANT
                        )

                    }


                    // Second Phone
                    PreferenceItem(
                        modifier = Modifier,
                        bluArmorAppPreference = BluArmorAppPreference(
                            R.drawable.ic_pref_phone,
                            R.string.second_phone_pairing,
                            R.string.second_phone_pairing_desc,
                        ),
                        position = 3,
                        expandBody = expandedItemId == 3,
                        expandItem = ::expandItem
                    ) {
                        when (secondPhoneState) {
                            SecondPhone.SecondPhoneState.DISCONNECTED -> {
                                PreferenceItemOptionLabel(
                                    null,
                                    stringResource(id = R.string.second_phone_pairing_start_advertising)
                                )

                                ButtonPrimaryLarge(
                                    label = "Connect Second Phone",
                                    onClick = { vm.startSecondPhoneAdvertising() },
                                    modifier = Modifier.padding(horizontal = 24.dp, vertical = 12.dp)
                                )
                            }
                            SecondPhone.SecondPhoneState.SERVICE_DISCOVERY -> {
                                PreferenceItemOptionLabel(
                                    null,
                                    stringResource(id = R.string.second_phone_pairing_start_advertising)
                                )

                                PreferenceItemOptionLabel(title = stringResource(id = R.string.second_phone_sdp_mode_desc))
                            }
                            SecondPhone.SecondPhoneState.ADVERTISING -> {
                                PreferenceItemOptionLabel(
                                    stringResource(id = R.string.second_phone_advertising_mode),
                                    stringResource(id = R.string.second_phone_advertising_mode_desc, deviceInfo?.deviceName ?: "")
                                )

                                ButtonPrimaryLarge(
                                    label = "Dismiss Second Phone Pairing",
                                    onClick = { vm.stopSecondPhoneAdvertising() },
                                    modifier = Modifier.padding(horizontal = 24.dp, vertical = 12.dp)
                                )
                            }
                            SecondPhone.SecondPhoneState.CONNECTED -> {

                                PreferenceItemOptionLabel(
                                    stringResource(id = R.string.second_phone_is_connected),
                                    stringResource(id = R.string.second_phone_disconnect_desc)
                                )

                                ButtonPrimaryLarge(
                                    label = "Disconnect Second Phone",
                                    onClick = { vm.disconnectSecondPhone() },
                                    modifier = Modifier.padding(horizontal = 24.dp, vertical = 12.dp)
                                )
                            }
                            null -> {
                                PreferenceItemOptionLabel(
                                    null,
                                    stringResource(id = R.string.second_phone_pairing_start_advertising)
                                )

                                PreferenceItemOptionLabel(title = stringResource(id = R.string.second_phone_pairing_unavailable))
                            }
                        }
                    }

                    // Privacy policy
                    PreferenceItem(
                        modifier = Modifier,
                        bluArmorAppPreference = BluArmorAppPreference(
                            R.drawable.ic_pref_privacy,
                            R.string.privacy_policy,
                            R.string.privacy_policy_desc,
                        ),
                        position = 4,
                        expandBody = expandedItemId == 4,
                        expandItem = ::expandItem
                    ) {
                        SwitchWithLabel(
                            stringResource(id = R.string.share_app_usage_data),
                            stringResource(id = R.string.share_app_usage_data_desc),
                            false,
                            { }
                        )
                    }

                    // RIDEGRID Test Controls
                    /*              PreferenceItem(
                                      modifier = Modifier,
                                      bluArmorAppPreference = BluArmorAppPreference(
                                          R.drawable.ic_pref_audio,
                                          R.string.ridegrid_test_ctrl,
                                          R.string.ridegrid_test_ctrl,
                                      ),
                                      position = 6,
                                      expandBody = expandedItemId == 6,
                                      expandItem = ::expandItem
                                  ) {

                                      SwitchWithLabel(
                                          "Wind noise bypass manual",
                                          null,
                                          ridegridTestState.windNoise
                                      ) {
                                          vm.rideGridTestStateChange(
                                              ridegridTestState.copy(
                                                  windNoise = it
                                              )
                                          )
                                      }

                                      SwitchWithLabel(
                                          "Wind noise based dynamic bypass",
                                          null,
                                          ridegridTestState.dynamicBypass
                                      ) {
                                          vm.rideGridTestStateChange(
                                              ridegridTestState.copy(
                                                  dynamicBypass = it
                                              )
                                          )
                                      }

              //                        PreferenceItemOptionLabel(title = "Connection Interval : ${nearTo5(ridegridTestState.connectionInterval)}")
              //
              //                        Slider(
              //                            value = ridegridTestState.connectionInterval,
              //                            onValueChange = {
              //                        vm.rideGridTestStateChange(
              //                                    ridegridTestState.copy(
              //                                        connectionInterval = it
              //                                    )
              //                                )
              //                            },
              //                            modifier = Modifier
              //                                .padding(vertical = 6.dp, horizontal = 24.dp),
              //                            valueRange = 15f..30f,
              //                            steps = 2
              //                        )
              //
              //                        PreferenceItemOptionLabel(
              //                            title = "Event Length: ${
              //                                nearToDecimal25(ridegridTestState.eventLength)
              //                            }"
              //                        )
              //
              //                        Slider(
              //                            value = ridegridTestState.eventLength,
              //                            onValueChange = {
              //                                vm.rideGridTestStateChange(
              //                                    ridegridTestState.copy(
              //                                        eventLength = it
              //                                    )
              //                                )
              //                            },
              //                            modifier = Modifier
              //                                .padding(vertical = 6.dp, horizontal = 24.dp),
              //                            valueRange = 3.75f..7.5f,
              //                            steps = 2
              //                        )
              //
              //
              //                        PreferenceItemOptionLabel(title = "MTU: ${nearTo5(ridegridTestState.mtu)}")
              //
              //                        Slider(
              //                            value = ridegridTestState.mtu,
              //                            onValueChange = {
              //                                vm.rideGridTestStateChange(
              //                                    ridegridTestState.copy(
              //                                        mtu = it
              //                                    )
              //                                )
              //                            },
              //                            modifier = Modifier
              //                                .padding(vertical = 6.dp, horizontal = 24.dp),
              //                            valueRange = 120f..200f,
              //                            steps = 15
              //                        )
              //
              //
              //                        PreferenceItemOptionLabel(title = "RSSI Configuration: ${nearTo5(ridegridTestState.rssi)}")
              //
              //                        Slider(
              //                            value = ridegridTestState.rssi,
              //                            onValueChange = {
              //                                vm.rideGridTestStateChange(
              //                                    ridegridTestState.copy(
              //                                        rssi = it
              //                                    )
              //                                )
              //                            },
              //                            modifier = Modifier
              //                                .padding(vertical = 6.dp, horizontal = 24.dp),
              //                            valueRange = -90f..-50f,
              //                            steps = 7
              //                        )


                                      PreferenceItemOptionLabel(title = "Raw: ${ridegridTestState.rawBytes}")

                                  }*/

                    Spacer(modifier = Modifier.height(12.dp))

                    if (!appVersion.isNullOrBlank())
                        Text(
                            text = "App Version: $appVersion",
                            style = MaterialTheme.typography.overline,
                            color = Color.White,
                            fontWeight = FontWeight.SemiBold,
                            modifier = Modifier.padding(horizontal = 16.dp, vertical = 0.dp)
                        )

                    if (!deviceInfoMetadata?.firmwareApplicationVersion.isNullOrBlank())
                        Text(
                            text = "Firmware Version: ${deviceInfoMetadata?.firmwareApplicationVersion} | ${deviceInfoMetadata?.firmwareOtaVersion}",
                            style = MaterialTheme.typography.overline,
                            color = Color.White,
                            fontWeight = FontWeight.SemiBold,
                            modifier = Modifier.padding(horizontal = 16.dp, vertical = 0.dp)
                        )

                    if (!deviceInfoMetadata?.rideGridApplicationVersion.isNullOrBlank())
                        Text(
                            text = "RIDEGRID Version: ${deviceInfoMetadata?.rideGridApplicationVersion} | ${deviceInfoMetadata?.rideGridOtaVersion}",
                            style = MaterialTheme.typography.overline,
                            color = Color.White,
                            fontWeight = FontWeight.SemiBold,
                            modifier = Modifier.padding(horizontal = 16.dp, vertical = 0.dp)
                        )


                    if (!deviceInfoMetadata?.srNo.isNullOrBlank())
                        Text(
                            text = "Sr. No: ${deviceInfoMetadata?.srNo}",
                            style = MaterialTheme.typography.overline,
                            color = Color.White,
                            fontWeight = FontWeight.SemiBold,
                            modifier = Modifier.padding(horizontal = 16.dp, vertical = 0.dp)
                        )
                    Spacer(modifier = Modifier.height(16.dp))
                }
            }
        }
    }

    DisposableEffect(lifecycleOwner) {
        // Create an observer that triggers our remembered callbacks
        // for sending analytics events
        val observer = LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_START) {
                vm.refreshPreferences(context)
            }
        }

        // Add the observer to the lifecycle
        lifecycleOwner.lifecycle.addObserver(observer)

        // When the effect leaves the Composition, remove the observer


        onDispose {
            lifecycleOwner.lifecycle.removeObserver(observer)
        }
    }
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun PreferenceItem(
    modifier: Modifier = Modifier,
    bluArmorAppPreference: BluArmorAppPreference,
    position: Int,
    expandBody: Boolean,
    expandItem: (position: Int) -> Unit,
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = modifier
            .fillMaxWidth()
            .wrapContentHeight()

    ) {
        PreferenceHeaderCard(
            modifier = Modifier
                .clickable { expandItem(position) }
                .padding(16.dp),
            bluArmorAppPreference = bluArmorAppPreference,
            expand = expandBody
        )
        PreferenceBodyCard(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight(),
            expand = expandBody,
            content = content
        )
    }
}


@Composable
fun PreferenceHeaderCard(
    modifier: Modifier = Modifier,
    bluArmorAppPreference: BluArmorAppPreference,
    expand: Boolean,
) {
    val angle: Float by animateFloatAsState(
        targetValue = if (expand) 90f else 270f,
        animationSpec = tween(
            durationMillis = 120,
            easing = LinearEasing
        )
    )

    Row(
        modifier = modifier
            .fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Image(
            imageVector = ImageVector.vectorResource(id = bluArmorAppPreference.icon),
            contentDescription = "Preference Icon",
            modifier = Modifier
                .height(48.dp)
                .aspectRatio(1f)
        )

        Spacer(modifier = Modifier.width(12.dp))

        Column(
            Modifier.weight(1f)
        ) {
            Text(
                text = stringResource(id = bluArmorAppPreference.title),
                style = MaterialTheme.typography.subtitle1,
                color = Color.White,
                fontWeight = FontWeight.SemiBold
            )

            Text(
                text = stringResource(id = bluArmorAppPreference.description),
                style = MaterialTheme.typography.subtitle2,
                color = Color.White,
                fontWeight = FontWeight.Normal
            )
        }

        Spacer(modifier = Modifier.width(12.dp))

        Icon(
            imageVector = ImageVector.vectorResource(id = R.drawable.ic_left_arrow),
            contentDescription = null,
            modifier = Modifier
                .height(12.dp)
                .aspectRatio(1f)
                .rotate(angle),
            tint = Color.White
        )
    }
}

@Composable
fun PreferenceBodyCard(
    modifier: Modifier = Modifier,
    expand: Boolean,
    content: @Composable ColumnScope.() -> Unit,
) {
    val animationTime = 200
    AnimatedVisibility(
        visible = expand,
        enter = expandVertically(
            expandFrom = Alignment.Top,
            animationSpec = tween(durationMillis = animationTime)
        ) + fadeIn(
            initialAlpha = 0.0f,
            animationSpec = tween(durationMillis = animationTime)
        ),

        exit = shrinkVertically(
            animationSpec = tween(durationMillis = animationTime)
        ) + fadeOut(
            animationSpec = tween(durationMillis = animationTime)
        ),
        modifier = modifier,
        content = { Column(content = content, modifier = Modifier.background(color = Color(0x11FFFFFF))) },
    )
}

@Composable
fun SwitchWithLabel(
    title: String? = null,
    description: String? = null,
    checked: Boolean,
    onCheckedChange: ((Boolean) -> Unit),
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .padding(vertical = 12.dp)
            .padding(start = 24.dp, end = 12.dp)
            .wrapContentHeight()
            .fillMaxWidth()
//            .clickable {
//                onCheckedChange(!checked)
//            }
    ) {

        Column(
            Modifier
                .weight(1f)
                .wrapContentHeight()
        ) {

            if (title != null)

                Text(
                    text = title,
                    style = MaterialTheme.typography.subtitle1,
                    color = ColorTextPrimary,
                    fontWeight = FontWeight.SemiBold
                )

            if (description != null)
                Text(
                    text = description,
                    style = MaterialTheme.typography.subtitle2,
                    color = ColorTextPrimaryAccent,
                    fontWeight = FontWeight.Normal
                )

        }

        Spacer(modifier = Modifier.width(12.dp))

        Switch(checked = checked, onCheckedChange = onCheckedChange)
    }
}


@Composable
fun PreferenceItemOptionLabel(
    title: String? = null,
    description: String? = null,
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .padding(vertical = 6.dp)
            .padding(start = 24.dp, end = 12.dp)
            .wrapContentHeight()
            .fillMaxWidth()
    ) {

        Column(
            Modifier
                .weight(1f)
                .wrapContentHeight()
        ) {

            if (title != null)

                Text(
                    text = title,
                    style = MaterialTheme.typography.subtitle1,
                    color = ColorTextPrimary,
                    fontWeight = FontWeight.SemiBold
                )

            if (description != null)
                Text(
                    text = description,
                    style = MaterialTheme.typography.subtitle2,
                    color = ColorTextPrimaryAccent,
                    fontWeight = FontWeight.Normal
                )

        }
    }
}

@Composable
fun RadioButtonWithLabel(
    label: String,
    selected: Boolean,
    modifier: Modifier = Modifier
) {
    Row(verticalAlignment = Alignment.CenterVertically, modifier = modifier.fillMaxWidth()) {
        Icon(
            imageVector = ImageVector.vectorResource(id = R.drawable.ic_back_nav),
            contentDescription = null,
            modifier = Modifier
                .width(20.dp)
                .aspectRatio(1f),
            tint = if (selected)
                Color.White
            else
                Color(0x1FFFFFFF)

        )

        Spacer(modifier = Modifier.width(8.dp))

        Text(
            text = label,
            style = MaterialTheme.typography.body2,
            color = if (selected)
                ColorTextPrimary
            else ColorTextPrimaryAccent,
            fontWeight = FontWeight.Medium,
            modifier = Modifier
        )
    }
}


@Preview
@Composable
fun Preview() {
    PreferenceView(navController = rememberNavController(), vm = object : PreferenceViewModelInterface {

        override val discoveryState: LiveData<DiscoveryState>
            get() = MutableLiveData()
        override val deviceInfo: LiveData<DeviceInfo>
            get() = MutableLiveData()
        override val batteryLevel: LiveData<Int>
            get() = MutableLiveData()
        override val audioOverlayLevel: LiveData<Float>
            get() = MutableLiveData()
        override val audioOverlayState: LiveData<RideGrid.AudioOverlayState>
            get() = TODO("Not yet implemented")

        override val secondPhoneState: LiveData<SecondPhone.SecondPhoneState>
            get() = MutableLiveData()
        override val appVersion: LiveData<String?>
            get() = MutableLiveData()
        override val deviceDetails: LiveData<com.aptener.bluconnect.device.sxcontrol.DeviceInfo.DeviceDetails>
            get() = MutableLiveData()
        override val whatsAppAutoRead: StateFlow<Boolean>
            get() = MutableStateFlow(false)
        override val whatsAppAutoRespond: StateFlow<Boolean>
            get() = MutableStateFlow(false)
        override val whatsAppAutoLocationShare: StateFlow<Boolean>
            get() = MutableStateFlow(false)
        override val whatsAppAutoLocationShareContact: StateFlow<String?>
            get() = MutableStateFlow(null)
        override val whatsAppDefaultMessage: StateFlow<String>
            get() = MutableStateFlow("")

        override fun updateWhatsAppDefaultMessage(message: String) {

        }

        override fun toggleWhatsAppAutoRead(context: Context, enable: Boolean) {
            TODO("Not yet implemented")
        }

        override fun toggleWhatsAppAutoRespond(context: Context, enable: Boolean) {
            TODO("Not yet implemented")
        }

        override fun toggleWhatsAppAutoLocationShare(enable: Boolean) {

        }

        override val phoneAutoAnswer: StateFlow<PhoneCallAutoAnswerRadioOption>
            get() = MutableStateFlow(PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_ALL)
        override val phoneAutoReject: StateFlow<PhoneCallAutoRejectRadioOption>
            get() = MutableStateFlow(PhoneCallAutoRejectRadioOption.AUTO_REJECT_ALL)

        override fun phoneAutoAnswerChange(context: Context, phoneCallAutoAnswerRadioOption: PhoneCallAutoAnswerRadioOption) {
            TODO("Not yet implemented")
        }

        override fun phoneAutoRejectChange(context: Context, phoneCallAutoRejectRadioOption: PhoneCallAutoRejectRadioOption) {
            TODO("Not yet implemented")
        }

        override val volumeBoost: StateFlow<VolumeBoostRadioOption>
            get() = MutableStateFlow(VolumeBoostRadioOption.BALANCED)
        override val equalizerBoost: StateFlow<EqualizerBoostRadioOption>
            get() = MutableStateFlow(EqualizerBoostRadioOption.BASS_BOOST)

        override val voicePromptLevel: LiveData<Volume.VoicePromptVerbosityLevel>
            get() = TODO("Not yet implemented")

        override fun volumeBoostChange(volumeBoostRadioOption: VolumeBoostRadioOption) {

        }

        override fun equalizerBoostChange(equalizerBoostRadioOption: EqualizerBoostRadioOption) {

        }

        override fun audioOverlayChange(enable: Boolean) {

        }

        override fun audioOverlayLevelChange(value: Float) {

        }

        override fun changeVoicePromptLevel(voicePromptLevel: Volume.VoicePromptVerbosityLevel) {
            TODO("Not yet implemented")
        }

        override val renameSwitch: StateFlow<Boolean>
            get() = MutableStateFlow(false)
        override val deviceName: StateFlow<String>
            get() = MutableStateFlow("")

        override fun toggleRenameSwitch(enable: Boolean) {

        }

        override fun deviceNameChange(name: String) {

        }

        override fun confirmDeviceNameChange() {

        }

        override fun cancelDeviceNameChange() {

        }

        override fun startSecondPhoneAdvertising() {

        }

        override fun stopSecondPhoneAdvertising() {

        }

        override fun disconnectSecondPhone() {

        }

        override fun refreshPreferences(context: Context) {
            TODO("Not yet implemented")
        }

    })
}

fun roundFloat(d: Float) = (d * 2).roundToInt() / 2.0;
fun roundFloat(d: Double) = (d * 2).roundToInt() / 2.0;
fun nearTo5(f: Float): Int = 5 * ((f / 5).roundToInt())
fun nearTo5(i: Int): Int = 5 * ((i.toFloat() / 5).roundToInt())
fun nearToDecimal25(f: Float): Float = 1.25f * ((f / 1.25f).roundToInt())
