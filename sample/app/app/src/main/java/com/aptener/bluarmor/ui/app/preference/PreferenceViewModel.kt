package com.aptener.bluarmor.ui.app.preference

import android.Manifest
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.provider.Settings
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.annotation.FloatRange
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.lifecycle.*
import com.aptener.bluarmor.BuildConfig
import com.aptener.bluarmor.baseutils.asMutableLiveData
import com.aptener.bluarmor.service.bluconnect.model.BluArmorInterface
import com.aptener.bluarmor.service.bluconnect.model.DeviceInfo
import com.aptener.bluarmor.service.bluconnect.model.DiscoveryState
import com.aptener.bluarmor.service.bluconnect.model.VolumeControlAction
import com.aptener.bluarmor.ui.app.preference.PreferenceViewModelInterface.*
import com.aptener.bluarmor.ui.util.asMutableStateFlow
import com.aptener.bluconnect.device.sxcontrol.DeviceInfo.DeviceDetails
import com.aptener.bluconnect.device.sxcontrol.RideGrid
import com.aptener.bluconnect.device.sxcontrol.SecondPhone
import com.aptener.bluconnect.device.sxcontrol.Volume
import com.aptener.bluconnect.repo.EqualizerPreferenceOption
import com.aptener.bluconnect.repo.PhoneCallPreferenceOption
import com.aptener.bluconnect.repo.VolumeBoostPreferenceOption
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import timber.log.Timber


@AssistedFactory
interface PreferenceViewModelAssistedFactory {
    fun create(bluArmorInterface: BluArmorInterface): PreferenceViewModel
}

interface PreferenceViewModelInterface {

    val discoveryState: LiveData<DiscoveryState>
    val deviceInfo: LiveData<DeviceInfo>
    val batteryLevel: LiveData<Int>

    val secondPhoneState: LiveData<SecondPhone.SecondPhoneState>

    val appVersion: LiveData<String?>
    val deviceDetails: LiveData<DeviceDetails>

    val whatsAppAutoRead: StateFlow<Boolean>
    val whatsAppAutoRespond: StateFlow<Boolean>
    val whatsAppAutoLocationShare: StateFlow<Boolean>
    val whatsAppAutoLocationShareContact: StateFlow<String?>
    val whatsAppDefaultMessage: StateFlow<String>
    fun updateWhatsAppDefaultMessage(message: String)
    fun toggleWhatsAppAutoRead(context: Context, enable: Boolean)
    fun toggleWhatsAppAutoRespond(context: Context, enable: Boolean)
    fun toggleWhatsAppAutoLocationShare(enable: Boolean)

    val phoneAutoAnswer: StateFlow<PhoneCallAutoAnswerRadioOption>
    val phoneAutoReject: StateFlow<PhoneCallAutoRejectRadioOption>
    fun phoneAutoAnswerChange(context: Context, phoneCallAutoAnswerRadioOption: PhoneCallAutoAnswerRadioOption)
    fun phoneAutoRejectChange(context: Context, phoneCallAutoRejectRadioOption: PhoneCallAutoRejectRadioOption)

    val volumeBoost: StateFlow<VolumeBoostRadioOption>
    val equalizerBoost: StateFlow<EqualizerBoostRadioOption>
    val voicePromptLevel: LiveData<Volume.VoicePromptVerbosityLevel>

    val audioOverlayLevel: LiveData<Float>
    val audioOverlayState: LiveData<RideGrid.AudioOverlayState>


    fun volumeBoostChange(volumeBoostRadioOption: VolumeBoostRadioOption)
    fun equalizerBoostChange(equalizerBoostRadioOption: EqualizerBoostRadioOption)
    fun audioOverlayChange(enable: Boolean)
    fun audioOverlayLevelChange(@FloatRange(from = 0.0, to = 7.0) value: Float)

    fun changeVoicePromptLevel(voicePromptLevel: Volume.VoicePromptVerbosityLevel)

    val renameSwitch: StateFlow<Boolean>
    val deviceName: StateFlow<String>
    fun toggleRenameSwitch(enable: Boolean)
    fun deviceNameChange(name: String)
    fun confirmDeviceNameChange()
    fun cancelDeviceNameChange()

    fun startSecondPhoneAdvertising()
    fun stopSecondPhoneAdvertising()
    fun disconnectSecondPhone()

    fun refreshPreferences(context: Context)

    enum class PhoneCallAutoAnswerRadioOption {
        AUTO_ANSWER_ALL,
        AUTO_ANSWER_KNOWN,
        AUTO_ANSWER_MANUALLY
    }

    enum class PhoneCallAutoRejectRadioOption {
        AUTO_REJECT_ALL,
        AUTO_REJECT_UNKNOWN,
        AUTO_REJECT_MANUALLY,
    }

    enum class VolumeBoostRadioOption {
        EASE_ON_EARS,
        BALANCED,
        HIGH_OUTPUT,
    }

    enum class EqualizerBoostRadioOption {
        REGULAR,
        BASS_BOOST,
    }
}

class PreferenceViewModel @AssistedInject constructor(@Assisted private val bluArmorInterface: BluArmorInterface) : ViewModel(),
    PreferenceViewModelInterface {
    override val discoveryState: LiveData<DiscoveryState> = bluArmorInterface.bluConnectManager.discoveryState
    override val deviceInfo: LiveData<DeviceInfo> = bluArmorInterface.bluConnectManager.deviceInfo
    override val batteryLevel: LiveData<Int> = bluArmorInterface.bluConnectManager.batteryLevel

    override val audioOverlayLevel: LiveData<Float> = (bluArmorInterface.bluConnectManager.musicOverlayLevel).map { it.toFloat() }
    override val audioOverlayState: LiveData<RideGrid.AudioOverlayState> = bluArmorInterface.bluConnectManager.audioOverlayState

    override val secondPhoneState: LiveData<SecondPhone.SecondPhoneState> = bluArmorInterface.bluConnectManager.secondPhoneState

    override val appVersion: LiveData<String?> = MutableLiveData(BuildConfig.VERSION_NAME)
    override val deviceDetails: LiveData<DeviceDetails> = bluArmorInterface.bluConnectManager.deviceDetails

    // Whatsapp
    override val whatsAppAutoRead: StateFlow<Boolean> = MutableStateFlow(false)
    override val whatsAppAutoRespond: StateFlow<Boolean> = MutableStateFlow(false)
    override val whatsAppAutoLocationShare: StateFlow<Boolean> = MutableStateFlow(false)
    override val whatsAppAutoLocationShareContact: StateFlow<String?> = MutableStateFlow(null)
    override val whatsAppDefaultMessage: StateFlow<String> = MutableStateFlow("")

    override fun updateWhatsAppDefaultMessage(message: String) {
        whatsAppDefaultMessage.asMutableStateFlow()?.tryEmit(message)
        bluArmorInterface.bluConnectManager.whatsappDefaultMsg = message
    }

    override fun toggleWhatsAppAutoRead(context: Context, enable: Boolean) {
        if (!checkNotificationPermission(context)) {
            Toast.makeText(context, "Notification access permission needed!", Toast.LENGTH_SHORT).show()
            openNotificationAccessActivity(context)
            return
        }
        whatsAppAutoRead.asMutableStateFlow()?.tryEmit(enable)
        bluArmorInterface.bluConnectManager.whatsappAutoRead = enable
    }

    override fun toggleWhatsAppAutoRespond(context: Context, enable: Boolean) {
        if (!checkNotificationPermission(context)) {
            Toast.makeText(context, "Notification access permission needed!", Toast.LENGTH_SHORT).show()
            openNotificationAccessActivity(context)
            return
        }
        whatsAppAutoRespond.asMutableStateFlow()?.tryEmit(enable)
        bluArmorInterface.bluConnectManager.whatsappAutoReply = enable
    }

    override fun toggleWhatsAppAutoLocationShare(enable: Boolean) {
        whatsAppAutoLocationShare.asMutableStateFlow()?.tryEmit(enable)
    }
    //override fun toggleWhatsAppAutoLocationShareContact(enable: Boolean) = whatsAppAutoLocationShareContact.asMutableStateFlow()?.tryEmit(enable)

    //Phone call
    override val phoneAutoAnswer: StateFlow<PhoneCallAutoAnswerRadioOption> =
        MutableStateFlow(PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_MANUALLY)
    override val phoneAutoReject: StateFlow<PhoneCallAutoRejectRadioOption> =
        MutableStateFlow(PhoneCallAutoRejectRadioOption.AUTO_REJECT_MANUALLY)

    override fun phoneAutoAnswerChange(context: Context, phoneCallAutoAnswerRadioOption: PhoneCallAutoAnswerRadioOption) {

        when (phoneCallAutoAnswerRadioOption) {
            PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_ALL ->
                if (phoneAutoReject.value != PhoneCallAutoRejectRadioOption.AUTO_REJECT_MANUALLY)
                    phoneAutoReject.asMutableStateFlow()?.value = (PhoneCallAutoRejectRadioOption.AUTO_REJECT_MANUALLY)
            PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_KNOWN -> {

                if (!checkPhonePermission(context)) {
                    Toast.makeText(context, "Contact permission needed!", Toast.LENGTH_SHORT).show()
                    return
                }

                if (phoneAutoReject.value == PhoneCallAutoRejectRadioOption.AUTO_REJECT_ALL)
                    phoneAutoReject.asMutableStateFlow()?.value = (PhoneCallAutoRejectRadioOption.AUTO_REJECT_MANUALLY)
            }

            PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_MANUALLY -> Unit
        }

        phoneAutoAnswer.asMutableStateFlow()?.value = phoneCallAutoAnswerRadioOption
        persistPhoneOptions()
    }

    override fun phoneAutoRejectChange(context: Context, phoneCallAutoRejectRadioOption: PhoneCallAutoRejectRadioOption) {

        when (phoneCallAutoRejectRadioOption) {
            PhoneCallAutoRejectRadioOption.AUTO_REJECT_ALL ->
                if (phoneAutoAnswer.value != PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_MANUALLY)
                    phoneAutoAnswer.asMutableStateFlow()?.value = (PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_MANUALLY)
            PhoneCallAutoRejectRadioOption.AUTO_REJECT_UNKNOWN -> {
                if (!checkPhonePermission(context)) {
                    Toast.makeText(context, "Contact permission needed!", Toast.LENGTH_SHORT).show()
                    return
                }
                if (phoneAutoAnswer.value == PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_ALL)
                    phoneAutoAnswer.asMutableStateFlow()?.value = (PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_MANUALLY)
            }
            PhoneCallAutoRejectRadioOption.AUTO_REJECT_MANUALLY -> Unit
        }

        phoneAutoReject.asMutableStateFlow()?.value = phoneCallAutoRejectRadioOption
        persistPhoneOptions()
    }


    // Audio
    override val volumeBoost: StateFlow<VolumeBoostRadioOption> = MutableStateFlow(VolumeBoostRadioOption.EASE_ON_EARS)
    override val equalizerBoost: StateFlow<EqualizerBoostRadioOption> = MutableStateFlow(EqualizerBoostRadioOption.REGULAR)
    override val voicePromptLevel: LiveData<Volume.VoicePromptVerbosityLevel> = bluArmorInterface.bluConnectManager.voicePromptLevel

    override fun changeVoicePromptLevel(voicePromptLevel: Volume.VoicePromptVerbosityLevel) {
        bluArmorInterface.bluConnectManager.changeVoicePromptVerbosity(voicePromptLevel)
    }

    override fun audioOverlayChange(enable: Boolean) {
        bluArmorInterface.bluConnectManager.changeAudioOverlayFlag(enable)
    }

    override fun volumeBoostChange(volumeBoostRadioOption: VolumeBoostRadioOption) {
        volumeBoost.asMutableStateFlow()?.tryEmit(volumeBoostRadioOption)
        bluArmorInterface.bluConnectManager.volumeAction(
            VolumeControlAction.ChangeVolumeBoost(
                when (volumeBoostRadioOption) {
                    VolumeBoostRadioOption.EASE_ON_EARS -> Volume.VolumeBoost.EASY_ON_EARS
                    VolumeBoostRadioOption.BALANCED -> Volume.VolumeBoost.BALANCED
                    VolumeBoostRadioOption.HIGH_OUTPUT -> Volume.VolumeBoost.HIGH_OUTPUT
                }
            )
        )
    }

    override fun equalizerBoostChange(equalizerBoostRadioOption: EqualizerBoostRadioOption) {
        equalizerBoost.asMutableStateFlow()?.tryEmit(equalizerBoostRadioOption)
        bluArmorInterface.bluConnectManager.volumeAction(
            VolumeControlAction.ChangeEqualizerBoost(
                when (equalizerBoostRadioOption) {
                    EqualizerBoostRadioOption.REGULAR -> Volume.EqualizerBassBoost.REGULAR
                    EqualizerBoostRadioOption.BASS_BOOST -> Volume.EqualizerBassBoost.BASS_BOOST
                }
            )
        )
    }

    override fun audioOverlayLevelChange(value: Float) {
        Timber.e("audioFocusControlChange: $value")
        bluArmorInterface.bluConnectManager.changeMusicLevelFocus(value)
//        audioOverlayLevel.asMutableLiveData()?.value = value
    }

    // Rename
    override val renameSwitch: StateFlow<Boolean> = MutableStateFlow(false)
    override val deviceName: StateFlow<String> = MutableStateFlow("")

    override fun toggleRenameSwitch(enable: Boolean) {
        renameSwitch.asMutableStateFlow()?.tryEmit(enable)
    }

    override fun deviceNameChange(name: String) {
        deviceName.asMutableStateFlow()?.tryEmit(
            if (name.length > 10)
                name.substring(0, 10)
            else
                name
        )
    }

    override fun confirmDeviceNameChange() {
        if (deviceName.value.isNotEmpty()) {
            cancelDeviceNameChange()
            bluArmorInterface.bluConnectManager.device?.rename(deviceName.value)
        }
    }

    override fun cancelDeviceNameChange() {
        renameSwitch.asMutableStateFlow()?.tryEmit(false)
    }

    override fun startSecondPhoneAdvertising() {
        bluArmorInterface.bluConnectManager.device?.secondPhone?.startAdvertising()
    }

    override fun stopSecondPhoneAdvertising() {
        bluArmorInterface.bluConnectManager.device?.secondPhone?.stopAdvertising()
    }

    override fun disconnectSecondPhone() {
        bluArmorInterface.bluConnectManager.device?.secondPhone?.disconnect()
    }

    override fun refreshPreferences(context: Context) {
        when (bluArmorInterface.bluConnectManager.phoneCallPreferenceOption) {
            PhoneCallPreferenceOption.MANUAL -> {
                this.phoneAutoAnswer.asMutableStateFlow()?.value = PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_MANUALLY
                this.phoneAutoReject.asMutableStateFlow()?.value = PhoneCallAutoRejectRadioOption.AUTO_REJECT_MANUALLY
            }
            PhoneCallPreferenceOption.AUTO_ANSWER_ALL -> {
                this.phoneAutoAnswer.asMutableStateFlow()?.value = PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_ALL

                if (phoneAutoReject.value != PhoneCallAutoRejectRadioOption.AUTO_REJECT_MANUALLY)
                    phoneAutoReject.asMutableStateFlow()?.tryEmit(PhoneCallAutoRejectRadioOption.AUTO_REJECT_MANUALLY)
            }
            PhoneCallPreferenceOption.AUTO_REJECT_ALL -> {
                this.phoneAutoReject.asMutableStateFlow()?.value = PhoneCallAutoRejectRadioOption.AUTO_REJECT_ALL

                if (phoneAutoAnswer.value != PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_MANUALLY)
                    phoneAutoAnswer.asMutableStateFlow()?.tryEmit(PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_MANUALLY)
            }
            PhoneCallPreferenceOption.AUTO_ANSWER_KNOWN -> {
                this.phoneAutoAnswer.asMutableStateFlow()?.value = PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_KNOWN

                if (phoneAutoReject.value == PhoneCallAutoRejectRadioOption.AUTO_REJECT_ALL)
                    phoneAutoReject.asMutableStateFlow()?.tryEmit(PhoneCallAutoRejectRadioOption.AUTO_REJECT_MANUALLY)

            }
            PhoneCallPreferenceOption.AUTO_REJECT_UNKNOWN -> {
                this.phoneAutoReject.asMutableStateFlow()?.value = PhoneCallAutoRejectRadioOption.AUTO_REJECT_UNKNOWN

                if (phoneAutoAnswer.value == PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_ALL)
                    phoneAutoAnswer.asMutableStateFlow()?.tryEmit(PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_MANUALLY)
            }
            PhoneCallPreferenceOption.AUTO_ANSWER_KNOWN_AND_AUTO_REJECT_UNKNOWN -> {
                this.phoneAutoAnswer.asMutableStateFlow()?.value = PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_KNOWN
                this.phoneAutoReject.asMutableStateFlow()?.value = PhoneCallAutoRejectRadioOption.AUTO_REJECT_UNKNOWN
            }
        }

        this.volumeBoost.asMutableStateFlow()?.value = when (bluArmorInterface.bluConnectManager.volumeBoostPreferenceOption) {
            VolumeBoostPreferenceOption.EASY_ON_EAR -> VolumeBoostRadioOption.EASE_ON_EARS
            VolumeBoostPreferenceOption.BALANCED -> VolumeBoostRadioOption.BALANCED
            VolumeBoostPreferenceOption.HIGH_OUTPUT -> VolumeBoostRadioOption.HIGH_OUTPUT
        }

        this.equalizerBoost.asMutableStateFlow()?.value = when (bluArmorInterface.bluConnectManager.equalizerPreferenceOption) {
            EqualizerPreferenceOption.REGULAR -> EqualizerBoostRadioOption.REGULAR
            EqualizerPreferenceOption.BASS_BOOST -> EqualizerBoostRadioOption.BASS_BOOST
        }

        this.whatsAppDefaultMessage.asMutableStateFlow()?.value = bluArmorInterface.bluConnectManager.whatsappDefaultMsg

        if (checkNotificationPermission(context)) {
            this.whatsAppAutoRead.asMutableStateFlow()?.value = bluArmorInterface.bluConnectManager.whatsappAutoRead
            this.whatsAppAutoRespond.asMutableStateFlow()?.value = bluArmorInterface.bluConnectManager.whatsappAutoReply
        }
    }

    private fun checkPhonePermission(context: Context) = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED

    private fun checkNotificationPermission(context: Context): Boolean {
        val contentResolver: ContentResolver = context.contentResolver
        val enabledNotificationListeners: String = Settings.Secure.getString(contentResolver, "enabled_notification_listeners")
        val packageName: String = context.packageName
        return enabledNotificationListeners.contains(packageName)
    }

    private fun openNotificationAccessActivity(context: Context) {
        context.startActivity(Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS));
    }

    private fun persistPhoneOptions() {

        bluArmorInterface.bluConnectManager.phoneCallPreferenceOption = when {
            phoneAutoAnswer.value == PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_ALL ->
                PhoneCallPreferenceOption.AUTO_ANSWER_ALL

            phoneAutoReject.value == PhoneCallAutoRejectRadioOption.AUTO_REJECT_ALL ->
                PhoneCallPreferenceOption.AUTO_REJECT_ALL

            phoneAutoAnswer.value == PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_KNOWN &&
                    phoneAutoReject.value == PhoneCallAutoRejectRadioOption.AUTO_REJECT_UNKNOWN ->
                PhoneCallPreferenceOption.AUTO_ANSWER_KNOWN_AND_AUTO_REJECT_UNKNOWN

            /*          phoneAutoAnswer.value == PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_MANUALLY &&
                              phoneAutoReject.value == PhoneCallAutoRejectRadioOption.AUTO_REJECT_MANUALLY ->
                          PhoneCallPreferenceOption.MANUAL*/


            phoneAutoAnswer.value == PhoneCallAutoAnswerRadioOption.AUTO_ANSWER_KNOWN ->
                PhoneCallPreferenceOption.AUTO_ANSWER_KNOWN

            phoneAutoReject.value == PhoneCallAutoRejectRadioOption.AUTO_REJECT_UNKNOWN ->
                PhoneCallPreferenceOption.AUTO_REJECT_UNKNOWN

            else -> PhoneCallPreferenceOption.MANUAL
        }
    }

    class Factory(
        private val assistedFactory: PreferenceViewModelAssistedFactory,
        private val bluArmorInterface: BluArmorInterface,
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return assistedFactory.create(bluArmorInterface) as T
        }
    }

}

data class BluArmorAppPreference(
    @DrawableRes val icon: Int,
    @StringRes val title: Int,
    @StringRes val description: Int,
)