package com.aptener.bluarmor.ui.app.ridegrid

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.content.ContextCompat
import androidx.lifecycle.*
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.aptener.bluarmor.R
import com.aptener.bluarmor.service.bluconnect.model.*
import com.aptener.bluarmor.ui.theme.*
import com.aptener.bluconnect.repo.RideGridGroupInvitation
import com.journeyapps.barcodescanner.CaptureManager
import com.journeyapps.barcodescanner.CompoundBarcodeView
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import timber.log.Timber

interface RideGridCreateGroupViewModelInterface {

    val rideGridScanResultStateFlow: StateFlow<RideGridGroupInvitation?>
    val newGroupName: LiveData<String>
    var newGroupId: Long
    val setPrimaryGroup: LiveData<Boolean>

    fun createRideGridGroup(name: String, isPrimary: Boolean, navController: NavController)
    fun joinRideGridGroup(rideGridGroupInvitation: RideGridGroupInvitation,isPrimary: Boolean, navController: NavController)

    fun onGroupNameUpdate(string: String)

    fun newRideGridGroupFound(rideGridGroupInvitation: RideGridGroupInvitation)
    fun ignoreRecentRideGridGroupInvite()

    fun onPrimaryGroupSwitchToggle(enabled: Boolean)
    fun navigateToRideGridInviteRiders(navController: NavController, groupId: String)
}


@Composable
fun AdminClubMembershipScanScreen(
    navController: NavController,
    newRideGridGroupFound: (rideGridGroupInvitation: RideGridGroupInvitation) -> Unit,
    lifecycleOwner: LifecycleOwner = LocalLifecycleOwner.current,
) {
    val context = LocalContext.current
    var scanFlag by remember {
        mutableStateOf(false)
    }

    val compoundBarcodeView = remember {
        CompoundBarcodeView(context).apply {
            val capture = CaptureManager(context as Activity, this)
            capture.initializeFromIntent(context.intent, null)
            this.setStatusText("")
            capture.decode()
            this.resume()
            this.decodeContinuous { result ->
                if (scanFlag) {
                    return@decodeContinuous
                }
                scanFlag = true
                result.text?.let { qrCodeResult ->
                    val newGroup = RideGridGroupInvitation.decodeFromQrCodeData(qrCodeResult)
//                    Timber.e("QR CODE RESULT: $qrCodeResult, ${newGroup?.json()}")
                    if (newGroup != null) {
                        newRideGridGroupFound(newGroup)
                        scanFlag = true
                        capture.onPause()
                        return@let
                    }
                    //Do something and when you finish this something
                    //put scanFlag = false to scan another item
                    scanFlag = false

                }
                //If you don't put this scanFlag = false, it will never work again.
                //you can put a delay over 2 seconds and then scanFlag = false to prevent multiple scanning

            }
        }
    }

    AndroidView(
        modifier = Modifier,
        factory = { compoundBarcodeView },
    )


    DisposableEffect(lifecycleOwner) {
        // Create an observer that triggers our remembered callbacks
        // for sending analytics events
        val observer = LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_PAUSE) {
                compoundBarcodeView.pause()
            } else if (event == Lifecycle.Event.ON_STOP) {
                compoundBarcodeView.pause()
            }
        }

        // Add the observer to the lifecycle
        lifecycleOwner.lifecycle.addObserver(observer)

        // When the effect leaves the Composition, remove the observer


        onDispose {
            compoundBarcodeView.pause()
            lifecycleOwner.lifecycle.removeObserver(observer)
        }
    }
}

@Composable
fun RideGridCreateGroupView(
    navController: NavController,
    vm: RideGridCreateGroupViewModelInterface
) {

    val rideGridScanResult by vm.rideGridScanResultStateFlow.collectAsState()
    val groupName by vm.newGroupName.observeAsState("")
    val setAsPrimaryGroup by vm.setPrimaryGroup.observeAsState(false)

    BluArmorTheme {
        Scaffold(
            modifier = Modifier.fillMaxSize(),
            topBar = {
                TopAppBar(
                    backgroundColor = MaterialTheme.colors.background,
                    elevation = 0.dp,
                ) {
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.SpaceBetween,
                    ) {
                        Image(
                            imageVector = ImageVector.vectorResource(R.drawable.ic_back_nav),
                            contentDescription = null,
                            modifier = Modifier
                                .padding(0.dp)
                                .clickable {
                                    navController.popBackStack()
                                }
                        )
                    }
                }
            }
        ) {
            Column(
                modifier = Modifier
                    .padding(horizontal = 32.dp)
                    .fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {

                if (rideGridScanResult == null) {
                    Card(
                        modifier = Modifier
                            .fillMaxWidth(0.8f)
                            .aspectRatio(1f)
                            .padding(horizontal = 16.dp)
                            .padding(top = 18.dp)
                            .background(
                                color = Color.White,
                                shape = RoundedCornerShape(8.dp)
                            ),
                        backgroundColor = Color.White,
                        elevation = 0.dp,
                    ) {
                        if (ContextCompat.checkSelfPermission(LocalContext.current, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
                            AdminClubMembershipScanScreen(navController, vm::newRideGridGroupFound)
                        else
                            Text(
                                modifier = Modifier
                                    .wrapContentWidth()
                                    .padding(vertical = 4.dp, horizontal = 16.dp),
                                text = "Camera permission is needed",
                                style = MaterialTheme.typography.subtitle2,
                                color = ColorShark,
                                fontWeight = FontWeight.SemiBold
                            )
                    }

                    Text(
                        modifier = Modifier
                            .wrapContentWidth()
                            .padding(vertical = 4.dp, horizontal = 16.dp),
                        text = "Scan other rider’s QR code to join the group",
                        style = MaterialTheme.typography.caption,
                        color = Color.White,
                        fontWeight = FontWeight.SemiBold
                    )
                } else {
                    Text(
                        modifier = Modifier
                            .wrapContentWidth()
                            .padding(vertical = 4.dp, horizontal = 16.dp),
                        text = "Found new RIDEGRID group",
                        style = MaterialTheme.typography.caption,
                        color = Color.White,
                        fontWeight = FontWeight.SemiBold
                    )

                    EditText(
                        label = "Group Name",
                        text = rideGridScanResult?.ridegridGroup?.name ?: "",
                        onValueChange = vm::onGroupNameUpdate,
                        enabled = false
                    )

                    Row(
                        modifier = Modifier
                            .padding(vertical = 12.dp)
                            .fillMaxWidth()
                    ) {
                        Checkbox(
                            checked = setAsPrimaryGroup, onCheckedChange = vm::onPrimaryGroupSwitchToggle
                        )

                        Text(
                            modifier = Modifier
                                .wrapContentWidth()
                                .padding(vertical = 4.dp, horizontal = 16.dp),
                            text = "Set as primary RIDEGRID group",
                            style = MaterialTheme.typography.body2,
                            color = Color.White,
                            fontWeight = FontWeight.SemiBold
                        )
                    }

                    Row(
                        modifier = Modifier
                            .padding(vertical = 12.dp)
                            .fillMaxWidth()
                    ) {

                        ButtonPrimaryHollow(
                            label = "Scan Again".uppercase(),
                            onClick = vm::ignoreRecentRideGridGroupInvite,
                            modifier = Modifier
                                .padding(vertical = 16.dp)
                        )

                        Spacer(modifier = Modifier.width(16.dp))

                        ButtonPrimary(
                            label = "Join".uppercase(),
                            onClick = {
                                with(rideGridScanResult) {
                                    this ?: return@with
                                    vm.joinRideGridGroup(this,  isPrimary = setAsPrimaryGroup,navController)
                                }
//                        navController.navigate(DashboardViewDestination.RideGridInviteRidersDest.route)
                            },
                            modifier = Modifier
                                .padding(vertical = 16.dp)
                                .weight(1f)
                        )
                    }


                }




                Spacer(modifier = Modifier.weight(0.1f))

                Spacer(
                    modifier = Modifier
                        .fillMaxWidth(0.6f)
                        .height(2.dp)
                        .background(
                            color = Color.White,
                            shape = RoundedCornerShape(50)
                        )
                )

                Spacer(modifier = Modifier.weight(0.1f))

                Text(
                    modifier = Modifier
                        .wrapContentWidth()
                        .padding(vertical = 4.dp, horizontal = 16.dp),
                    text = "Create new Group",
                    style = MaterialTheme.typography.caption,
                    color = Color.White,
                    fontWeight = FontWeight.SemiBold
                )

                EditText(
                    label = "Group Name",
                    text = groupName,
                    onValueChange = vm::onGroupNameUpdate
                )

                Row(
                    modifier = Modifier
                        .padding(vertical = 12.dp)
                        .fillMaxWidth()
                ) {
                    Checkbox(
                        checked = setAsPrimaryGroup, onCheckedChange = vm::onPrimaryGroupSwitchToggle
                    )

                    Text(
                        modifier = Modifier
                            .wrapContentWidth()
                            .padding(vertical = 4.dp, horizontal = 16.dp),
                        text = "Set as primary RIDEGRID group",
                        style = MaterialTheme.typography.body2,
                        color = Color.White,
                        fontWeight = FontWeight.SemiBold
                    )
                }

                ButtonPrimary(
                    label = "Create".uppercase(),
                    onClick = {
                        if (groupName.isNotBlank())
                        vm.createRideGridGroup(name = groupName, isPrimary = setAsPrimaryGroup, navController = navController)
                    },
                    modifier = Modifier
                        .align(alignment = Alignment.End)
                        .padding(vertical = 16.dp)
                )


            }
        }
    }
}

@Composable
fun EditText(
    modifier: Modifier = Modifier,
    label: String,
    enabled: Boolean = true,
    onValueChange: (String) -> Unit,
    text: String,
    textFieldBackGroundColor: Color? = null,
    showCharacterCount: Boolean = false,
    maxCharacterCount: Int = 0
) {

    Column(
        horizontalAlignment = Alignment.Start,
        modifier = modifier,
    ) {

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 4.dp, horizontal = 16.dp),
            text = label,
            style = MaterialTheme.typography.caption,
            color = ColorTextPrimaryAccent,
            fontWeight = FontWeight.SemiBold
        )

        TextField(
            value = text,
            onValueChange = onValueChange,
            shape = RoundedCornerShape(10.dp),
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = textFieldBackGroundColor ?: Color(if (enabled) 0xFF17324A else 0xFF0D1D2B),
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent
            ),
            singleLine = true,
            modifier = Modifier.fillMaxWidth(),
            textStyle = TextStyle(
                fontFamily = MaterialTheme.typography.body1.fontFamily,
                fontWeight = FontWeight.Normal,
                fontSize = MaterialTheme.typography.body1.fontSize,
                fontStyle = MaterialTheme.typography.body1.fontStyle,
                color = ColorTextPrimary
            ),
            enabled = enabled,
        )
        if (showCharacterCount)
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 0.dp, horizontal = 16.dp),
                text = "${text.length}/$maxCharacterCount",
                style = MaterialTheme.typography.overline,
                color = ColorTextPrimaryAccent,
                fontWeight = FontWeight.SemiBold,
                textAlign = TextAlign.End,
                letterSpacing = 0.8.sp
            )

    }

}


@Preview
@Composable
fun Preview2() {
    RideGridCreateGroupView(
        rememberNavController(),
        object : RideGridGroupViewModelInterface, RideGridCreateGroupViewModelInterface {
            override val rideGridActiveClusterRiders: LiveData<List<RideGridUserDevice>>
                get() = MutableLiveData()
            override val rideGridGroups: LiveData<List<RideGridGroup>>
                get() = MutableLiveData()
            override val enableGroupDeletion: StateFlow<Boolean>
                get() = MutableStateFlow(false)

            override fun navigateToRideGridDashboard(navController: NavController, rideGridGroup: RideGridGroup) {

            }

            override fun navigateToCreateNewRideGridGroup(navController: NavController) {

            }

            override fun navigateToRideGridInviteRiders(navController: NavController, groupId: String) {
                TODO("Not yet implemented")
            }

            override fun deleteRideGridGroup(rideGridGroup: RideGridGroup) {

            }

            override fun markRideGridGroupPrimary(rideGridGroup: RideGridGroup) {

            }

            override fun rideGridAction(rideGridIntercomAction: RideGridIntercomAction) {

            }

            override fun toggleGroupDeletion(enable: Boolean) {

            }

            override val deviceInfo: LiveData<DeviceInfo>
                get() = MutableLiveData()
            override val batteryLevel: LiveData<Int>
                get() = MutableLiveData()
            override val rideGridIntercomState: LiveData<RideGridIntercomState>
                get() = MutableLiveData()
            override val rideGridScanResultStateFlow: StateFlow<RideGridGroupInvitation?>
                get() = MutableStateFlow(null)
            override val newGroupName: LiveData<String>
                get() = MutableLiveData()
            override var newGroupId: Long
                get() = 0
                set(value) {}
            override val setPrimaryGroup: LiveData<Boolean>
                get() = MutableLiveData()

            override fun createRideGridGroup(name: String, isPrimary: Boolean, navController: NavController) {
            }

            override fun joinRideGridGroup(rideGridGroupInvitation: RideGridGroupInvitation,isPrimary: Boolean, navController: NavController) {
                TODO("Not yet implemented")
            }

            override fun onGroupNameUpdate(string: String) {}
            override fun newRideGridGroupFound(rideGridGroupInvitation: RideGridGroupInvitation) {
                TODO("Not yet implemented")
            }

            override fun ignoreRecentRideGridGroupInvite() {}

            override fun onPrimaryGroupSwitchToggle(enabled: Boolean) {}
        }
    )
}