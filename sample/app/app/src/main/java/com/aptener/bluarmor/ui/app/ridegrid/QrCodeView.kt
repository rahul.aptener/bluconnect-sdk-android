package com.aptener.bluarmor.ui.app.ridegrid

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import com.aptener.bluarmor.R
import com.aptener.bluarmor.ui.theme.BluArmorTheme
import com.aptener.bluarmor.ui.theme.ColorShark

@Preview(showBackground = true)
@Composable
fun QRCodeView(dismiss: () -> Unit = {}) {
    BluArmorTheme {
        Dialog(onDismissRequest = { dismiss.invoke() }) {
            Card(
                backgroundColor = Color.White,
                elevation = 8.dp,
                shape = RoundedCornerShape(8.dp)
            ) {
                Column(
                    modifier = Modifier
                        .background(color = Color.White)
                        .padding(vertical = 16.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {

                    Text(
                        text = "BluArmor",
                        style = MaterialTheme.typography.subtitle2,
                        color = ColorShark,
                        fontWeight = FontWeight.Normal
                    )

                    Text(
                        text = "RIDEGRID".uppercase(),
                        style = MaterialTheme.typography.caption,
                        color = ColorShark,
                        fontWeight = FontWeight.SemiBold,
                        modifier = Modifier.alpha(0.6f)
                    )
                    Image(
                        imageVector = ImageVector.vectorResource(R.drawable.ic_qrcode_temp),
                        contentDescription = null,
                        modifier = Modifier
                            .padding(16.dp)
                            .fillMaxWidth(1f)
                    )

                    Spacer(
                        modifier = Modifier
                            .alpha(0.2f)
                            .padding(vertical = 10.dp)
                            .fillMaxWidth(0.8f)
                            .height(1.dp)
                            .background(color = ColorShark, shape = RoundedCornerShape(50))
                    )


                    Button(
                        onClick = { },
                        shape = RoundedCornerShape(50),
                        elevation = ButtonDefaults.elevation(0.dp),
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = ColorShark,
                            contentColor = Color.White
                        ),
                        modifier = Modifier
                            .wrapContentWidth()
                            .wrapContentHeight(),

                        ) {
                        Row(verticalAlignment = Alignment.CenterVertically) {

                            Icon(
                                modifier = Modifier
                                    .width(14.dp)
                                    .height(14.dp),
                                imageVector = ImageVector.vectorResource(id = R.drawable.ic_share),
                                contentDescription = "Share icon",
                                tint = Color.White,
                            )

                            Spacer(modifier = Modifier.width(8.dp))
                            Text(
                                text = "Share Invite Link",
                                style = MaterialTheme.typography.body2,
                                fontWeight = FontWeight.Medium,
                                color = Color.White
                            )
                        }
                    }

                }
            }
        }
    }
}