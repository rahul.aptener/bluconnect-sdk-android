package com.aptener.bluarmor.ui.app.ridegrid

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.aptener.bluarmor.R
import com.aptener.bluarmor.service.bluconnect.model.*
import com.aptener.bluarmor.ui.app.dashboard.DeviceInfoCard
import com.aptener.bluarmor.ui.theme.BluArmorTheme
import com.aptener.bluarmor.ui.theme.ColorNegative
import com.aptener.bluarmor.ui.theme.ColorPositive
import com.aptener.bluarmor.ui.theme.ColorShark
import com.aptener.bluarmor.ui.util.EdgeCard
import com.aptener.bluconnect.device.sxcontrol.Battery
import com.google.accompanist.flowlayout.FlowRow
import kotlinx.coroutines.flow.StateFlow

@Preview(
    showBackground = true
)
@Composable
fun Preview() {
    RideGridDashboardView(
        navController = rememberNavController(),
        vm = object : RideGridViewModelInterface {
            override val rideGridRiders: LiveData<List<RideGridUserDevice>> = MutableLiveData()
            override val rideGridRidersRadarMapStateFlow: StateFlow<MutableMap<String, RideGridUserDevice>>
                get() = TODO("Not yet implemented")
            override val rideGridRidersStateFlow: StateFlow<List<RideGridUserDevice>>
                get() = TODO("Not yet implemented")
            override val rideGridStateFlow: StateFlow<RideGridIntercomState>
                get() = TODO("Not yet implemented")
            override val rideGridIntercomState: LiveData<RideGridIntercomState>
                get() = MutableLiveData()
            override val deviceInfo: LiveData<DeviceInfo>
                get() = MutableLiveData()
            override val batteryLevel: LiveData<Int>
                get() = MutableLiveData()
            override val chargingState: LiveData<Battery.ChargingState>
                get() = MutableLiveData()
            override val musicState: LiveData<MusicState>
                get() = MutableLiveData()
            override val phoneCallState: LiveData<PhoneCallState>
                get() = MutableLiveData()
            override val rideGridRidersRadarMapLiveData: LiveData<MutableMap<String, RideGridUserDevice>>
                get() = MutableLiveData()

            override fun musicAction(musicAction: MusicAction) {

            }

            override fun phoneCallAction(phoneCallAction: PhoneCallAction) {

            }

            override fun rideGridAction(rideGridIntercomAction: RideGridIntercomAction) {

            }

            override fun navigateToRideGridInviteRiders(navController: NavController) {

            }
        })
}

@Composable
fun RideGridDashboardView(
    navController: NavController, vm: RideGridViewModelInterface
) {
    val rowScrollState = rememberScrollState()
//
//    val rideGridRidersMap: MutableMap<String, RideGridUserDevice> by vm.rideGridRidersRadarMapLiveData.observeAsState(
//        mutableMapOf()
//    )
//    val rideGridRiders: List<RideGridUserDevice> by vm.rideGridRiders.observeAsState(listOf())


    val rideGridRidersMap: MutableMap<String, RideGridUserDevice> by vm.rideGridRidersRadarMapStateFlow.collectAsState()
    val rideGridRiders: List<RideGridUserDevice> by vm.rideGridRidersStateFlow.collectAsState()
    val rideGridState: RideGridIntercomState by vm.rideGridIntercomState.observeAsState(RideGridIntercomState.OFF)
    BluArmorTheme {
        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {
            TopAppBar(
                backgroundColor = MaterialTheme.colors.background,
                elevation = 0.dp,
                modifier = Modifier.padding(end = 8.dp)
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Image(
                        imageVector = ImageVector.vectorResource(R.drawable.ic_back_nav),
                        contentDescription = null,
                        modifier = Modifier
                            .padding(0.dp)
                            .clickable {
                                navController.popBackStack()
                            }
                    )

                    Spacer(modifier = Modifier.weight(1f))

                    if (arrayOf(
                            RideGridIntercomState.DISCOVERY,
                            RideGridIntercomState.CONNECTED,
                            RideGridIntercomState.INCOMING_CALL,
                        ).contains(rideGridState)
                    ) {
                        Button(
                            onClick = { vm.rideGridAction(RideGridIntercomAction.TurnOff) },
                            shape = RoundedCornerShape(50),
                            elevation = ButtonDefaults.elevation(0.dp),
                            colors = ButtonDefaults.buttonColors(
                                backgroundColor = ColorNegative,
                                contentColor = Color.White
                            ),
                            modifier = Modifier
                                .wrapContentWidth()
                                .wrapContentHeight(),

                            ) {
                            Row(verticalAlignment = Alignment.CenterVertically) {
                                Text(
                                    text = "Turn Off",
                                    style = MaterialTheme.typography.body2,
                                    fontWeight = FontWeight.SemiBold,
                                    color = Color.White
                                )
                            }
                        }
                        Spacer(modifier = Modifier.width(8.dp))

                    }


                    Button(
                        onClick = { vm.navigateToRideGridInviteRiders(navController) },
                        shape = RoundedCornerShape(50),
                        elevation = ButtonDefaults.elevation(0.dp),
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = Color.White,
                            contentColor = ColorShark
                        ),
                        modifier = Modifier
                            .wrapContentWidth()
                            .wrapContentHeight(),

                        ) {
                        Row(verticalAlignment = Alignment.CenterVertically) {

                            Icon(
                                modifier = Modifier
                                    .width(14.dp)
                                    .height(14.dp),
                                imageVector = ImageVector.vectorResource(id = R.drawable.ic_share),
                                contentDescription = "Share icon",
                                tint = ColorShark,
                            )

                            Spacer(modifier = Modifier.width(4.dp))
                            Text(
                                text = "Invite",
                                style = MaterialTheme.typography.body2,
                                fontWeight = FontWeight.SemiBold,
                                color = ColorShark
                            )
                        }
                    }
                }
            }

            DeviceInfoCard(
                vm.deviceInfo,
                vm.batteryLevel,
                vm.chargingState,
                vm.musicState,
                vm.phoneCallState,
                vm::musicAction,
                vm::phoneCallAction
            )

            Column(
                modifier = Modifier
                    .verticalScroll(rowScrollState)
                    .weight(1f)
            ) {
                Row(
                    modifier = Modifier
                        .padding(horizontal = 16.dp, vertical = 12.dp),
                    horizontalArrangement = Arrangement.Center,
                ) {

                    if (rideGridState == RideGridIntercomState.OFF) {
                        EdgeCard(
                            modifier = Modifier
                                .weight(1f)
                                .height(52.dp)
                                .clickable {
                                    vm.rideGridAction(RideGridIntercomAction.TurnOn)
                                },
                            bottomStrokeColor = ColorPositive
                        ) {
                            Column(
                                horizontalAlignment = Alignment.CenterHorizontally,
                                verticalArrangement = Arrangement.Center
                            ) {
                                Text(
                                    text = "Turn On".uppercase(),
                                    style = MaterialTheme.typography.subtitle2,
                                    color = Color.White,
                                    fontWeight = FontWeight.Normal
                                )

                                Text(
                                    text = "RIDEGRID".uppercase(),
                                    style = MaterialTheme.typography.caption,
                                    color = Color.White,
                                    fontWeight = FontWeight.SemiBold,
                                    modifier = Modifier.alpha(0.8f)
                                )
                            }
                        }
                    }
                    if (rideGridState == RideGridIntercomState.DISCOVERY)
                        Text(
                            text = "Looking for devices!",
                            style = MaterialTheme.typography.subtitle2,
                            color = Color.White,
                            fontWeight = FontWeight.Normal
                        )


                    if (rideGridState == RideGridIntercomState.CONNECTED || rideGridState == RideGridIntercomState.INCOMING_CALL)
                        EdgeCard(
                            modifier = Modifier
                                .weight(1f)
                                .height(52.dp)
                                .clickable {
                                    vm.rideGridAction(RideGridIntercomAction.StartCall)
                                },
                            bottomStrokeColor = ColorPositive
                        ) {
                            Column(
                                horizontalAlignment = Alignment.CenterHorizontally,
                                verticalArrangement = Arrangement.Center
                            ) {
                                Text(
                                    text =
                                    if (rideGridState == RideGridIntercomState.INCOMING_CALL)
                                        "Join".uppercase()
                                    else
                                        "Start".uppercase(),
                                    style = MaterialTheme.typography.subtitle2,
                                    color = Color.White,
                                    fontWeight = FontWeight.Normal
                                )

                                Text(
                                    text = "Call".uppercase(),
                                    style = MaterialTheme.typography.caption,
                                    color = Color.White,
                                    fontWeight = FontWeight.SemiBold,
                                    modifier = Modifier.alpha(0.8f)
                                )
                            }
                        }

                    if (rideGridState == RideGridIntercomState.ACTIVE_CALL || rideGridState == RideGridIntercomState.MUTED) {
                        EdgeCard(
                            modifier = Modifier
                                .weight(1f)
                                .height(52.dp)
                                .clickable {
                                    vm.rideGridAction(RideGridIntercomAction.StopCall)
                                },
                            bottomStrokeColor = ColorNegative
                        ) {
                            Column(
                                horizontalAlignment = Alignment.CenterHorizontally,
                                verticalArrangement = Arrangement.Center
                            ) {
                                Text(
                                    text = "End".uppercase(),
                                    style = MaterialTheme.typography.subtitle2,
                                    color = Color.White,
                                    fontWeight = FontWeight.Normal
                                )

                                Text(
                                    text = "Call".uppercase(),
                                    style = MaterialTheme.typography.caption,
                                    color = Color.White,
                                    fontWeight = FontWeight.SemiBold,
                                    modifier = Modifier.alpha(0.8f)
                                )
                            }
                        }

                        Spacer(modifier = Modifier.width(16.dp))

                        EdgeCard(
                            modifier = Modifier
                                .weight(1f)
                                .height(52.dp)
                                .clickable {
                                    if (rideGridState == RideGridIntercomState.MUTED)
                                        vm.rideGridAction(RideGridIntercomAction.UnMute)
                                    else
                                        vm.rideGridAction(RideGridIntercomAction.Mute)
                                },
                            bottomStrokeColor = ColorPositive
                        ) {
                            Column(
                                horizontalAlignment = Alignment.CenterHorizontally,
                                verticalArrangement = Arrangement.Center
                            ) {
                                Text(
                                    text = if (rideGridState == RideGridIntercomState.MUTED)
                                        "UnMute".uppercase()
                                    else "Mute".uppercase(),
                                    style = MaterialTheme.typography.subtitle2,
                                    color = Color.White,
                                    fontWeight = FontWeight.Normal
                                )

                                Text(
                                    text = "Call".uppercase(),
                                    style = MaterialTheme.typography.caption,
                                    color = Color.White,
                                    fontWeight = FontWeight.SemiBold,
                                    modifier = Modifier.alpha(0.8f)
                                )
                            }
                        }
                    }
                }

                RideGridRadarView(rideGridRidersMap)

                FlowRow(
                    modifier = Modifier
                        .padding(16.dp)
                        .fillMaxWidth(),
                    mainAxisSpacing = 8.dp,
                    crossAxisSpacing = 6.dp
                ) {

                    rideGridRiders.forEach {
                        RideGridUserCapsule(it)
                    }

                }
            }
            Text(
                text = "Updates every 30 seconds".uppercase(),
                style = MaterialTheme.typography.overline,
                color = Color.White,
                fontWeight = FontWeight.Normal,
                textAlign = TextAlign.Center
            )
        }
    }
}


@OptIn(androidx.compose.ui.ExperimentalComposeUiApi::class)
@Composable
fun RideGridRadarView(rideGridUsers: Map<String, RideGridUserDevice>) {
    Box(Modifier) {
        Spacer(
            modifier = Modifier
                .padding(32.dp)
                .aspectRatio(1f)
                .background(
                    color = Color(0xFF1A1A1A),
                    shape = CircleShape
                )
                .fillMaxWidth()
        )
        Spacer(
            modifier = Modifier
                .padding(72.dp)
                .aspectRatio(1f)
                .background(
                    color = Color(0xFF282828),
                    shape = CircleShape
                )
                .fillMaxWidth()
        )
        Spacer(
            modifier = Modifier
                .padding(112.dp)
                .aspectRatio(1f)
                .background(
                    color = Color(0xFF313131),
                    shape = CircleShape
                )
                .fillMaxWidth()
        )

        ConstraintLayout(
            modifier = Modifier
                .aspectRatio(1f)
                .fillMaxWidth()
        ) {
            val (
                c,
                r1,
                r2,
                r3,
                r4,
                r5,
                r6,
                r7,
                r8,
                r9,
                r10,
                r11,
                r12,
                r13,
                r14,
                r15,
            ) = createRefs()

            Icon(
                imageVector = ImageVector.vectorResource(id = R.drawable.ic_navigation),
                contentDescription = "Navigation Icon",
                tint = Color(0xFF009DFF),
                modifier = Modifier
                    .constrainAs(c) {
                        centerTo(parent)
                    }
                    .padding(20.dp)
                    .width(32.dp)

            )

            with(rideGridUsers["T1"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r1) {
                                this.bottom.linkTo(c.top)
//                    top.linkTo(parent.top, margin = 16.dp)
                                centerHorizontallyTo(parent)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(modifier = Modifier
                        .constrainAs(r1) {
                            this.bottom.linkTo(c.top)
//                    top.linkTo(parent.top, margin = 16.dp)
                            centerHorizontallyTo(parent)
                        }
                        .padding(2.dp))
            }

            with(rideGridUsers["T2"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r2) {
                                this.bottom.linkTo(r1.top)
                                centerHorizontallyTo(parent)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r2) {
                                this.bottom.linkTo(r1.top)
                                centerHorizontallyTo(parent)
                            }
                            .padding(2.dp)
                    )
            }

            with(rideGridUsers["T3"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r3) {
                                this.start.linkTo(r2.end)
                                this.end.linkTo(r2.end)
                                this.bottom.linkTo(r2.top)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r3) {
                                this.start.linkTo(r2.end)
                                this.end.linkTo(r2.end)
                                this.bottom.linkTo(r2.top)
                            }
                            .padding(2.dp)
                    )
            }

            with(rideGridUsers["T4"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r4) {
                                this.start.linkTo(r2.start)
                                this.end.linkTo(r2.start)
                                this.bottom.linkTo(r2.top)
                            }
                            .padding(2.dp)
                    ) else
                    RideGridUserFlatCapsuleEmpty(modifier = Modifier
                        .constrainAs(r4) {
                            this.start.linkTo(r2.start)
                            this.end.linkTo(r2.start)
                            this.bottom.linkTo(r2.top)
                        }
                        .padding(2.dp)
                    )
            }

            with(rideGridUsers["T5"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r5) {
                                this.centerHorizontallyTo(parent)
                                this.bottom.linkTo(r4.top)
                            }
                            .padding(2.dp)
                    ) else
                    RideGridUserFlatCapsuleEmpty(modifier = Modifier
                        .constrainAs(r5) {
                            this.centerHorizontallyTo(parent)
                            this.bottom.linkTo(r4.top)
                        }
                        .padding(2.dp)
                    )
            }

            with(rideGridUsers["T6"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r6) {
                                this.start.linkTo(r5.end)
                                this.bottom.linkTo(r4.top)
                            }
                            .padding(2.dp)
                    ) else
                    RideGridUserFlatCapsuleEmpty(modifier = Modifier
                        .constrainAs(r6) {
                            this.start.linkTo(r5.end)
                            this.bottom.linkTo(r4.top)
                        }
                        .padding(2.dp)
                    )
            }

            with(rideGridUsers["T7"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r7) {
                                this.end.linkTo(r5.start)
                                this.bottom.linkTo(r4.top)
                            }
                            .padding(2.dp)
                    ) else
                    RideGridUserFlatCapsuleEmpty(modifier = Modifier
                        .constrainAs(r7) {
                            this.end.linkTo(r5.start)
                            this.bottom.linkTo(r4.top)
                        }
                        .padding(2.dp)
                    )
            }

            with(rideGridUsers["T8"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r8) {
                                this.start.linkTo(r5.end)
                                this.end.linkTo(r5.end)
                                this.bottom.linkTo(r5.top)
                            }
                            .padding(2.dp)
                    ) else
                    RideGridUserFlatCapsuleEmpty(modifier = Modifier
                        .constrainAs(r8) {
                            this.start.linkTo(r5.end)
                            this.end.linkTo(r5.end)
                            this.bottom.linkTo(r5.top)
                        }
                        .padding(2.dp)
                    )
            }

            with(rideGridUsers["T9"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r9) {
                                this.start.linkTo(r5.start)
                                this.end.linkTo(r5.start)
                                this.bottom.linkTo(r5.top)
                            }
                            .padding(2.dp)
                    ) else
                    RideGridUserFlatCapsuleEmpty(modifier = Modifier
                        .constrainAs(r9) {
                            this.start.linkTo(r5.start)
                            this.end.linkTo(r5.start)
                            this.bottom.linkTo(r5.top)
                        }
                        .padding(2.dp)
                    )
            }

            with(rideGridUsers["T10"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r10) {
                                this.centerHorizontallyTo(parent)
                                this.bottom.linkTo(r9.top)
                            }
                            .padding(2.dp)
                    ) else
                    RideGridUserFlatCapsuleEmpty(modifier = Modifier
                        .constrainAs(r10) {
                            this.centerHorizontallyTo(parent)
                            this.bottom.linkTo(r9.top)
                        }
                        .padding(2.dp)
                    )
            }

            with(rideGridUsers["T11"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r11) {
                                this.start.linkTo(r10.end)
                                this.bottom.linkTo(r9.top)
                            }
                            .padding(2.dp)
                    ) else
                    RideGridUserFlatCapsuleEmpty(modifier = Modifier
                        .constrainAs(r11) {
                            this.start.linkTo(r10.end)
                            this.bottom.linkTo(r9.top)
                        }
                        .padding(2.dp)
                    )
            }

            with(rideGridUsers["T12"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r12) {
                                this.end.linkTo(r10.start)
                                this.bottom.linkTo(r9.top)
                            }
                            .padding(2.dp)
                    ) else
                    RideGridUserFlatCapsuleEmpty(modifier = Modifier
                        .constrainAs(r12) {
                            this.end.linkTo(r10.start)
                            this.bottom.linkTo(r9.top)
                        }
                        .padding(2.dp)
                    )
            }


            with(rideGridUsers["T13"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r13) {
                                this.start.linkTo(r2.end)
                                this.bottom.linkTo(r1.top)
                            }
                            .padding(2.dp)
                    ) else
                    RideGridUserFlatCapsuleEmpty(modifier = Modifier
                        .constrainAs(r13) {
                            this.start.linkTo(r2.end)
                            this.bottom.linkTo(r1.top)
                        }
                        .padding(2.dp)
                    )
            }


            with(rideGridUsers["T14"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r14) {
                                this.end.linkTo(r2.start)
                                this.bottom.linkTo(r1.top)
                            }
                            .padding(2.dp)
                    ) else
                    RideGridUserFlatCapsuleEmpty(modifier = Modifier
                        .constrainAs(r14) {
                            this.end.linkTo(r2.start)
                            this.bottom.linkTo(r1.top)
                        }
                        .padding(2.dp)
                    )
            }

        }

        ConstraintLayout(
            modifier = Modifier
                .aspectRatio(1f)
                .fillMaxWidth()
        ) {
            val (
                c,
                r1,
                r2,
                r3,
                r4,
                r5,
                r6,
                r7,
                r8,
                r9,
                r10,
                r11,
                r12,
                r13,
                r14,
                r15,
            ) = createRefs()

            Icon(
                imageVector = ImageVector.vectorResource(id = R.drawable.ic_navigation),
                contentDescription = "Navigation Icon",
                tint = Color(0xFF009DFF),
                modifier = Modifier
                    .constrainAs(c) {
                        centerTo(parent)
                    }
                    .padding(20.dp)
                    .width(32.dp)

            )

            with(rideGridUsers["B1"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r1) {
                                this.top.linkTo(c.bottom)
//                    top.linkTo(parent.top, margin = 16.dp)
                                centerHorizontallyTo(parent)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r1) {
                                this.top.linkTo(c.bottom)
//                    top.linkTo(parent.top, margin = 16.dp)
                                centerHorizontallyTo(parent)
                            }
                            .padding(2.dp)
                    )
            }



            with(rideGridUsers["B2"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r2) {
                                this.top.linkTo(r1.bottom)
                                centerHorizontallyTo(parent)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r2) {
                                this.top.linkTo(r1.bottom)
                                centerHorizontallyTo(parent)
                            }
                            .padding(2.dp)
                    )
            }

            with(rideGridUsers["B3"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r3) {
                                this.start.linkTo(r2.end)
                                this.end.linkTo(r2.end)
                                this.top.linkTo(r2.bottom)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r3) {
                                this.start.linkTo(r2.end)
                                this.end.linkTo(r2.end)
                                this.top.linkTo(r2.bottom)
                            }
                            .padding(2.dp)
                    )
            }
            with(rideGridUsers["B4"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r4) {
                                this.start.linkTo(r2.start)
                                this.end.linkTo(r2.start)
                                this.top.linkTo(r2.bottom)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r4) {
                                this.start.linkTo(r2.start)
                                this.end.linkTo(r2.start)
                                this.top.linkTo(r2.bottom)
                            }
                            .padding(2.dp)
                    )
            }

            with(rideGridUsers["B5"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r5) {
                                this.centerHorizontallyTo(parent)
                                this.top.linkTo(r4.bottom)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r5) {
                                this.centerHorizontallyTo(parent)
                                this.top.linkTo(r4.bottom)
                            }
                            .padding(2.dp)
                    )
            }
            with(rideGridUsers["B6"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r6) {
                                this.start.linkTo(r5.end)
                                this.top.linkTo(r4.bottom)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r6) {
                                this.start.linkTo(r5.end)
                                this.top.linkTo(r4.bottom)
                            }
                            .padding(2.dp)
                    )
            }

            with(rideGridUsers["B7"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r7) {
                                this.end.linkTo(r5.start)
                                this.top.linkTo(r4.bottom)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r7) {
                                this.end.linkTo(r5.start)
                                this.top.linkTo(r4.bottom)
                            }
                            .padding(2.dp)
                    )
            }

            with(rideGridUsers["B8"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r8) {
                                this.start.linkTo(r5.end)
                                this.end.linkTo(r5.end)
                                this.top.linkTo(r5.bottom)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r8) {
                                this.start.linkTo(r5.end)
                                this.end.linkTo(r5.end)
                                this.top.linkTo(r5.bottom)
                            }
                            .padding(2.dp)
                    )
            }

            with(rideGridUsers["B9"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r9) {
                                this.start.linkTo(r5.start)
                                this.end.linkTo(r5.start)
                                this.top.linkTo(r5.bottom)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r9) {
                                this.start.linkTo(r5.start)
                                this.end.linkTo(r5.start)
                                this.top.linkTo(r5.bottom)
                            }
                            .padding(2.dp)
                    )
            }

            with(rideGridUsers["B10"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r10) {
                                this.centerHorizontallyTo(parent)
                                this.top.linkTo(r9.bottom)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r10) {
                                this.centerHorizontallyTo(parent)
                                this.top.linkTo(r9.bottom)
                            }
                            .padding(2.dp)
                    )
            }



            with(rideGridUsers["B11"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r11) {
                                this.start.linkTo(r10.end)
                                this.top.linkTo(r9.bottom)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r11) {
                                this.start.linkTo(r10.end)
                                this.top.linkTo(r9.bottom)
                            }
                            .padding(2.dp)
                    )
            }



            with(rideGridUsers["B12"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r12) {
                                this.end.linkTo(r10.start)
                                this.top.linkTo(r9.bottom)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r12) {
                                this.end.linkTo(r10.start)
                                this.top.linkTo(r9.bottom)
                            }
                            .padding(2.dp)
                    )
            }



            with(rideGridUsers["B13"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r13) {
                                this.start.linkTo(r2.end)
                                this.top.linkTo(r1.bottom)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r13) {
                                this.start.linkTo(r2.end)
                                this.top.linkTo(r1.bottom)
                            }
                            .padding(2.dp)
                    )
            }


            with(rideGridUsers["B14"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r14) {
                                this.end.linkTo(r2.start)
                                this.top.linkTo(r1.bottom)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r14) {
                                this.end.linkTo(r2.start)
                                this.top.linkTo(r1.bottom)
                            }
                            .padding(2.dp)
                    )
            }

        }


        ConstraintLayout(
            modifier = Modifier
                .aspectRatio(1f)
                .fillMaxWidth()
        ) {
            val (
                c,
                r1,
                r2,
                r3,
                r4,
                r5,
                r6,
                r7,
                r8,
                r9,
                r10,
                r11,
                r12,
                r13,
                r14,
                r15,
            ) = createRefs()

            Icon(
                imageVector = ImageVector.vectorResource(id = R.drawable.ic_navigation),
                contentDescription = "Navigation Icon",
                tint = Color(0xFF009DFF),
                modifier = Modifier
                    .constrainAs(c) {
                        centerTo(parent)
                    }
                    .padding(20.dp)
                    .width(32.dp)

            )

            with(rideGridUsers["R1"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r1) {
                                this.start.linkTo(c.end)
//                    top.linkTo(parent.top, margin = 16.dp)
                                centerVerticallyTo(parent)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r1) {
                                this.start.linkTo(c.end)
//                    top.linkTo(parent.top, margin = 16.dp)
                                centerVerticallyTo(parent)
                            }
                            .padding(2.dp)
                    )
            }



            with(rideGridUsers["R2"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r2) {
                                this.top.linkTo(r1.bottom)
                                this.start.linkTo(r1.end)
                                this.end.linkTo(r1.end)

                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r2) {
                                this.top.linkTo(r1.bottom)
                                this.start.linkTo(r1.end)
                                this.end.linkTo(r1.end)

                            }
                            .padding(2.dp)
                    )
            }

            with(rideGridUsers["R3"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r3) {
                                this.bottom.linkTo(r1.top)
                                this.start.linkTo(r1.end)
                                this.end.linkTo(r1.end)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r3) {
                                this.bottom.linkTo(r1.top)
                                this.start.linkTo(r1.end)
                                this.end.linkTo(r1.end)
                            }
                            .padding(2.dp)
                    )
            }


            with(rideGridUsers["R4"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r4) {
                                this.end.linkTo(c.start)
//                    top.linkTo(parent.top, margin = 16.dp)
                                centerVerticallyTo(parent)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r4) {
                                this.end.linkTo(c.start)
//                    top.linkTo(parent.top, margin = 16.dp)
                                centerVerticallyTo(parent)
                            }
                            .padding(2.dp)
                    )
            }

            with(rideGridUsers["R5"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r5) {
                                this.top.linkTo(r4.bottom)
                                this.start.linkTo(r4.start)
                                this.end.linkTo(r4.start)

                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r5) {
                                this.top.linkTo(r4.bottom)
                                this.start.linkTo(r4.start)
                                this.end.linkTo(r4.start)

                            }
                            .padding(2.dp)
                    )
            }

            with(rideGridUsers["R6"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r6) {
                                this.bottom.linkTo(r4.top)
                                this.start.linkTo(r4.start)
                                this.end.linkTo(r4.start)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r6) {
                                this.bottom.linkTo(r4.top)
                                this.start.linkTo(r4.start)
                                this.end.linkTo(r4.start)
                            }
                            .padding(2.dp)
                    )
            }

            with(rideGridUsers["R7"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r7) {
                                this.bottom.linkTo(r6.top)
                                this.start.linkTo(r4.start)
                                this.end.linkTo(r4.start)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r7) {
                                this.bottom.linkTo(r6.top)
                                this.start.linkTo(r4.start)
                                this.end.linkTo(r4.start)
                            }
                            .padding(2.dp)
                    )
            }



            with(rideGridUsers["R8"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r8) {
                                this.top.linkTo(r5.bottom)
                                this.start.linkTo(r4.start)
                                this.end.linkTo(r4.start)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r8) {
                                this.top.linkTo(r5.bottom)
                                this.start.linkTo(r4.start)
                                this.end.linkTo(r4.start)
                            }
                            .padding(2.dp)
                    )
            }


            with(rideGridUsers["R9"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r9) {
                                this.bottom.linkTo(r3.top)
                                this.start.linkTo(r1.end)
                                this.end.linkTo(r1.end)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r9) {
                                this.bottom.linkTo(r3.top)
                                this.start.linkTo(r1.end)
                                this.end.linkTo(r1.end)
                            }
                            .padding(2.dp)
                    )
            }


            with(rideGridUsers["R10"]) {
                if (this != null)
                    RideGridUserFlatCapsule(
                        this,
                        modifier = Modifier
                            .constrainAs(r10) {
                                this.top.linkTo(r2.bottom)
                                this.start.linkTo(r1.end)
                                this.end.linkTo(r1.end)
                            }
                            .padding(2.dp)
                    )
                else
                    RideGridUserFlatCapsuleEmpty(
                        modifier = Modifier
                            .constrainAs(r10) {
                                this.top.linkTo(r2.bottom)
                                this.start.linkTo(r1.end)
                                this.end.linkTo(r1.end)
                            }
                            .padding(2.dp)
                    )
            }
        }

        /*    Box(
                modifier = Modifier
                    .padding(112.dp)
                    .aspectRatio(1f)
                    .fillMaxWidth()
            ) {
                FlowRow(
                    modifier = Modifier
                        .padding(16.dp)
                        .fillMaxWidth(),
                    mainAxisSpacing = 8.dp,
                    crossAxisSpacing = 6.dp
                ) {

                    rideGridUsers.filter { it.distance in 0..100 }.forEach {
                        RideGridUserCapsule(it, true)
                    }

                }
            }

            Box(
                modifier = Modifier
                    .padding(72.dp)
                    .aspectRatio(1f)
                    .fillMaxWidth()
            ) {
                FlowRow(
                    modifier = Modifier
                        .padding(16.dp)
                        .fillMaxWidth(),
                    mainAxisSpacing = 8.dp,
                    crossAxisSpacing = 6.dp
                ) {

                    rideGridUsers.filter { it.distance in 101..200 }.forEach {
                        RideGridUserCapsule(it, true)
                    }

                }
            }
            Box(
                modifier = Modifier
                    .padding(32.dp)
                    .aspectRatio(1f)
                    .fillMaxWidth()
            ) {
                FlowRow(
                    modifier = Modifier
                        .padding(16.dp)
                        .fillMaxWidth(),
                    mainAxisSpacing = 8.dp,
                    crossAxisSpacing = 6.dp
                ) {
                    rideGridUsers.filter { it.distance > 200 }.forEach {
                        RideGridUserCapsule(it, true)
                    }

                }
            }
    */

    }
}

@Composable
fun RideGridUserCapsule(rideGridUserDevice: RideGridUserDevice, activeLocation: Boolean = false, modifier: Modifier = Modifier) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .background(
                color =
                if (activeLocation)
                    Color.Transparent
                else
                    Color(0xff1a1a1a),
                shape = RoundedCornerShape(50)
            )
            .border(if (rideGridUserDevice.directChild) 1.dp else 0.dp, if (rideGridUserDevice.directChild) ColorPositive else Color.Transparent, shape = RoundedCornerShape(50))
            .padding(vertical = 1.dp)
            .padding(start = 10.dp, end = 10.dp)
    ) {
//        Icon(
//            imageVector = ImageVector.vectorResource(id = rideGridUserDevice.imageRes),
//            contentDescription = "Helmet Icon",
//            tint = Color.White,
//            modifier = Modifier.width(22.dp)
//        )
//        Spacer(modifier = Modifier.width(8.dp))
        Column {
            Text(
                text = rideGridUserDevice.name,
                color = Color.White,
                fontWeight = FontWeight.SemiBold,
                style = MaterialTheme.typography.caption,
                modifier = Modifier.alpha(0.7f),
                maxLines = 1
            )
            Row(verticalAlignment = Alignment.CenterVertically) {

                if (!activeLocation) {
                    Spacer(
                        modifier = Modifier
                            .background(
                                color = ColorNegative,
                                shape = CircleShape
                            )
                            .width(8.dp)
                            .height(8.dp)
                    )
                    Spacer(modifier = Modifier.width(4.dp))
                }
                Text(
                    text = when {
                        rideGridUserDevice.distance == 0 -> "Nearby"
                        rideGridUserDevice.distance > 0 -> "${rideGridUserDevice.distance}m"
                        else -> "NO GPS"
                    },
                    color = Color.White,
                    fontWeight = FontWeight.Bold,
                    style = MaterialTheme.typography.caption,
                    modifier = Modifier.alpha(0.7f),
                    maxLines = 1
                )
            }
        }
    }
}


@Composable
fun RideGridUserFlatCapsule(rideGridUserDevice: RideGridUserDevice, activeLocation: Boolean = false, modifier: Modifier = Modifier) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .background(
                color =
                if (activeLocation)
                    Color.Transparent
                else
                    Color(0xff1a1a1a),
                shape = RoundedCornerShape(50)
            )
            .padding(vertical = 1.dp)
            .padding(start = 4.dp, end = 6.dp)
    ) {
//        Icon(
//            imageVector = ImageVector.vectorResource(id = rideGridUserDevice.imageRes),
//            contentDescription = "Helmet Icon",
//            tint = Color.White,
//            modifier = Modifier.width(22.dp)
//        )
//        Spacer(modifier = Modifier.width(8.dp))
        Column {
            Row(verticalAlignment = Alignment.CenterVertically) {

                if (!activeLocation) {
                    Spacer(
                        modifier = Modifier
                            .background(
                                color = ColorPositive,
                                shape = CircleShape
                            )
                            .width(8.dp)
                            .height(8.dp)
                    )
                    Spacer(modifier = Modifier.width(4.dp))
                }
                Text(
                    text = "${rideGridUserDevice.name}-${
                        when {
                            rideGridUserDevice.distance == 0 -> "Nearby"
                            rideGridUserDevice.distance > 0 -> "${rideGridUserDevice.distance}m"
                            else -> "NO GPS"
                        }
                    }",
                    color = Color.White,
                    fontWeight = FontWeight.Bold,
                    style = MaterialTheme.typography.caption,
                    modifier = Modifier.alpha(0.7f),
                    maxLines = 1
                )
            }
        }
    }
}

@Composable
fun RideGridUserFlatCapsuleEmpty(modifier: Modifier = Modifier) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .background(
                color = Color.Transparent,
                shape = RoundedCornerShape(50)
            )
            .padding(vertical = 1.dp)
            .padding(start = 4.dp, end = 6.dp)
    ) {
//        Icon(
//            imageVector = ImageVector.vectorResource(id = rideGridUserDevice.imageRes),
//            contentDescription = "Helmet Icon",
//            tint = Color.White,
//            modifier = Modifier.width(22.dp)
//        )
//        Spacer(modifier = Modifier.width(8.dp))
        Column {
            Row(verticalAlignment = Alignment.CenterVertically) {

                Spacer(
                    modifier = Modifier
                        .background(
                            color = Color.Transparent,
                            shape = CircleShape
                        )
                        .width(8.dp)
                        .height(8.dp)
                )
                Spacer(modifier = Modifier.width(4.dp))

                Text(
                    text = "         ",
                    color = Color.Transparent,
                    fontWeight = FontWeight.Bold,
                    style = MaterialTheme.typography.caption,
                    modifier = Modifier.alpha(0.7f),
                    maxLines = 1
                )
            }
        }
    }
}


@Composable
fun RideGridUserListItem(rideGridUserDevice: RideGridUserDevice, activeLocation: Boolean = false) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .background(
                color =
                if (activeLocation)
                    Color.Transparent
                else
                    Color(0xff1a1a1a),
                shape = RoundedCornerShape(50)
            )
            .padding(vertical = 3.dp)
            .padding(start = 8.dp, end = 14.dp)
    ) {
        Icon(
            imageVector = ImageVector.vectorResource(id = rideGridUserDevice.imageRes),
            contentDescription = "Helmet Icon",
            tint = Color.White,
            modifier = Modifier.width(22.dp)
        )
        Spacer(modifier = Modifier.width(8.dp))
        Column {
            Text(
                text = rideGridUserDevice.name,
                color = Color.White,
                fontWeight = FontWeight.SemiBold,
                style = MaterialTheme.typography.caption,
                modifier = Modifier.alpha(0.7f),
                maxLines = 1
            )
            Row(verticalAlignment = Alignment.CenterVertically) {
                Text(
                    text = "RIDEGRID",
                    color = Color.White,
                    fontWeight = FontWeight.Bold,
                    style = MaterialTheme.typography.caption,
                    modifier = Modifier.alpha(0.7f),
                    maxLines = 1
                )
            }
        }
    }
}