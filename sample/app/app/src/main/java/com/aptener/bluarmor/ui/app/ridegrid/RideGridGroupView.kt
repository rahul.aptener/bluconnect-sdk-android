package com.aptener.bluarmor.ui.app.ridegrid

import android.widget.Toast
import androidx.compose.foundation.*
import androidx.compose.foundation.gestures.rememberDraggableState
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.aptener.bluarmor.R
import com.aptener.bluarmor.service.bluconnect.model.*
import com.aptener.bluarmor.ui.app.buddylist.DeviceInfoCardCollapsed
import com.aptener.bluarmor.ui.theme.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import timber.log.Timber


interface RideGridGroupViewModelInterface {
    val deviceInfo: LiveData<DeviceInfo>
    val batteryLevel: LiveData<Int>

    val rideGridIntercomState: LiveData<RideGridIntercomState>
    val rideGridActiveClusterRiders: LiveData<List<RideGridUserDevice>>

    val rideGridGroups: LiveData<List<RideGridGroup>>

    val enableGroupDeletion: StateFlow<Boolean>

    fun navigateToRideGridDashboard(navController: NavController, rideGridGroup: RideGridGroup)
    fun navigateToCreateNewRideGridGroup(navController: NavController)
    fun navigateToRideGridInviteRiders(navController: NavController, groupId: String)
    fun deleteRideGridGroup(rideGridGroup: RideGridGroup)
    fun markRideGridGroupPrimary(rideGridGroup: RideGridGroup)
    fun rideGridAction(rideGridIntercomAction: RideGridIntercomAction)

    fun toggleGroupDeletion(enable: Boolean)

}

@ExperimentalFoundationApi
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun RideGridGroupView(
    navController: NavController, vm: RideGridGroupViewModelInterface
) {

    val context = LocalContext.current
    var toast = Toast(context)

    val rideGridState: RideGridIntercomState by vm.rideGridIntercomState.observeAsState(RideGridIntercomState.OFF)
    val enableDeletion: Boolean by vm.enableGroupDeletion.collectAsState()


    val scaffoldState = ScaffoldState(
        drawerState = DrawerState(initialValue = DrawerValue.Open),
        snackbarHostState = SnackbarHostState(),
    )

    val rideGridGroups: List<RideGridGroup> by vm.rideGridGroups.observeAsState(emptyList())
    val rideGridRiders: List<RideGridUserDevice> by vm.rideGridActiveClusterRiders.observeAsState(listOf())

    val offsetX = remember {
        mutableStateOf(1f)
    }

    val dragableState = rememberDraggableState { delta ->
        Timber.e("DRAG: ${offsetX.value}")
        offsetX.value = offsetX.value + delta
    }

    //    val rideGridRiders: List<RideGridUserDevice> by vm.rideGridRiders.observeAsState(listOf())
    //    val rideGridState: RideGridIntercomState by vm.rideGridIntercomState.observeAsState(RideGridIntercomState.OFF)
    BluArmorTheme {
        Scaffold(
            topBar = {
                TopAppBar(
                    backgroundColor = MaterialTheme.colors.background,
                    elevation = 0.dp,
                    modifier = Modifier.padding(end = 8.dp)
                ) {
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        verticalAlignment = Alignment.CenterVertically,
                    ) {
                        Image(
                            imageVector = ImageVector.vectorResource(R.drawable.ic_back_nav),
                            contentDescription = null,
                            modifier = Modifier
                                .padding(0.dp)
                                .clickable {
                                    navController.popBackStack()
                                }
                        )

                        Spacer(modifier = Modifier.weight(1f))

                        Button(
                            onClick = {
                                vm.rideGridAction(
                                    if (RideGridIntercomState.OFF != rideGridState)
                                        RideGridIntercomAction.TurnOff
                                    else RideGridIntercomAction.TurnOn
                                )
                            },
                            shape = RoundedCornerShape(50),
                            elevation = ButtonDefaults.elevation(0.dp),
                            colors = ButtonDefaults.buttonColors(
                                backgroundColor = if (RideGridIntercomState.OFF != rideGridState) ColorNegative
                                else ColorPositive,
                                contentColor = Color.White
                            ),
                            modifier = Modifier
                                .wrapContentWidth()
                                .wrapContentHeight(),

                            ) {
                            Row(verticalAlignment = Alignment.CenterVertically) {
                                Text(
                                    text =
                                    if (RideGridIntercomState.OFF != rideGridState)
                                        "Turn Off"
                                    else
                                        "Turn On",
                                    style = MaterialTheme.typography.body2,
                                    fontWeight = FontWeight.SemiBold,
                                    color = Color.White
                                )
                            }
                        }
                    }
                }
            },
            scaffoldState = scaffoldState
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
            ) {


                DeviceInfoCardCollapsed(
                    vm.deviceInfo,
                    vm.batteryLevel
                )

                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp, vertical = 8.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        modifier = Modifier
                            .defaultMinSize(minHeight = 42.dp)
                            .align(alignment = Alignment.CenterVertically),
                        text = "Groups".uppercase(),
                        style = MaterialTheme.typography.subtitle2,
                        color = Color.White,
                        fontWeight = FontWeight.SemiBold
                    )

                    Spacer(modifier = Modifier.weight(1f))

                    if (enableDeletion)
                        ButtonPrimary(label = "Done", onClick = { vm.toggleGroupDeletion(false) })

                }

                Row(Modifier.horizontalScroll(enabled = true, state = ScrollState(0))) {
                    for ((i, rideGridGroup) in rideGridGroups.withIndex()) {
                        RideGridGroupCard(
                            groupName = rideGridGroup.name,
                            isPrimary = rideGridGroup.isPrimary,
                            backgroundColorTint = Color(0xff4567C4 + i),
                            modifier = Modifier
                                .padding(start = if (i == 0) 8.dp else 0.dp, end = 8.dp)
                                .combinedClickable(onLongClick = { vm.toggleGroupDeletion(true) }) {
                                    if (!enableDeletion)
                                        vm.navigateToRideGridDashboard(navController, rideGridGroup)
                                },
                            navigateToRideGrid = { vm.navigateToRideGridDashboard(navController, rideGridGroup) },
                            enableDeletion = enableDeletion,
                            deleteGroup = { vm.deleteRideGridGroup(rideGridGroup) },
                            markAsPrimary = {
                                if (rideGridState == RideGridIntercomState.DISCOVERY || rideGridState == RideGridIntercomState.OFF) {
                                    vm.markRideGridGroupPrimary(rideGridGroup)
                                } else {
                                    toast.cancel()
                                    toast = Toast.makeText(context, "RIDEGRID group modification is not allowed in when RIDEGRID is connected!", Toast.LENGTH_SHORT)
                                    toast.show()
                                }
                            }
                        )
                    }
                    if (rideGridGroups.size < 5) {
                        RideGridCreateNewGroupCard(navigateToCreateNewRideGridGroup = {
                            if (rideGridState == RideGridIntercomState.DISCOVERY || rideGridState == RideGridIntercomState.OFF) {
                                if (rideGridState == RideGridIntercomState.OFF)
                                    vm.rideGridAction(RideGridIntercomAction.TurnOn)
                                vm.navigateToCreateNewRideGridGroup(navController)
                            } else {
                                toast.cancel()
                                toast = Toast.makeText(context, "Can not create new RIDEGRID group when RIDEGRID is connected!", Toast.LENGTH_SHORT)
                                toast.show()
                            }


                        }, modifier = Modifier.padding(start = if (rideGridGroups.isEmpty()) 16.dp else 0.dp, end = 16.dp))
                    }
                }


                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp, vertical = 12.dp),
                    text = "Active Devices".uppercase(),
                    style = MaterialTheme.typography.subtitle2,
                    color = Color.White,
                    fontWeight = FontWeight.SemiBold
                )

                Column(modifier = Modifier.padding(16.dp)) {
                    rideGridRiders.forEach {
                        Spacer(modifier = Modifier.height(5.dp))
                        RideGridUserListItem(it, false)
                    }
                }

            }
        }

    }

}


@OptIn(ExperimentalMaterialApi::class)
@Composable
fun RideGridGroupCard(
    groupName: String,
    isPrimary: Boolean,
    backgroundColorTint: Color,
    modifier: Modifier = Modifier,
    navigateToRideGrid: () -> Unit,
    deleteGroup: () -> Unit,
    markAsPrimary: () -> Unit,
    enableDeletion: Boolean
) {
    Box(
        contentAlignment = Alignment.TopCenter, modifier = modifier.width(104.dp)
    ) {
        Card(
            elevation = 0.dp,
            shape = RoundedCornerShape(10.dp),
            modifier = Modifier
                .height(84.dp)
                .fillMaxWidth()
                .padding(top = 8.dp, bottom = 8.dp)
                .padding(horizontal = 8.dp),
            backgroundColor = backgroundColorTint,
//        onClick = navigateToRideGrid,
        ) {

            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                Text(
                    text = (groupName.firstOrNull() ?: "").toString().uppercase(),
                    style = MaterialTheme.typography.h5,
                    color = Color.White,
                    fontWeight = FontWeight.Black,
                    modifier = Modifier.alpha(0.5f)
                )

                Spacer(modifier = Modifier.height(0.dp))
                Text(
                    text = groupName,
                    style = MaterialTheme.typography.subtitle2,
                    color = Color.White,
                    fontWeight = FontWeight.Normal,
                    modifier = Modifier.padding(horizontal = 4.dp)
                )
            }
        }

        if (isPrimary && !enableDeletion)
            Text(
                text = "PRIMARY",
                style = MaterialTheme.typography.overline,
                fontWeight = FontWeight.SemiBold,
                color = ColorShark,
                modifier = Modifier
                    .background(
                        color = Color.White,
                        shape = RoundedCornerShape(50)
                    )
                    .padding(vertical = 3.dp, horizontal = 6.dp),
                fontSize = 9.sp,
                letterSpacing = 1.sp

            )

        if (enableDeletion) {
            Row(
                horizontalArrangement = Arrangement.End,
                modifier = Modifier.fillMaxWidth()
            ) {
                Image(
                    imageVector = ImageVector.vectorResource(id = R.drawable.ic_cancel),
                    contentDescription = null,
                    modifier = Modifier
                        .clickable {
                            deleteGroup()
                        }
                        .width(26.dp)
                        .height(26.dp)
                )
            }


            if (isPrimary)
                Text(
                    text = "Primary",
                    style = MaterialTheme.typography.overline,
                    fontWeight = FontWeight.SemiBold,
                    color = ColorShark,
                    modifier = Modifier
                        .background(
                            color = Color.White,
                            shape = RoundedCornerShape(50)
                        )
                        .padding(vertical = 3.dp, horizontal = 6.dp)
                        .align(alignment = Alignment.BottomCenter),
                    letterSpacing = 1.sp
                )
            else
                Text(
                    text = "Set Primary",
                    style = MaterialTheme.typography.overline,
                    fontWeight = FontWeight.SemiBold,
                    color = ColorShark,
                    modifier = Modifier
                        .background(
                            color = Color.White,
                            shape = RoundedCornerShape(50)
                        )
                        .padding(vertical = 3.dp, horizontal = 6.dp)
                        .align(alignment = Alignment.BottomCenter)
                        .clickable(onClick = markAsPrimary),
                    letterSpacing = 1.sp
                )

        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun RideGridCreateNewGroupCard(modifier: Modifier = Modifier, navigateToCreateNewRideGridGroup: () -> Unit) {
    Card(
        elevation = 0.dp,
        shape = RoundedCornerShape(10.dp),
        modifier = modifier
            .height(84.dp)
            .wrapContentWidth()
            .padding(top = 6.dp),
        backgroundColor = Color(0xff2E2E36),
        onClick = navigateToCreateNewRideGridGroup
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
            modifier = Modifier.padding(horizontal = 12.dp)
        ) {
            Icon(
                imageVector = ImageVector.vectorResource(id = R.drawable.ic_add),
                contentDescription = "Add group",
                modifier = Modifier
                    .width(20.dp)
                    .height(20.dp)
                    .alpha(0.5f),
                tint = Color.White
            )
            Spacer(modifier = Modifier.height(8.dp))
            Text(
                text = "New".uppercase(),
                style = MaterialTheme.typography.subtitle2,
                color = Color.White,
                fontWeight = FontWeight.Normal
            )

            Text(
                text = "Group".uppercase(),
                style = MaterialTheme.typography.caption,
                color = Color.White,
                fontWeight = FontWeight.SemiBold,
                modifier = Modifier.alpha(0.6f)
            )
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Preview(showBackground = true)
@Composable
fun PreviewRideGridGroupView() {
    RideGridGroupView(rememberNavController(), object : RideGridGroupViewModelInterface {
        override val rideGridActiveClusterRiders: LiveData<List<RideGridUserDevice>> = MutableLiveData()
        override val rideGridGroups: LiveData<List<RideGridGroup>> = MutableLiveData()
        override val enableGroupDeletion: StateFlow<Boolean>
            get() = MutableStateFlow(false)

        override fun navigateToRideGridDashboard(navController: NavController, rideGridGroup: RideGridGroup) {

        }

        override fun navigateToCreateNewRideGridGroup(navController: NavController) {
        }

        override fun navigateToRideGridInviteRiders(navController: NavController, groupId: String) {
            TODO("Not yet implemented")
        }

        override fun deleteRideGridGroup(rideGridGroup: RideGridGroup) {
        }

        override fun markRideGridGroupPrimary(rideGridGroup: RideGridGroup) {

        }

        override fun rideGridAction(rideGridIntercomAction: RideGridIntercomAction) {
        }

        override fun toggleGroupDeletion(enable: Boolean) {
        }

        override val deviceInfo: LiveData<DeviceInfo> = MutableLiveData()
        override val batteryLevel: LiveData<Int> = MutableLiveData()
        override val rideGridIntercomState: LiveData<RideGridIntercomState>
            get() = MutableLiveData()
    })
}