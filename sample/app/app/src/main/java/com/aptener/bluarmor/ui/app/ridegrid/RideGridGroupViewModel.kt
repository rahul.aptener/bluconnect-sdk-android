package com.aptener.bluarmor.ui.app.ridegrid

import androidx.lifecycle.*
import androidx.navigation.NavController
import com.aptener.bluarmor.baseutils.asMutableLiveData
import com.aptener.bluarmor.service.bluconnect.model.*
import com.aptener.bluarmor.ui.app.dashboard.DashboardViewDestination
import com.aptener.bluarmor.ui.util.asMutableStateFlow
import com.aptener.bluarmor.ui.util.navigate
import com.aptener.bluconnect.repo.RideGridGroupInvitation
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

@AssistedFactory
interface RideGridGroupViewModelAssistedFactory {
    fun create(bluArmorInterface: BluArmorInterface): RideGridGroupViewModel
}

class RideGridGroupViewModel @AssistedInject constructor(@Assisted private val bluArmorInterface: BluArmorInterface) : ViewModel(),
    RideGridGroupViewModelInterface, RideGridCreateGroupViewModelInterface {
    override val deviceInfo: LiveData<DeviceInfo> = bluArmorInterface.bluConnectManager.deviceInfo
    override val batteryLevel: LiveData<Int> = bluArmorInterface.bluConnectManager.batteryLevel

    override val rideGridIntercomState: LiveData<RideGridIntercomState> = bluArmorInterface.bluConnectManager.rideGridState
    override val rideGridActiveClusterRiders: LiveData<List<RideGridUserDevice>> = bluArmorInterface.bluConnectManager.rideGridRiders

    override val rideGridGroups: LiveData<List<RideGridGroup>>
        get() = bluArmorInterface.bluConnectManager.rideGridGroups

    override val rideGridScanResultStateFlow: StateFlow<RideGridGroupInvitation?> = MutableStateFlow(null)

    override val newGroupName: LiveData<String> = MutableLiveData()
    override var newGroupId: Long = -1L

    override val setPrimaryGroup: LiveData<Boolean> = MutableLiveData()

    override val enableGroupDeletion: StateFlow<Boolean> = MutableStateFlow(false)

    override fun navigateToRideGridDashboard(navController: NavController, rideGridGroup: RideGridGroup) =
        navController.navigate(DashboardViewDestination.RideGridDashboardDest.route + "/${rideGridGroup.groupId}")

    override fun navigateToCreateNewRideGridGroup(navController: NavController) =
        navController.navigate(DashboardViewDestination.RideGridCreateGroupDest)

    override fun navigateToRideGridInviteRiders(navController: NavController, groupId: String) =
        navController.navigate(DashboardViewDestination.RideGridInviteRidersDest.route + "/${groupId}")

    override fun rideGridAction(rideGridIntercomAction: RideGridIntercomAction) =
        bluArmorInterface.bluConnectManager.rideGridAction(rideGridIntercomAction)

    override fun deleteRideGridGroup(rideGridGroup: RideGridGroup) {
        if (rideGridGroup.isPrimary)
            bluArmorInterface.bluConnectManager.device?.rideGrid?.unsetRideGridGroup()
        viewModelScope.launch { bluArmorInterface.bluConnectManager.deleteRideGridGroup(rideGridGroup) }
//        viewModelScope.launch { bluArmorInterface.bluConnectManager.buddyListDataSource.deleteBuddyGroup(rideGridGroup.id) }
        toggleGroupDeletion(false)
    }

    override fun markRideGridGroupPrimary(rideGridGroup: RideGridGroup) {
        viewModelScope.launch {
            bluArmorInterface.bluConnectManager.markRideGridGroupPrimary(rideGridGroup)
            /*     bluArmorInterface.bluConnectManager.buddyListDataSource.markBuddyListAsPrimary(
                     BuddyList(
                         rideGridGroup.id,
                         rideGridGroup.groupId,
                         rideGridGroup.name,
                         rideGridGroup.isPrimary,
                         rideGridGroup.firstCreated
                     )
                 )*/
        }

//        bluArmorInterface.bluConnectManager.device?.rideGrid?.setActiveGridGroupId(rideGridGroup.groupId)
    }

    override fun onGroupNameUpdate(string: String) {
        if (string.length <= 15)
            newGroupName.asMutableLiveData()?.value = string
    }

    override fun newRideGridGroupFound(rideGridGroupInvitation: RideGridGroupInvitation) {
        rideGridScanResultStateFlow.asMutableStateFlow()?.tryEmit(rideGridGroupInvitation)
    }

    override fun ignoreRecentRideGridGroupInvite() {
        rideGridScanResultStateFlow.asMutableStateFlow()?.tryEmit(null)
    }

    override fun onPrimaryGroupSwitchToggle(enabled: Boolean) {
        setPrimaryGroup.asMutableLiveData()?.value = enabled
    }

    override fun joinRideGridGroup(rideGridGroupInvitation: RideGridGroupInvitation,isPrimary: Boolean, navController: NavController) {
        viewModelScope.launch {
//            Timber.e("CREATE GROUP: ${rideGridGroupInvitation.json()}")
            val rideGridGroup = bluArmorInterface.bluConnectManager.joinRideGridGroup(rideGridGroupInvitation, isPrimary)

            withContext(Dispatchers.Main){
               navigateToRideGridInviteRiders(navController, rideGridGroupInvitation.ridegridGroup.groupId)
           }
            // Can be taken care from SDK
            /* if (setPrimaryGroup.value == true) {
                 bluArmorInterface.bluConnectManager.device?.rideGrid?.setActiveGridGroupId(rideGridGroupInvitation.ridegridGroup.groupId)
             }*/
        }
    }

    override fun createRideGridGroup(name: String, isPrimary: Boolean, navController: NavController) {
        viewModelScope.launch {
            val rideGridGroup = bluArmorInterface.bluConnectManager.createRideGridGroup(name, isPrimary) ?: return@launch
            if (rideGridGroup.isPrimary)
                bluArmorInterface.bluConnectManager.device?.rideGrid?.setActiveGridGroupId(rideGridGroup.groupId)

            navigateToRideGridInviteRiders(navController, rideGridGroup.groupId)

            // Can be taken care from SDK
            /*if (setPrimaryGroup.value == true) {
                bluArmorInterface.bluConnectManager.device?.rideGrid?.setActiveGridGroupId(rideGridGroup.groupId)
            }*/
        }
    }

/*
    override fun createRideGridGroup(createFromInvite: Boolean, navController: NavController) {
        viewModelScope.launch {
            val buddyList = if (createFromInvite) {
                val rideGridGroup = rideGridScanResultStateFlow.value ?: return@launch
                val id = bluArmorInterface.bluConnectManager.buddyListDataSource.getPossibleBuddyListId() + 1
                BuddyList(
                    id.toInt(),
                    rideGridGroup.groupId,
                    rideGridGroup.name,
                    setPrimaryGroup.value ?: false,
                    System.currentTimeMillis()
                )
            } else {
                val groupId = bluArmorInterface.bluConnectManager.buddyListDataSource.generateBuddyListId()
                val id = bluArmorInterface.bluConnectManager.buddyListDataSource.getPossibleBuddyListId() + 1

                BuddyList(
                    id.toInt(),
                    groupId,
                    newGroupName.value ?: "",
                    setPrimaryGroup.value ?: false,
                    System.currentTimeMillis()
                )
            }

            bluArmorInterface.bluConnectManager.buddyListDataSource.addBuddyGroup(buddyList)
            navigateToRideGridInviteRiders(navController, buddyList.id)

            if (setPrimaryGroup.value == true) {
                bluArmorInterface.bluConnectManager.device?.rideGrid?.setActiveGridGroupId(buddyList.groupId)
            }
//            bluArmorInterface.bluConnectManager.buddyListDataSource.updateBuddy(buddyList)
        }
    }
*/

    override fun toggleGroupDeletion(enable: Boolean) {
        enableGroupDeletion.asMutableStateFlow()?.tryEmit(enable)
    }

    class Factory(
        private val assistedFactory: RideGridGroupViewModelAssistedFactory,
        private val bluArmorInterface: BluArmorInterface,
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return assistedFactory.create(bluArmorInterface) as T
        }
    }
}
