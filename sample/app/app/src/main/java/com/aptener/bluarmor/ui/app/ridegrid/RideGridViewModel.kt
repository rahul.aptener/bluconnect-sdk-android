package com.aptener.bluarmor.ui.app.ridegrid

import androidx.lifecycle.*
import androidx.navigation.NavController
import com.aptener.bluarmor.baseutils.asMutableLiveData
import com.aptener.bluarmor.service.bluconnect.model.*
import com.aptener.bluarmor.ui.app.dashboard.DashboardViewDestination
import com.aptener.bluarmor.ui.util.asMutableStateFlow
import com.aptener.bluconnect.device.sxcontrol.Battery
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.*
import timber.log.Timber

@AssistedFactory
interface RideGridViewModelAssistedFactory {
    fun create(
        bluArmorInterface: BluArmorInterface,
        activeGroupId: String,
    ): RideGridViewModel
}

interface RideGridViewModelInterface {
    val rideGridIntercomState: LiveData<RideGridIntercomState>
    val deviceInfo: LiveData<DeviceInfo>
    val batteryLevel: LiveData<Int>
    val chargingState: LiveData<Battery.ChargingState>
    val musicState: LiveData<MusicState>
    val phoneCallState: LiveData<PhoneCallState>

    val rideGridRidersRadarMapLiveData: LiveData<MutableMap<String, RideGridUserDevice>>
    val rideGridRiders: LiveData<List<RideGridUserDevice>>

    val rideGridRidersRadarMapStateFlow: StateFlow<MutableMap<String, RideGridUserDevice>>
    val rideGridRidersStateFlow: StateFlow<List<RideGridUserDevice>>
    val rideGridStateFlow: StateFlow<RideGridIntercomState>

    fun musicAction(musicAction: MusicAction)
    fun phoneCallAction(phoneCallAction: PhoneCallAction)
    fun rideGridAction(rideGridIntercomAction: RideGridIntercomAction)
    fun navigateToRideGridInviteRiders(navController: NavController)
}

class RideGridViewModel @AssistedInject constructor(
    @Assisted private val bluArmorInterface: BluArmorInterface,
    @Assisted private val activeGroupId: String,
) : ViewModel(),
    RideGridViewModelInterface {
    override val deviceInfo: LiveData<DeviceInfo> = bluArmorInterface.bluConnectManager.deviceInfo
    override val batteryLevel: LiveData<Int> = bluArmorInterface.bluConnectManager.batteryLevel
    override val chargingState: LiveData<Battery.ChargingState> = bluArmorInterface.bluConnectManager.chargingState
    override val musicState: LiveData<MusicState> = bluArmorInterface.bluConnectManager.musicState
    override val phoneCallState: LiveData<PhoneCallState> = bluArmorInterface.bluConnectManager.phoneCallState
    private val rideGridRidersRadarMap: MutableMap<String, RideGridUserDevice> = mutableMapOf()

    private val rideGridRidersRadarMapNodeMap: MutableMap<Int, String> = mutableMapOf()
    override val rideGridRidersRadarMapLiveData: LiveData<MutableMap<String, RideGridUserDevice>> = MutableLiveData()

    override val rideGridRidersRadarMapStateFlow: StateFlow<MutableMap<String, RideGridUserDevice>> = MutableStateFlow(mutableMapOf())
    override val rideGridRidersStateFlow: StateFlow<List<RideGridUserDevice>> = bluArmorInterface.bluConnectManager.rideGridRidersFlow
    override val rideGridStateFlow: StateFlow<RideGridIntercomState> = bluArmorInterface.bluConnectManager.rideGridStateFlow

    override val rideGridRiders: LiveData<List<RideGridUserDevice>> = (bluArmorInterface.bluConnectManager.rideGridRiders).map { list ->
        Timber.e("TEST 09:")

        for (rDevice in list.sortedByDescending { it.distance }.subList(0, minOf(list.size, 5))) {
            when {

                rDevice.bearing == 0 && rDevice.distance <= 100 -> {
                    val key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]
                    if (key != null) {
                        rideGridRidersRadarMap.remove(key)
                    }

                    when {
                        !rideGridRidersRadarMap.containsKey("T1") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T1"
                            rideGridRidersRadarMap["T1"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("T2") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T2"
                            rideGridRidersRadarMap["T2"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("T5") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T5"
                            rideGridRidersRadarMap["T5"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("T8") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T8"
                            rideGridRidersRadarMap["T8"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("T9") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T9"
                            rideGridRidersRadarMap["T9"] = rDevice
                        }
                    }
                }

                rDevice.bearing == 0 && rDevice.distance <= 200 -> {
                    val key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]
                    if (key != null) {
                        rideGridRidersRadarMap.remove(key)
                    }

                    when {
                        !rideGridRidersRadarMap.containsKey("T8") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T8"
                            rideGridRidersRadarMap["T8"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("T9") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T9"
                            rideGridRidersRadarMap["T9"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("T10") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T10"
                            rideGridRidersRadarMap["T10"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("T11") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T11"
                            rideGridRidersRadarMap["T11"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("T12") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T12"
                            rideGridRidersRadarMap["T12"] = rDevice
                        }
                    }
                }

                rDevice.bearing == 0 && rDevice.distance > 200 -> {
                    val key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]
                    if (key != null) {
                        rideGridRidersRadarMap.remove(key)
                    }

                    when {
                        !rideGridRidersRadarMap.containsKey("T6") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T6"
                            rideGridRidersRadarMap["T6"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("T7") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T7"
                            rideGridRidersRadarMap["T7"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("T8") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T8"
                            rideGridRidersRadarMap["T8"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("T9") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T9"
                            rideGridRidersRadarMap["T9"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("T10") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T10"
                            rideGridRidersRadarMap["T10"] = rDevice
                        }
                    }
                }

                rDevice.bearing == 45 -> {
                    val key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]
                    if (key != null) {
                        rideGridRidersRadarMap.remove(key)
                    }

                    when {
                        !rideGridRidersRadarMap.containsKey("T3") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T3"
                            rideGridRidersRadarMap["T3"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("T13") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T13"
                            rideGridRidersRadarMap["T13"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("T6") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T6"
                            rideGridRidersRadarMap["T6"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("T8") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T8"
                            rideGridRidersRadarMap["T8"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("T11") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T11"
                            rideGridRidersRadarMap["T11"] = rDevice
                        }
                    }
                }

                rDevice.bearing == 90 -> {
                    val key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]
                    if (key != null) {
                        rideGridRidersRadarMap.remove(key)
                    }

                    when {
                        !rideGridRidersRadarMap.containsKey("R1") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R1"
                            rideGridRidersRadarMap["R1"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("R2") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R2"
                            rideGridRidersRadarMap["R2"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("R3") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R3"
                            rideGridRidersRadarMap["R3"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("R9") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R9"
                            rideGridRidersRadarMap["R9"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("R10") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R10"
                            rideGridRidersRadarMap["R10"] = rDevice
                        }
                    }
                }

                rDevice.bearing == 135 -> {
                    val key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]
                    if (key != null) {
                        rideGridRidersRadarMap.remove(key)
                    }

                    when {
                        !rideGridRidersRadarMap.containsKey("B3") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B3"
                            rideGridRidersRadarMap["B3"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("B13") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B13"
                            rideGridRidersRadarMap["B13"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("B6") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B6"
                            rideGridRidersRadarMap["B6"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("B8") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B8"
                            rideGridRidersRadarMap["B8"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("B11") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B11"
                            rideGridRidersRadarMap["B11"] = rDevice
                        }
                    }
                }

                rDevice.bearing == 180 && rDevice.distance <= 100 -> {
                    val key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]
                    if (key != null) {
                        rideGridRidersRadarMap.remove(key)
                    }

                    when {
                        !rideGridRidersRadarMap.containsKey("B1") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B1"
                            rideGridRidersRadarMap["B1"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("B2") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B2"
                            rideGridRidersRadarMap["B2"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("B5") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B5"
                            rideGridRidersRadarMap["B5"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("B8") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B8"
                            rideGridRidersRadarMap["B8"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("B9") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B9"
                            rideGridRidersRadarMap["B9"] = rDevice
                        }
                    }
                }

                rDevice.bearing == 180 && rDevice.distance <= 200 -> {
                    val key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]
                    if (key != null) {
                        rideGridRidersRadarMap.remove(key)
                    }

                    when {
                        !rideGridRidersRadarMap.containsKey("B8") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B8"
                            rideGridRidersRadarMap["B8"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("B9") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B9"
                            rideGridRidersRadarMap["B9"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("B10") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B10"
                            rideGridRidersRadarMap["B10"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("B11") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B11"
                            rideGridRidersRadarMap["B11"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("B12") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B12"
                            rideGridRidersRadarMap["B12"] = rDevice
                        }
                    }
                }

                rDevice.bearing == 180 && rDevice.distance > 200 -> {
                    val key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]
                    if (key != null) {
                        rideGridRidersRadarMap.remove(key)
                    }

                    when {
                        !rideGridRidersRadarMap.containsKey("B6") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B6"
                            rideGridRidersRadarMap["B6"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("B7") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B7"
                            rideGridRidersRadarMap["B7"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("B8") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B8"
                            rideGridRidersRadarMap["B8"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("B9") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B9"
                            rideGridRidersRadarMap["B9"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("B10") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B10"
                            rideGridRidersRadarMap["B10"] = rDevice
                        }
                    }
                }

                rDevice.bearing == -135 -> {
                    val key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]
                    if (key != null) {
                        rideGridRidersRadarMap.remove(key)
                    }

                    when {
                        !rideGridRidersRadarMap.containsKey("B4") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B4"
                            rideGridRidersRadarMap["B4"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("B14") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B14"
                            rideGridRidersRadarMap["B14"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("B7") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B7"
                            rideGridRidersRadarMap["B7"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("B9") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B9"
                            rideGridRidersRadarMap["B9"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("B12") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B12"
                            rideGridRidersRadarMap["B12"] = rDevice
                        }
                    }
                }

                rDevice.bearing == -90 -> {
                    val key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]
                    if (key != null) {
                        rideGridRidersRadarMap.remove(key)
                    }

                    when {
                        !rideGridRidersRadarMap.containsKey("R4") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R4"
                            rideGridRidersRadarMap["R4"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("R5") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R5"
                            rideGridRidersRadarMap["R5"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("R6") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R6"
                            rideGridRidersRadarMap["R6"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("R7") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R7"
                            rideGridRidersRadarMap["R7"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("R8") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R8"
                            rideGridRidersRadarMap["R8"] = rDevice
                        }
                    }
                }

                rDevice.bearing == -45 -> {
                    val key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]
                    if (key != null) {
                        rideGridRidersRadarMap.remove(key)
                    }

                    when {
                        !rideGridRidersRadarMap.containsKey("T4") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T4"
                            rideGridRidersRadarMap["T4"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("T14") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T14"
                            rideGridRidersRadarMap["T14"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("T7") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T7"
                            rideGridRidersRadarMap["T7"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("T9") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T9"
                            rideGridRidersRadarMap["T9"] = rDevice
                        }
                        !rideGridRidersRadarMap.containsKey("T12") -> {
                            rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T12"
                            rideGridRidersRadarMap["T12"] = rDevice
                        }
                    }
                }
            }
        }


        val filteredList = rideGridRidersRadarMapNodeMap.filter { map ->
            list.indexOfFirst { it.nodeId == map.key } < 0
        }

        filteredList.forEach {
            rideGridRidersRadarMapNodeMap.remove(it.key)
            rideGridRidersRadarMap.remove(it.value)
        }

//        Timber.e("TEST 09: ${rideGridRidersRadarMap.json()}")

        rideGridRidersRadarMapLiveData.asMutableLiveData()?.value = rideGridRidersRadarMap

        list
    }
    override val rideGridIntercomState: LiveData<RideGridIntercomState> = bluArmorInterface.bluConnectManager.rideGridState
    override fun musicAction(musicAction: MusicAction) = bluArmorInterface.bluConnectManager.musicAction(musicAction)

    override fun phoneCallAction(phoneCallAction: PhoneCallAction) = bluArmorInterface.bluConnectManager.phoneCallAction(phoneCallAction)

    override fun rideGridAction(rideGridIntercomAction: RideGridIntercomAction) =
        bluArmorInterface.bluConnectManager.rideGridAction(rideGridIntercomAction)

    override fun navigateToRideGridInviteRiders(navController: NavController) =
        navController.navigate(DashboardViewDestination.RideGridInviteRidersDest.route + "/${activeGroupId}")


    init {
        this.rideGridRidersStateFlow
            .onEach { list ->
                for (rDevice in list.sortedByDescending { it.distance }.subList(0, minOf(list.size, 5))) {
                    when {
                        rDevice.bearing == 0 && rDevice.distance <= 100 -> {
                            val key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]
                            if (key != null) {
                                rideGridRidersRadarMap.remove(key)
                            }

                            when {
                                !rideGridRidersRadarMap.containsKey("T1") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T1"
                                    rideGridRidersRadarMap["T1"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("T2") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T2"
                                    rideGridRidersRadarMap["T2"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("T5") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T5"
                                    rideGridRidersRadarMap["T5"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("T8") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T8"
                                    rideGridRidersRadarMap["T8"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("T9") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T9"
                                    rideGridRidersRadarMap["T9"] = rDevice
                                }
                            }
                        }

                        rDevice.bearing == 0 && rDevice.distance <= 200 -> {
                            val key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]
                            if (key != null) {
                                rideGridRidersRadarMap.remove(key)
                            }

                            when {
                                !rideGridRidersRadarMap.containsKey("T8") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T8"
                                    rideGridRidersRadarMap["T8"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("T9") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T9"
                                    rideGridRidersRadarMap["T9"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("T10") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T10"
                                    rideGridRidersRadarMap["T10"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("T11") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T11"
                                    rideGridRidersRadarMap["T11"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("T12") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T12"
                                    rideGridRidersRadarMap["T12"] = rDevice
                                }
                            }
                        }

                        rDevice.bearing == 0 && rDevice.distance > 200 -> {
                            val key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]
                            if (key != null) {
                                rideGridRidersRadarMap.remove(key)
                            }

                            when {
                                !rideGridRidersRadarMap.containsKey("T6") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T6"
                                    rideGridRidersRadarMap["T6"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("T7") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T7"
                                    rideGridRidersRadarMap["T7"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("T8") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T8"
                                    rideGridRidersRadarMap["T8"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("T9") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T9"
                                    rideGridRidersRadarMap["T9"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("T10") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T10"
                                    rideGridRidersRadarMap["T10"] = rDevice
                                }
                            }
                        }


                        rDevice.bearing == 45 -> {
                            val key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]
                            if (key != null) {
                                rideGridRidersRadarMap.remove(key)
                            }

                            when {
                                !rideGridRidersRadarMap.containsKey("T3") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T3"
                                    rideGridRidersRadarMap["T3"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("T13") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T13"
                                    rideGridRidersRadarMap["T13"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("T6") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T6"
                                    rideGridRidersRadarMap["T6"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("T8") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T8"
                                    rideGridRidersRadarMap["T8"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("T11") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T11"
                                    rideGridRidersRadarMap["T11"] = rDevice
                                }
                            }
                        }

                        rDevice.bearing == 90 -> {
                            val key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]
                            if (key != null) {
                                rideGridRidersRadarMap.remove(key)
                            }

                            when {
                                !rideGridRidersRadarMap.containsKey("R1") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R1"
                                    rideGridRidersRadarMap["R1"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("R2") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R2"
                                    rideGridRidersRadarMap["R2"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("R3") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R3"
                                    rideGridRidersRadarMap["R3"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("R9") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R9"
                                    rideGridRidersRadarMap["R9"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("R10") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R10"
                                    rideGridRidersRadarMap["R10"] = rDevice
                                }
                            }
                        }

                        rDevice.bearing == 135 -> {
                            val key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]
                            if (key != null) {
                                rideGridRidersRadarMap.remove(key)
                            }

                            when {
                                !rideGridRidersRadarMap.containsKey("B3") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B3"
                                    rideGridRidersRadarMap["B3"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("B13") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B13"
                                    rideGridRidersRadarMap["B13"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("B6") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B6"
                                    rideGridRidersRadarMap["B6"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("B8") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B8"
                                    rideGridRidersRadarMap["B8"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("B11") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B11"
                                    rideGridRidersRadarMap["B11"] = rDevice
                                }
                            }
                        }

                        rDevice.bearing == 180 && rDevice.distance <= 100 -> {
                            val key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]
                            if (key != null) {
                                rideGridRidersRadarMap.remove(key)
                            }

                            when {
                                !rideGridRidersRadarMap.containsKey("B1") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B1"
                                    rideGridRidersRadarMap["B1"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("B2") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B2"
                                    rideGridRidersRadarMap["B2"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("B5") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B5"
                                    rideGridRidersRadarMap["B5"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("B8") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B8"
                                    rideGridRidersRadarMap["B8"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("B9") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B9"
                                    rideGridRidersRadarMap["B9"] = rDevice
                                }
                            }
                        }

                        rDevice.bearing == 180 && rDevice.distance <= 200 -> {
                            val key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]
                            if (key != null) {
                                rideGridRidersRadarMap.remove(key)
                            }

                            when {
                                !rideGridRidersRadarMap.containsKey("B8") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B8"
                                    rideGridRidersRadarMap["B8"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("B9") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B9"
                                    rideGridRidersRadarMap["B9"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("B10") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B10"
                                    rideGridRidersRadarMap["B10"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("B11") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B11"
                                    rideGridRidersRadarMap["B11"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("B12") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B12"
                                    rideGridRidersRadarMap["B12"] = rDevice
                                }
                            }
                        }

                        rDevice.bearing == 180 && rDevice.distance > 200 -> {
                            val key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]
                            if (key != null) {
                                rideGridRidersRadarMap.remove(key)
                            }

                            when {
                                !rideGridRidersRadarMap.containsKey("B6") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B6"
                                    rideGridRidersRadarMap["B6"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("B7") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B7"
                                    rideGridRidersRadarMap["B7"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("B8") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B8"
                                    rideGridRidersRadarMap["B8"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("B9") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B9"
                                    rideGridRidersRadarMap["B9"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("B10") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B10"
                                    rideGridRidersRadarMap["B10"] = rDevice
                                }
                            }
                        }

                        rDevice.bearing == -135 -> {
                            val key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]
                            if (key != null) {
                                rideGridRidersRadarMap.remove(key)
                            }

                            when {
                                !rideGridRidersRadarMap.containsKey("B4") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B4"
                                    rideGridRidersRadarMap["B4"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("B14") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B14"
                                    rideGridRidersRadarMap["B14"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("B7") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B7"
                                    rideGridRidersRadarMap["B7"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("B9") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B9"
                                    rideGridRidersRadarMap["B9"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("B12") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B12"
                                    rideGridRidersRadarMap["B12"] = rDevice
                                }
                            }
                        }

                        rDevice.bearing == -90 -> {
                            val key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]
                            if (key != null) {
                                rideGridRidersRadarMap.remove(key)
                            }

                            when {
                                !rideGridRidersRadarMap.containsKey("R4") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R4"
                                    rideGridRidersRadarMap["R4"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("R5") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R5"
                                    rideGridRidersRadarMap["R5"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("R6") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R6"
                                    rideGridRidersRadarMap["R6"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("R7") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R7"
                                    rideGridRidersRadarMap["R7"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("R8") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R8"
                                    rideGridRidersRadarMap["R8"] = rDevice
                                }
                            }
                        }

                        rDevice.bearing == -45 -> {
                            val key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]
                            if (key != null) {
                                rideGridRidersRadarMap.remove(key)
                            }

                            when {
                                !rideGridRidersRadarMap.containsKey("T4") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T4"
                                    rideGridRidersRadarMap["T4"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("T14") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T14"
                                    rideGridRidersRadarMap["T14"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("T7") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T7"
                                    rideGridRidersRadarMap["T7"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("T9") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T9"
                                    rideGridRidersRadarMap["T9"] = rDevice
                                }
                                !rideGridRidersRadarMap.containsKey("T12") -> {
                                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T12"
                                    rideGridRidersRadarMap["T12"] = rDevice
                                }
                            }
                        }


                    }
                }


                val filteredList = rideGridRidersRadarMapNodeMap.filter { map ->
                    list.indexOfFirst { it.nodeId == map.key } < 0
                }

                filteredList.forEach {
                    rideGridRidersRadarMapNodeMap.remove(it.key)
                    rideGridRidersRadarMap.remove(it.value)
                }

                rideGridRidersRadarMapStateFlow.asMutableStateFlow()?.tryEmit(rideGridRidersRadarMap)
                list
            }.launchIn(viewModelScope)
        this.rideGridStateFlow.onEach {
            when(it){
                RideGridIntercomState.OFF,
                RideGridIntercomState.DISCOVERY -> {
                    rideGridRidersRadarMap.clear()
                    rideGridRidersRadarMapNodeMap.clear()
                    rideGridRidersRadarMapLiveData.asMutableLiveData()?.value = HashMap()
                    rideGridRidersRadarMapStateFlow.asMutableStateFlow()?.tryEmit(HashMap())
                }
                else -> Unit
            }
        }.launchIn(viewModelScope)

    }

    class Factory(
        private val assistedFactory: RideGridViewModelAssistedFactory,
        private val bluArmorInterface: BluArmorInterface,
        private val activeGroupId: String,
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return assistedFactory.create(bluArmorInterface, activeGroupId) as T
        }
    }
}