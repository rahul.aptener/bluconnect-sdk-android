package com.aptener.bluarmor.ui.app.ridegrid.inviteRiders

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.aptener.bluarmor.R
import com.aptener.bluarmor.service.bluconnect.model.RideGridUserDevice
import com.aptener.bluarmor.ui.app.ridegrid.EditText
import com.aptener.bluarmor.ui.theme.BluArmorTheme
import com.aptener.bluarmor.ui.theme.ButtonPrimary
import com.aptener.bluarmor.ui.util.loadPicture
import com.aptener.bluconnect.repo.Buddy
import com.aptener.bluconnect.repo.RideGridGroupInvitation
import com.journeyapps.barcodescanner.CaptureManager
import com.journeyapps.barcodescanner.CompoundBarcodeView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import timber.log.Timber


interface RideGridInviteRidersViewInterface {
    val groupName: LiveData<String>
    val groupId: LiveData<String>
    val rideGridActiveClusterRiders: LiveData<List<RideGridUserDevice>>
    fun inviteRider(rideGridUserDevice: RideGridUserDevice)
    fun getQrCode(context: Context): Flow<Bitmap>
}



@Composable
fun RideGridInviteRidersView(
    navController: NavController = rememberNavController(),
    vm: RideGridInviteRidersViewInterface
) {


    val groupName: String by vm.groupName.observeAsState(initial = " ")
    val rideGridActiveClusterRiders: List<RideGridUserDevice> by vm.rideGridActiveClusterRiders.observeAsState(initial = emptyList())

    val groupInviteQr: Bitmap? by vm.getQrCode(LocalContext.current).collectAsState(initial = null, Dispatchers.IO)
    val context = LocalContext.current

    BluArmorTheme {
        Scaffold(
            modifier = Modifier.fillMaxSize(),
            topBar = {
                TopAppBar(
                    backgroundColor = MaterialTheme.colors.background,
                    elevation = 0.dp,
                ) {
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.SpaceBetween,
                    ) {
                        Image(
                            imageVector = ImageVector.vectorResource(R.drawable.ic_back_nav),
                            contentDescription = null,
                            modifier = Modifier
                                .padding(0.dp)
                                .clickable {
                                    navController.popBackStack()
                                }
                        )
                    }
                }
            }
        ) {
            Column(
                modifier = Modifier
                    .padding(horizontal = 32.dp)
                    .fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                EditText(
                    label = "Group Name",
                    text = groupName,
                    onValueChange = {},
                    enabled = false
                )

                Card(
                    modifier = Modifier
                        .fillMaxWidth(0.8f)
                        .aspectRatio(1f)
                        .padding(top = 18.dp)
                        .background(
                            color = Color.White,
                            shape = RoundedCornerShape(8.dp)
                        )
                        .padding(vertical = 14.dp, horizontal = 8.dp),
                    backgroundColor = Color.White,
                    elevation = 0.dp
                ) {

                    Timber.e("TEST TIME: ${System.currentTimeMillis()}")

                    if (groupInviteQr != null)
                        Image(
                            bitmap = groupInviteQr!!.asImageBitmap(),
                            contentDescription = null
                        )

                    Timber.e("TEST TIME END: ${System.currentTimeMillis()}")

//                    QrCodeView(
//                        content = vm.groupName.value ?: " ",
//                    )
                }

                ButtonPrimary(
                    label = "Share".uppercase(),
                    onClick = {
                        /*val sendIntent: Intent = Intent().apply {
                            action = Intent.ACTION_SEND
                            putExtra(Intent.EXTRA_TEXT, "https://ridegrid.page.link/${(vm.groupId.value + "|" + vm.groupName.value).asciiToHex()}")
                            type = "text/plain"
                        }
                        val shareIntent = Intent.createChooser(sendIntent, null)
                        context.startActivity(shareIntent)*/
                    },
                    modifier = Modifier
                        .padding(vertical = 16.dp)
                        .fillMaxWidth(0.8f)
                )

                Text(
                    modifier = Modifier
                        .wrapContentWidth()
                        .padding(vertical = 4.dp, horizontal = 16.dp),
                    text = "Invite other riders via link",
                    style = MaterialTheme.typography.caption,
                    color = Color.White,
                    fontWeight = FontWeight.SemiBold
                )

                Spacer(modifier = Modifier.weight(0.1f))


                Column(modifier = Modifier.fillMaxWidth()) {
                    Text(
                        modifier = Modifier
                            .wrapContentWidth()
                            .padding(vertical = 4.dp, horizontal = 16.dp),
                        text = "Add buddies to the group",
                        style = MaterialTheme.typography.caption,
                        color = Color.White,
                        fontWeight = FontWeight.SemiBold
                    )

                    Spacer(modifier = Modifier.height(14.dp))

                    for (rider in rideGridActiveClusterRiders) {
                        BuddyRiderView(
                            riderName = rider.name,
                            onClick = {
                                vm.inviteRider(rider)
                            }
                        )
                        Spacer(modifier = Modifier.height(12.dp))
                    }
                }
            }
        }
    }
}

@Composable
fun BuddyRiderView(
    riderName: String,
    onClick: () -> Unit,
    position: Int? = null,
    positiveIcon: Boolean = true
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp)
            .padding(bottom = 12.dp),
        backgroundColor = Color(0x1AFFFFFF),
        elevation = 0.dp,
        shape = RoundedCornerShape(10.dp)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(6.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {

            val imageRes = loadPicture(R.drawable.ic_helmet).value

            if (imageRes != null)
                Icon(
                    bitmap = imageRes.asImageBitmap(),
                    contentDescription = null,
                    tint = Color.White,
                    modifier = Modifier
                        .clip(shape = CircleShape)
                        .padding(6.dp)
                        .width(24.dp)
                        .height(24.dp)
                        .padding(0.dp),
                )

            Text(
                modifier = Modifier
                    .padding(horizontal = 8.dp)
                    .weight(1f),
                text = riderName,
                style = MaterialTheme.typography.subtitle1,
                color = Color.White,
                fontWeight = FontWeight.SemiBold
            )



            if (position != null)
                Text(
                    modifier = Modifier
                        .padding(horizontal = 8.dp),
                    text = position.toString(),
                    style = MaterialTheme.typography.subtitle1,
                    color = Color.White,
                    fontWeight = FontWeight.SemiBold
                )

            val imageRes2 = loadPicture(
                if (positiveIcon)
                    R.drawable.ic_add
                else R.drawable.ic_remove
            ).value

            if (imageRes2 != null)
                Image(
                    bitmap = imageRes2.asImageBitmap(),
                    contentDescription = null,
                    modifier = Modifier
                        .clip(shape = CircleShape)
                        .clickable(onClick = onClick)
                        .padding(2.dp)
                        .width(40.dp)
                        .height(40.dp),
                )
        }

    }
}

@Preview
@Composable
fun Preview() {
    RideGridInviteRidersView(rememberNavController(), object : RideGridInviteRidersViewInterface {
        override val groupName: LiveData<String>
            get() = MutableLiveData()
        override val groupId: LiveData<String>
            get() = MutableLiveData()
        override val rideGridActiveClusterRiders: LiveData<List<RideGridUserDevice>>
            get() = MutableLiveData()

        override fun inviteRider(rideGridUserDevice: RideGridUserDevice) {

        }

        override fun getQrCode(context: Context): Flow<Bitmap> {
            TODO("Not yet implemented")
        }

    })
}