package com.aptener.bluarmor.ui.app.ridegrid.inviteRiders

import android.content.Context
import android.graphics.Bitmap
import androidx.core.content.ContextCompat
import androidx.lifecycle.*
import com.aptener.bluarmor.R
import com.aptener.bluarmor.baseutils.asMutableLiveData
import com.aptener.bluarmor.service.bluconnect.model.BluArmorInterface
import com.aptener.bluarmor.service.bluconnect.model.RideGridGroup
import com.aptener.bluarmor.service.bluconnect.model.RideGridUserDevice
import com.aptener.bluarmor.ui.util.getQrCodeBitmap
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.launch
import timber.log.Timber


@AssistedFactory
interface RideGridInviteRidersViewModelAssistedFactory {
    fun create(bluArmorInterface: BluArmorInterface, rideGridGroupId: String): RideGridInviteRidersViewModel
}

class RideGridInviteRidersViewModel @AssistedInject constructor(
    @Assisted private val bluArmorInterface: BluArmorInterface,
    @Assisted private val ridegridGroupId: String,
) : ViewModel(),
    RideGridInviteRidersViewInterface {

    override val groupId: LiveData<String> = MutableLiveData(ridegridGroupId)

    override val groupName: LiveData<String> = MutableLiveData()
    private var buddyList: RideGridGroup? = null
    override val rideGridActiveClusterRiders: LiveData<List<RideGridUserDevice>> = bluArmorInterface.bluConnectManager.rideGridRiders

    init {
        viewModelScope.launch {
            buddyList = bluArmorInterface.bluConnectManager.getRideGridGroupById(ridegridGroupId)
//            Timber.e("TEST 01: $ridegridGroupId ${buddyList?.json()}")
            groupName.asMutableLiveData()?.value = buddyList?.name ?: ""
        }
    }

    override fun inviteRider(rideGridUserDevice: RideGridUserDevice) {
//        with(groupName.value ?: return) {
//            bluArmorInterface.bluConnectManager.device?.rideGrid?.inviteRider(
//                buddyListId,
//                this,
//                RideGridDevice(
//                    rideGridUserDevice.nodeId,
//                    rideGridUserDevice.name,
//                    rideGridUserDevice.macAddress,
//                    0,
//                    0,
//                    false
//                )
//            )
//        }
    }

    override fun getQrCode(context: Context): Flow<Bitmap> {
        return callbackFlow {

            val rideGridGroup = bluArmorInterface.bluConnectManager.getRideGridGroupById(ridegridGroupId) ?: run {
                close()
                return@callbackFlow
            }

            val qrData = bluArmorInterface.bluConnectManager.encodeRideGridGroupData(rideGridGroup) ?: run {
                close()
                return@callbackFlow
            }

            val backgroundTint = ContextCompat.getColor(context, R.color.white)
            val mainTint = ContextCompat.getColor(context, R.color.black)

            send(
                getQrCodeBitmap(
                    qrTint = mainTint,
                    backgroundTint = backgroundTint,
                    content = qrData
                )
            )

            awaitClose { }
        }
    }

    class Factory(
        private val assistedFactory: RideGridInviteRidersViewModelAssistedFactory,
        private val bluArmorInterface: BluArmorInterface,
        private val rideGridGroupId: String,
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return assistedFactory.create(bluArmorInterface, rideGridGroupId) as T
        }
    }

}
