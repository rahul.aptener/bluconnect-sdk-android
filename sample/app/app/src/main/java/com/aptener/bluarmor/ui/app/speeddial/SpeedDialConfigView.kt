package com.aptener.bluarmor.ui.app.speeddial

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.aptener.bluarmor.R
import com.aptener.bluarmor.service.bluconnect.model.*
import com.aptener.bluarmor.service.bluconnect.BluConnectManager
import com.aptener.bluarmor.ui.app.ridegrid.inviteRiders.BuddyRiderView
import com.aptener.bluarmor.ui.theme.BluArmorTheme
import com.aptener.bluarmor.ui.theme.ButtonPrimary
import com.aptener.bluarmor.ui.theme.ButtonPrimaryHollow
import com.aptener.bluconnect.BluArmorModel
import com.aptener.bluconnect.repo.Buddy


@Composable
fun SpeedDialConfigView(navController: NavController = rememberNavController(), vm: SpeedDialConfigViewModel) {
    val buddyDevices: List<BuddyDevice> by vm.buddyDevices.observeAsState(listOf())
    val speedDialList: Array<Buddy> by vm.speedDialList.collectAsState()

    BluArmorTheme {
        Scaffold(
            topBar = {
                TopAppBar(
                    backgroundColor = MaterialTheme.colors.background,
                    elevation = 0.dp,
                ) {
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.SpaceBetween,
                    ) {
                        Image(
                            imageVector = ImageVector.vectorResource(R.drawable.ic_back_nav),
                            contentDescription = null,
                            modifier = Modifier
                                .padding(0.dp)
                                .clickable {
                                    navController.popBackStack()
                                }
                        )
                    }
                }
            }
        ) {
            Column(modifier = Modifier.fillMaxSize()) {
                Row(verticalAlignment = Alignment.Bottom, modifier = Modifier.padding(vertical = 12.dp, horizontal = 28.dp)) {
                    Text(
                        modifier = Modifier,
                        text = "Speed Dail List".uppercase(),
                        style = MaterialTheme.typography.subtitle2,
                        color = Color.White,
                        fontWeight = FontWeight.SemiBold
                    )
                    Spacer(modifier = Modifier.weight(1f))
                    Text(
                        modifier = Modifier,
                        text = "${speedDialList.size}/5".uppercase(),
                        style = MaterialTheme.typography.caption,
                        color = Color.White,
                        fontWeight = FontWeight.SemiBold,
                        textAlign = TextAlign.End
                    )

                }

                for ((i, v) in speedDialList.withIndex()) {
                    BuddyRiderView(v.name, { vm.removeBuddyDeviceToSpeedDial(v) }, i + 1, false)
                }

                Row(
                    modifier = Modifier
                        .padding(horizontal = 16.dp)
                        .fillMaxWidth()
                ) {

                    ButtonPrimaryHollow(
                        label = "Discard".uppercase(),
                        onClick = { navController.popBackStack() },
                        modifier = Modifier
                            .padding(vertical = 16.dp)
                    )

                    Spacer(modifier = Modifier.width(16.dp))

                    ButtonPrimary(
                        label = "Update List".uppercase(),
                        onClick = { vm.saveSpeedDial(navController) },
                        modifier = Modifier
                            .padding(vertical = 16.dp)
                            .weight(1f)
                    )
                }

                Spacer(modifier = Modifier.height(8.dp))
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 12.dp, horizontal = 28.dp),
                    text = "Available Buddies".uppercase(),
                    style = MaterialTheme.typography.subtitle2,
                    color = Color.White,
                    fontWeight = FontWeight.SemiBold
                )

                LazyColumn(modifier = Modifier.weight(1f)) {
                    itemsIndexed(
                        items = buddyDevices.filter { it.buddy?.model != null && it.buddy.model != BluArmorModel.NO_DEVICE  },
                        itemContent = { i, v ->
                            BuddyRiderView(v.name, { vm.addBuddyDeviceToSpeedDial(v.buddy!!) })
                        }
                    )
                }

            }
        }
    }
}