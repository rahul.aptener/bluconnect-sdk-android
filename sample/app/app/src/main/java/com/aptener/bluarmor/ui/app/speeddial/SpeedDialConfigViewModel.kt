package com.aptener.bluarmor.ui.app.speeddial

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import com.aptener.bluarmor.service.bluconnect.model.BluArmorInterface
import com.aptener.bluarmor.service.bluconnect.model.BuddyDevice
import com.aptener.bluarmor.ui.util.asMutableStateFlow
import com.aptener.bluconnect.repo.Buddy
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

@AssistedFactory
interface SpeedDialConfigViewModelAssistedFactory {
    fun create(bluArmorInterface: BluArmorInterface): SpeedDialConfigViewModel
}

class SpeedDialConfigViewModel @AssistedInject constructor(@Assisted private val bluArmorInterface: BluArmorInterface) :
    ViewModel() {

    val speedDialList: StateFlow<Array<Buddy>> =
        MutableStateFlow(bluArmorInterface.bluConnectManager.speedDialContacts.value?.filterNotNull()?.toTypedArray() ?: arrayOf())
    val buddyDevices: LiveData<List<BuddyDevice>> = bluArmorInterface.bluConnectManager.buddyRidersList

    fun addBuddyDeviceToSpeedDial(buddy: Buddy) {
        if (speedDialList.value.size >= 5)
            return

        val list: ArrayList<Buddy> = ArrayList(speedDialList.value.toList())

        val found = list.firstOrNull {
            it.macAddress.equals(buddy.macAddress, true)
        }

        if (found == null) {
            list.add(buddy)
            speedDialList.asMutableStateFlow()?.value = list.toTypedArray()
        }

    }

    fun removeBuddyDeviceToSpeedDial(buddy: Buddy) {
        val list: ArrayList<Buddy> = ArrayList()
        for ((i, s) in speedDialList.value.filter {
            !it.macAddress.equals(buddy.macAddress, true)
        }.withIndex()) {
            list.add(s)
        }

        speedDialList.asMutableStateFlow()?.value = list.toTypedArray()
    }

    fun saveSpeedDial(navController: NavController) {
        bluArmorInterface.bluConnectManager.updateSpeedDialList(speedDialList.value)
        navController.popBackStack()
    }


    class Factory(
        private val assistedFactory: SpeedDialConfigViewModelAssistedFactory,
        private val bluArmorInterface: BluArmorInterface
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return assistedFactory.create(bluArmorInterface) as T
        }
    }
}