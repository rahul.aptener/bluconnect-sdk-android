package com.aptener.bluarmor.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val Transparent = Color(0x00FFFFFF)


val ColorPositive = Color(0xFF4de77c)
val ColorNegative = Color(0xFFff4f64)
val ColorNegativeLight = Color(0xFFFFA3AE)
val ColorDarkShadow = Color(0xFF4E4E4E)
val ColorShark = Color(0xFF242428)
val ColorSharkDark = Color(0xFF242428)

val ColorTextPrimary = Color(0xFFFFFFFF)
val ColorTextPrimaryAccent = Color(0xB3FFFFFF)
val ColorTextSecondary = Color(0xFFFFFFFF)
val ColorTextSecondaryAccent = Color(0xFFFFFFFF)

