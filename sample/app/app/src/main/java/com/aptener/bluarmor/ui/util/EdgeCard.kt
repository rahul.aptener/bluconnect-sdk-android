package com.aptener.bluarmor.ui.util

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.aptener.bluarmor.ui.theme.BluArmorTheme
import com.aptener.bluarmor.ui.theme.ColorDarkShadow

@Composable
fun EdgeCard(
    modifier: Modifier = Modifier,
    bottomStroke: Dp = 3.dp,
    backgroundColor: Color = MaterialTheme.colors.surface,
    bottomStrokeColor: Color = ColorDarkShadow,
    cornerRadius: Dp = 10.dp,
    content: @Composable () -> Unit
) {
    BluArmorTheme {
        Card(
            modifier = modifier,
            backgroundColor = bottomStrokeColor,
            shape = RoundedCornerShape(cornerRadius)
        ) {
            Card(
                backgroundColor = backgroundColor,
                modifier = Modifier.padding(bottom = bottomStroke),
                shape = RoundedCornerShape(cornerRadius),
                content = content,
            )
        }
    }
}