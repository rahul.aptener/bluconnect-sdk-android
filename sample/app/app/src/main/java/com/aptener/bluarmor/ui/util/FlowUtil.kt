package com.aptener.bluarmor.ui.util

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.isActive


fun CoroutineScope.intervalFlow(
    timerTickMillis: Long,
    durationMillis: Long,
    onTick: () -> Unit,
    onFinished: () -> Unit,
) = this.async {
    var durationCounter = durationMillis
    if (durationMillis > 0)
        while (isActive) {
            onTick()
            durationCounter -= timerTickMillis
            if (durationCounter <= 0) {
                onFinished()
                break
            }
            delay(durationMillis)
        }
    else {
        onFinished()
    }
}


fun <T> StateFlow<T>.asMutableStateFlow(): MutableStateFlow<T>? {
    return if (this is MutableStateFlow<T>)
        this
    else null
}