package com.aptener.bluarmor.ui.util

import androidx.compose.runtime.Composable
import androidx.navigation.*
import androidx.navigation.compose.composable
import timber.log.Timber

interface NavControllerDestination {
    val route: String
}


fun NavController.navigate(navControllerDestination: NavControllerDestination) =
    if (this.currentDestination?.route != navControllerDestination.route)
        navigate(navControllerDestination.route)
            .also {
                Timber.e("Navigate to: ${navControllerDestination.route}")
            } else Unit


fun NavController.navigate(navControllerDestination: NavControllerDestination, builder: NavOptionsBuilder.() -> Unit) =
    if (this.currentDestination?.route != navControllerDestination.route)
        navigate(navControllerDestination.route, builder = builder).also {
            Timber.e("Navigate to: ${navControllerDestination.route}")
        } else Unit

fun NavHostController.navigate(navControllerDestination: NavControllerDestination, builder: NavOptionsBuilder.() -> Unit) =
    if (this.currentDestination?.route != navControllerDestination.route)
        navigate(navControllerDestination.route, builder = builder).also {
            Timber.e("Navigate to: ${navControllerDestination.route}")
        } else Unit

fun NavOptionsBuilder.popUpTo(navControllerDestination: NavControllerDestination, popUpToBuilder: PopUpToBuilder.() -> Unit = {}) =
    popUpTo(navControllerDestination.route, popUpToBuilder).also {
        Timber.e("PopUpTo to: ${navControllerDestination.route}")
    }

