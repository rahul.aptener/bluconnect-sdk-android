package com.aptener.bluarmor.ui.util

import android.graphics.Bitmap
import androidx.annotation.ColorInt
import androidx.compose.foundation.Image
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.platform.LocalContext
import androidx.core.content.ContextCompat
import com.aptener.bluarmor.R
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import com.google.zxing.qrcode.QRCodeWriter
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel
import java.util.*
import javax.annotation.Nonnull


@Throws(WriterException::class)
private fun createBitMatrix(@Nonnull qrCodeText: String, @Nonnull size: Int): BitMatrix? {
    val hints: MutableMap<EncodeHintType, Any?> = EnumMap(com.google.zxing.EncodeHintType::class.java)
    hints[EncodeHintType.MARGIN] = 0
    hints[EncodeHintType.ERROR_CORRECTION] = ErrorCorrectionLevel.L
    return QRCodeWriter().encode(qrCodeText, BarcodeFormat.QR_CODE, size, size, hints)
}

fun getQrCodeBitmap(
    @ColorInt qrTint: Int,
    @ColorInt backgroundTint: Int,
    content: String
): Bitmap {
    val bitMatrix = createBitMatrix(content, 256) ?: return Bitmap.createBitmap(0, 0, Bitmap.Config.RGB_565)

    val height: Int = bitMatrix.height
    val width: Int = bitMatrix.width

    val bmp: Bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
    for (x in 0 until width) {
        for (y in 0 until height) {
            bmp.setPixel(
                x, y,
                if (bitMatrix.get(x, y))
                    qrTint
                else
                    backgroundTint
            )
        }
    }

    return bmp

}

@Composable
fun QrCodeView(
    modifier: Modifier = Modifier,
    content: String,
) {
    val backgroundTint = ContextCompat.getColor(LocalContext.current, R.color.white)
    val mainTint = ContextCompat.getColor(LocalContext.current, R.color.black)
    val sliderPosition by remember {
        mutableStateOf(
            getQrCodeBitmap(
                qrTint = mainTint,
                backgroundTint = backgroundTint,
                content = content
            )
        )
    }

    Image(
        bitmap = sliderPosition.asImageBitmap(),
        contentDescription = null,
        modifier = modifier
    )
}