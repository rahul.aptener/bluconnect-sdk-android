package com.aptener.bluarmor.ui.util

import androidx.compose.foundation.background
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize
import kotlinx.coroutines.launch
import timber.log.Timber


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun SwipeActionView(
    modifier: Modifier,
    onDragCompleted: () -> Unit,
    foregroundContent: @Composable BoxScope.() -> Unit,
    backgroundContent: @Composable BoxScope.() -> Unit
) {
    val coroutineScope = rememberCoroutineScope()
    val fwUpdateScrollState = rememberScrollState()
    var size by remember { mutableStateOf(Size.Zero) }
    var canPushDrag by remember { mutableStateOf(false) }
    Box(modifier = modifier.onGloballyPositioned { size = it.size.toSize() }) {
        if (!fwUpdateScrollState.isScrollInProgress) {
            coroutineScope.launch {
                when (fwUpdateScrollState.value) {
                    0 -> {
                        canPushDrag = false
                        return@launch
                    }
                    fwUpdateScrollState.maxValue -> {
                        if (!canPushDrag) {
                            onDragCompleted()
                            canPushDrag = true
                        }
                        return@launch
                    }
                }

                val dragArea = (fwUpdateScrollState.value.toFloat() / fwUpdateScrollState.maxValue.toFloat() * 100f * 0.01f)
                when {
                    dragArea < 0.75f -> fwUpdateScrollState.animateScrollTo(0)
                    dragArea > 0.75f -> fwUpdateScrollState.animateScrollTo(fwUpdateScrollState.maxValue)
                }
            }
        }

        if (fwUpdateScrollState.value > 0)
            Box(
                content = backgroundContent,
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight()
            )

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
                .horizontalScroll(fwUpdateScrollState, reverseScrolling = true)
        ) {
            Row(
                modifier = Modifier
                    .fillMaxHeight()
                    .fillMaxWidth()
            ) {
                Spacer(
                    modifier =
                    Modifier
                        .width((size.width / (LocalContext.current.resources.displayMetrics.density)).dp)
                        .background(Color.Transparent)
                )
                Box(
                    content = foregroundContent,
                    modifier = Modifier
                        .width((size.width / (LocalContext.current.resources.displayMetrics.density)).dp)
                        .fillMaxHeight()
                )
            }
        }
    }
}