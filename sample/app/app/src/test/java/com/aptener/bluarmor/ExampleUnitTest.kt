package com.aptener.bluarmor

import com.aptener.bluconnect.app.device.buildAppRequest
import com.aptener.bluconnect.app.device.sx.proto.Media
import org.junit.Assert.assertEquals
import org.junit.Test
import java.io.ByteArrayOutputStream

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)

        val writeBuffer = ByteArrayOutputStream()

//
//        Media.MediaPlayerRequest.newBuilder()
//            .setAction(Media.MediaPlayerRequest.MediaPlayerAction.NEXT)
//            .build()
//            .buildAppRequest()
//            .writeTo(writeBuffer)


//
//        Dialer.DialerRequest.newBuilder()
//            .setAction(Dialer.DialerRequest.DialerAction.ACCEPT)
//            .build()
//            .buildAppRequest()
//            .writeTo(writeBuffer)

        Media.MediaPlayerRequest.newBuilder()
            .setAction(Media.MediaPlayerRequest.MediaPlayerAction.NEXT)
            .build()

            .buildAppRequest()
            .writeTo(writeBuffer)

        println("TEST LOG: ${writeBuffer.size()}")
        println("TEST LOG: ${writeBuffer.toByteArray().byteToHexString()}")
    }
}
